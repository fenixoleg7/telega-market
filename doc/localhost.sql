-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Янв 31 2023 г., 10:59
-- Версия сервера: 5.7.35-38
-- Версия PHP: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `wm9982_telega`
--
CREATE DATABASE IF NOT EXISTS `wm9982_telega` DEFAULT CHARACTER SET cp1251 COLLATE cp1251_general_ci;
USE `wm9982_telega`;

-- --------------------------------------------------------

--
-- Структура таблицы `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `title` varchar(512) NOT NULL,
  `body` text NOT NULL,
  `view` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `thumbnail_base_url` varchar(1024) DEFAULT NULL,
  `thumbnail_path` varchar(1024) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `published_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `article`
--

INSERT INTO `article` (`id`, `slug`, `title`, `body`, `view`, `category_id`, `thumbnail_base_url`, `thumbnail_path`, `status`, `created_by`, `updated_by`, `published_at`, `created_at`, `updated_at`) VALUES
(1, 'rtyrtyrty', 'rtyrtyrty', '<p>rtyrtyr</p>', '', 1, NULL, NULL, 1, 1, 1, 1566507382, 1565384187, 1565384187),
(2, 'test-article2', 'Test Article2', '<p>trtryrtyrty tyryrtyrt rtyrty</p>', '', 1, NULL, NULL, 0, 1, 1, 1575030371, 1574943980, 1574943980),
(3, 'sdfsd', 'sdfsdfsd', '<p>sdfsdfs</p>', '', 1, NULL, NULL, 0, 1, 1, 1575030371, 1576506601, 1576506601),
(4, 'fgdfg', 'fgdfg', '<p>dfgdg</p>', '', 1, NULL, NULL, 0, 1, 1, 1577449540, 1576929769, 1576929769);

-- --------------------------------------------------------

--
-- Структура таблицы `article_attachment`
--

CREATE TABLE `article_attachment` (
  `id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `base_url` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `article_category`
--

CREATE TABLE `article_category` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `title` varchar(512) NOT NULL,
  `body` text,
  `parent_id` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `article_category`
--

INSERT INTO `article_category` (`id`, `slug`, `title`, `body`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'news', 'News', NULL, NULL, 1, 1561974150, NULL),
(2, 'test', 'test', NULL, 1, 1, 1565383607, 1565383607);

-- --------------------------------------------------------

--
-- Структура таблицы `channel`
--

CREATE TABLE `channel` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `slug` varchar(1024) NOT NULL,
  `title` varchar(512) NOT NULL,
  `type` smallint(6) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `channel_description` text,
  `avatar_path` varchar(255) DEFAULT NULL,
  `avatar_base_url` varchar(255) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `exchange_price` int(11) DEFAULT NULL,
  `reason` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `channel`
--

INSERT INTO `channel` (`id`, `author_id`, `users`, `slug`, `title`, `type`, `category_id`, `price`, `channel_description`, `avatar_path`, `avatar_base_url`, `status`, `exchange_price`, `reason`) VALUES
(5, 3, 100, 'http://telega-market.localhost/channel/create', 'test', 1, 1, 200, 'wewerwerw', '1\\z1vg9S_WAHsabXeFDCqSiym8gJkgaPPx.jpg', '/source', 1, 300, NULL),
(6, 1, 200, 'http://telega-market.localhost/channel/create', 'test', 0, 1, 200, 'tfyrtytytyr', 'channel-img\\1\\Sdh3xDOU9CzjCw-EXRB-2pvAhHSPJjgf.png', '/source', 1, 250, NULL),
(7, 2, 2500, 'http://telega-market.localhost/channel/create', 'First channel', 0, 1, 400, 'Большинство валидаторов имеют сообщения об ошибках по умолчанию, которые будут добавлены к модели когда его атрибуты не проходят проверку. Например, required валидатор добавляет к модели сообщение об ошибке \"Имя пользователя не может быть пустым.\" когда атрибут username не удовлетворил правилу этого валидатора.', '\\1\\TkSZiOyIAOoaU9Hade0MRcR5XOmeDvJz.jpg', '/source', 1, 480, NULL),
(9, 2, 200, 'http://qaru.site/questions/611596/image-in-gridview-in-yii2', 'test222', 1, 1, 300, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, aliquid architecto assumenda doloribus ducimus eveniet optio reiciendis repellat repudiandae voluptates? Aut autem eius esse expedita laboriosam nisi optio quia voluptas?', '\\1\\diAQf2ND5k6RASsJkETjIXGTsP8lKagO.jpg', NULL, 1, 360, NULL),
(10, 3, 100, 'http://telega-market.localhost/channel/create', 'fdfgdfgd', 0, 4, 23, 'vdfgdfgdfgdg', '\\1\\NhzSqUCKGmtc-ABBoxdD1DIQK8m8W46R.jpg', NULL, 1, 5000, NULL),
(11, 1, 200, 'http://telega-market.localhost/channel/create', 'test', 0, 2, 200, 'tfyrtytytyr', 'channel-img\\1\\Sdh3xDOU9CzjCw-EXRB-2pvAhHSPJjgf.png', '/source', 1, 250, NULL),
(12, 2, 2500, 'http://telega-market.localhost/channel/create', 'test-manager', 0, 1, 400, 'uyiiyiy', '\\1\\QdBVpvWEmPi6lizQcHABUlxVuSNX-UhT.png', '/source', 1, 480, NULL),
(13, 2, 200, 'http://qaru.site/questions/611596/image-in-gridview-in-yii2', 'test222', 1, 2, 300, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, aliquid architecto assumenda doloribus ducimus eveniet optio reiciendis repellat repudiandae voluptates? Aut autem eius esse expedita laboriosam nisi optio quia voluptas?', '\\1\\diAQf2ND5k6RASsJkETjIXGTsP8lKagO.jpg', NULL, 1, 360, NULL),
(14, 3, 100, 'http://telega-market.localhost/channel/create', 'fdfgdfgd', 0, 4, 23, 'vdfgdfgdfgdg', '\\1\\NhzSqUCKGmtc-ABBoxdD1DIQK8m8W46R.jpg', NULL, 1, 5000, NULL),
(15, 4, 0, 'http://telega-market.localhost/channel/test-channel', 'Хороший канал', 0, 3, 215, 'Background\r\nWe have a plugin called true passwordless login which:\r\n- Creates a user\r\n- Set cookies & access 365\r\n- Sets GUID\r\n- CRM API GUID + Page URL\r\n- Magic link access\r\n\r\nOur Goals\r\nWe want to split the plugins into two separate plugins one handling the magic link access and the other controlling the API input and output.\r\n\r\nThe reasoning being that they both require their own security configuration one being for the front end magic link access and the other for the API GUID access which sits within the database table.\r\nFunction\r\n\r\nMagic Link Access should:\r\n- Create a user\r\n- Set cookies & access to 365 days\r\n- Grant magic link access\r\n\r\nThe CRM API should:\r\nSet GUID\r\nCreate API GUID + PAGE URL\r\nNew Feature: Look table to produce an “entrypoint” code into the API\r\n\r\nAdditional Goals\r\nWe want to release the Magic Link Access Plugin to the WordPress.org Plugin Store', '\\1\\getZcKAF797DD6quT4eliqyCOmP-kJ8v.jpg', NULL, 0, 300, NULL),
(16, 3, 0, 'http://telega-market.localhost/', 'New Channel', 2, 3, 400, 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ', '\\1\\PMyyQ7MwbubZ1ij3FCEPHMY5MzgKbk8Z.jpg', NULL, 3, 200, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `channel_category`
--

CREATE TABLE `channel_category` (
  `id` int(11) NOT NULL,
  `slug` varchar(1024) NOT NULL,
  `title` varchar(512) NOT NULL,
  `body` text,
  `parent_id` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `channel_category`
--

INSERT INTO `channel_category` (`id`, `slug`, `title`, `body`, `parent_id`, `status`) VALUES
(1, 'test-slug', 'test1', 'ddfgdfg', NULL, 0),
(2, 'test-slug2', 'test2', 'ddfgdfg', NULL, 0),
(3, '', 'Авто и Мото', 'Раздел о автомобилях и мотоциклах', NULL, 1),
(4, '', 'Авто и Мото4', 'cvbcvbcb', NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `checked_channel`
--

CREATE TABLE `checked_channel` (
  `id` int(11) NOT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `published_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `checked_channel`
--

INSERT INTO `checked_channel` (`id`, `channel_id`, `order_id`, `status`, `url`, `created_at`, `published_at`, `updated_at`) VALUES
(80, 15, 69, 2, NULL, 1583151141, 1567504301, 1583326679),
(81, 15, 70, 7, NULL, 1583161075, 1566977822, 1583326674),
(82, 15, 71, 3, 'http://telega-market.localhost/channel/index', 1583161112, 1566977822, 1583162312),
(83, 15, 72, 7, NULL, 1583163798, 1567504301, 1583326673),
(84, 15, 73, 7, NULL, 1583164167, 1566977822, 1583326672),
(85, 15, 74, 7, NULL, 1583164436, 1571900972, 1583326670),
(86, 15, 75, 7, NULL, 1583165125, 1567504301, 1583326667),
(87, 15, 76, 1, NULL, 1583329747, 1566977822, 1583329747),
(88, 15, 77, 1, NULL, 1583329803, 1566977822, 1583329803),
(89, 15, 78, 1, NULL, 1583329902, 1566977822, 1583329902),
(90, 15, 79, 1, NULL, 1583329969, 1566977822, 1583329969),
(91, 15, 80, 1, NULL, 1583329982, 1566977822, 1583329982),
(92, 15, 81, 1, NULL, 1583330001, 1566977822, 1583330001),
(93, 15, 82, 1, NULL, 1583333853, 1566977822, 1583333853),
(94, 15, 83, 1, NULL, 1583333903, 1566977822, 1583333903),
(95, 15, 84, 1, NULL, 1583334048, 1566977822, 1583334048),
(96, 15, 85, 1, NULL, 1583346941, 1568796533, 1583346941),
(97, 15, 86, 1, NULL, 1583347123, 1568796533, 1583347123),
(98, 15, 87, 1, NULL, 1583347170, 1568796533, 1583347170),
(99, 15, 88, 1, NULL, 1583347321, 1568796533, 1583347321),
(100, 15, 89, 1, NULL, 1583347858, 1568796533, 1583347858),
(101, 15, 90, 1, NULL, 1583347945, 1566977822, 1583347945),
(102, 5, 91, 7, NULL, 1583441732, 1568796533, 1587384700),
(103, 5, 92, 7, NULL, 1583441761, 1568796533, 1587384702),
(104, 5, 93, 7, NULL, 1583441857, 1568796533, 1583442291),
(105, 5, 94, 7, NULL, 1587472909, 1566977822, 1587476861),
(106, 5, 95, 7, NULL, 1587473069, 1566977822, 1587912483),
(107, 5, 96, 7, NULL, 1587477059, 1568885114, 1587912537),
(108, 16, 97, 7, NULL, 1587732295, 1566977822, 1587910566),
(109, 5, 98, 7, NULL, 1587999639, 1565249822, 1588858685),
(110, 5, 99, 7, NULL, 1587999918, 1565249822, 1588859296),
(111, 5, 100, 7, NULL, 1587999930, 1565249822, 1588940706),
(112, 5, 101, 3, 'http://telega-market.localhost/channel/index', 1587999974, 1565249822, 1588940859),
(113, 5, 102, 7, NULL, 1588941378, 1566977822, 1588955709),
(114, 5, 103, 7, NULL, 1588944156, 1565249822, 1588955801);

-- --------------------------------------------------------

--
-- Структура таблицы `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_ip` text NOT NULL,
  `updated_ip` text NOT NULL,
  `created_by` text NOT NULL,
  `updated_by` text NOT NULL,
  `code` text NOT NULL,
  `name` text NOT NULL,
  `symbol` text NOT NULL,
  `rate` double(11,2) UNSIGNED NOT NULL,
  `decimal_places` int(11) UNSIGNED NOT NULL,
  `is_default` int(11) UNSIGNED NOT NULL,
  `sort` int(11) UNSIGNED NOT NULL,
  `status` smallint(6) UNSIGNED NOT NULL,
  `base_ccy` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `currency`
--

INSERT INTO `currency` (`id`, `created_at`, `updated_at`, `created_ip`, `updated_ip`, `created_by`, `updated_by`, `code`, `name`, `symbol`, `rate`, `decimal_places`, `is_default`, `sort`, `status`, `base_ccy`) VALUES
(28, 1592747255, 1594717860, '', '', '1', '1', 'USD', '', '$', 26.45, 0, 0, 0, 1, 'UAH'),
(29, 1592747255, 1594717803, '', '', '1', '1', 'EUR', '', '€', 29.60, 0, 0, 0, 1, 'UAH'),
(30, 1592747255, 1594717925, '', '', '1', '1', 'RUR', 'Рубль', '₽', 0.36, 0, 1, 0, 1, 'UAH'),
(31, 1594666104, 1594666104, '', '', '1', '1', 'UAN', 'Гривна', '₴', 1.00, 1, 0, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `file_storage_item`
--

CREATE TABLE `file_storage_item` (
  `id` int(11) NOT NULL,
  `component` varchar(255) NOT NULL,
  `base_url` varchar(1024) NOT NULL,
  `path` varchar(1024) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `upload_ip` varchar(15) DEFAULT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `file_storage_item`
--

INSERT INTO `file_storage_item` (`id`, `component`, `base_url`, `path`, `type`, `size`, `name`, `upload_ip`, `created_at`) VALUES
(1, 'fileStorage', '/storage/web/source', '\\1\\FbQRdB8vpOli063K2_ezFWSN4VDA6BBE.jpg', 'image/jpeg', 60878, 'FbQRdB8vpOli063K2_ezFWSN4VDA6BBE', '127.0.0.1', 1561978897),
(2, 'fileStorage', '/source', '\\1\\Y1ewAtFadswLpKqYIeddc2OpKzV0YiPX.jpg', 'image/jpeg', 133621, 'Y1ewAtFadswLpKqYIeddc2OpKzV0YiPX', '127.0.0.1', 1562100908),
(3, 'fileStorage', '/source', '\\1\\X05nAZrUffWVyvrMDNoRTOhtqqLIRj7K.jpg', 'image/jpeg', 133621, 'X05nAZrUffWVyvrMDNoRTOhtqqLIRj7K', '127.0.0.1', 1562101151),
(4, 'fileStorage', '/source', '\\1\\E31TDwBLvHGuXnd3Dmujt8ODWze7L-gm.png', 'image/png', 254211, 'E31TDwBLvHGuXnd3Dmujt8ODWze7L-gm', '127.0.0.1', 1564998856),
(5, 'fileStorage', '/source', '\\1\\9ddTA2uFRQJ8v7tcZmUi5Qw8XnCt6otF.jpg', 'image/jpeg', 60878, '9ddTA2uFRQJ8v7tcZmUi5Qw8XnCt6otF', '127.0.0.1', 1565299238),
(6, 'fileStorage', '/source', '\\1\\evbWJkKP9vVHIRpXBRnUljXM8DeUVwbH.jpg', 'image/jpeg', 60878, 'evbWJkKP9vVHIRpXBRnUljXM8DeUVwbH', '127.0.0.1', 1565299307),
(7, 'fileStorage', '/source', '\\1\\e-SAHa_9xZ9-u9aRN08fJTzMuWNGwfsB.jpg', 'image/jpeg', 15499, 'e-SAHa_9xZ9-u9aRN08fJTzMuWNGwfsB', '127.0.0.1', 1565384001),
(8, 'fileStorage', '/source', '\\1\\L5dtha-63jJIYmoW13wiafXZpigNqyrT.jpg', 'image/jpeg', 1230715, 'L5dtha-63jJIYmoW13wiafXZpigNqyrT', '127.0.0.1', 1565384011),
(10, 'fileStorage', '/source', '\\1\\3SrUgBKmX4Z0u9c-D4xgPDRUc8yQPrxn.jpg', 'image/jpeg', 60878, '3SrUgBKmX4Z0u9c-D4xgPDRUc8yQPrxn', '127.0.0.1', 1565950104),
(11, 'fileStorage', '/source', '\\1\\L2EvWMljGVKHlz3ZdFSPMcZHZKCvpTGU.jpg', 'image/jpeg', 60878, 'L2EvWMljGVKHlz3ZdFSPMcZHZKCvpTGU', '127.0.0.1', 1565952358),
(12, 'fileStorage', '/source', '\\1\\_NM-lSrH5Qk7JiX6xBvA4EF4Ev799K9S.jpg', 'image/jpeg', 113379, '_NM-lSrH5Qk7JiX6xBvA4EF4Ev799K9S', '127.0.0.1', 1565952540),
(13, 'fileStorage', '/source', '\\1\\xPEgv8F0BnEKLdYxBV1DOmZafcxgCN9Z.png', 'image/png', 7099, 'xPEgv8F0BnEKLdYxBV1DOmZafcxgCN9Z', '127.0.0.1', 1565952609),
(14, 'fileStorage', '/source', '\\1\\z1vg9S_WAHsabXeFDCqSiym8gJkgaPPx.jpg', 'image/jpeg', 133621, 'z1vg9S_WAHsabXeFDCqSiym8gJkgaPPx', '127.0.0.1', 1565952778),
(15, 'fileStorage', '/source', '\\1\\9bo32QYeWXiisXF_SdRQn8fE_g9gJkuQ.jpg', 'image/jpeg', 113379, '9bo32QYeWXiisXF_SdRQn8fE_g9gJkuQ', '127.0.0.1', 1565953576),
(16, 'fileStorage', '/source', 'subfolder\\1\\HzeMhwNfgTudsueXyXhIcCzvg2XTiR_B.png', 'image/png', 254211, 'HzeMhwNfgTudsueXyXhIcCzvg2XTiR_B', '127.0.0.1', 1566038282),
(17, 'fileStorage', '/source', 'channel-img\\1\\Sdh3xDOU9CzjCw-EXRB-2pvAhHSPJjgf.png', 'image/png', 254211, 'Sdh3xDOU9CzjCw-EXRB-2pvAhHSPJjgf', '127.0.0.1', 1566043427),
(18, 'fileStorage', '/source', 'channel-img\\1\\u_2tZrFlrxlgpLBM-zeC3uPuPRGYu3hs.png', 'image/png', 254211, 'u_2tZrFlrxlgpLBM-zeC3uPuPRGYu3hs', '127.0.0.1', 1566044367),
(19, 'fileStorage', '/source', '\\1\\PZeCAwIHrBrILf8MgSwI2_AaPCiCVj1a.png', 'image/png', 254211, 'PZeCAwIHrBrILf8MgSwI2_AaPCiCVj1a', '127.0.0.1', 1566044519),
(20, 'fileStorage', '/source', '\\1\\meoXIKzizTQvVnleO-f3Eh_fQ7mBFkOP.png', 'image/png', 254211, 'meoXIKzizTQvVnleO-f3Eh_fQ7mBFkOP', '127.0.0.1', 1566045841),
(21, 'fileStorage', '/source', '\\1\\B72CquuCMl14Y4QjGK8F9cVe0O_zif6D.png', 'image/png', 254211, 'B72CquuCMl14Y4QjGK8F9cVe0O_zif6D', '127.0.0.1', 1566046084),
(22, 'fileStorage', '/source', '\\1\\nPII0jwoxlbCayzYGVY-ie9faBS4nZbC.png', 'image/png', 254211, 'nPII0jwoxlbCayzYGVY-ie9faBS4nZbC', '127.0.0.1', 1566046322),
(23, 'fileStorage', '/source', '\\1\\F8XR0hXDPHcLHNTAVkEy__udSE3uRRSJ.png', 'image/png', 254211, 'F8XR0hXDPHcLHNTAVkEy__udSE3uRRSJ', '127.0.0.1', 1566307356),
(25, 'fileStorage', '/source', '\\1\\nD8PVjsLtbgiNA2OzjPMxH1pTwVzTfi6.png', 'image/png', 254211, 'nD8PVjsLtbgiNA2OzjPMxH1pTwVzTfi6', '127.0.0.1', 1566307881),
(26, 'fileStorage', '/source', '\\1\\2vurJcBtKUGLwkPCVL2OrRpwnsKyc62z.png', 'image/png', 254211, '2vurJcBtKUGLwkPCVL2OrRpwnsKyc62z', '127.0.0.1', 1566308287),
(28, 'fileStorage', '/source', '\\1\\YW_4S7XdVZVN1Hmm8xcbD1MTpeoGSYRA.png', 'image/png', 254211, 'YW_4S7XdVZVN1Hmm8xcbD1MTpeoGSYRA', '127.0.0.1', 1566313517),
(30, 'fileStorage', '/source', '\\1\\p2TYwPqgxTj4TBdUp0CiB_4ijWjzBHyl.png', 'image/png', 254211, 'p2TYwPqgxTj4TBdUp0CiB_4ijWjzBHyl', '127.0.0.1', 1566376173),
(31, 'fileStorage', 'D:\\OSPanel\\domains\\telega-market\\storage/web/source', '\\1\\qbCCEUpfwfY4CaHfMFhY0bqhQYA0ugi-.png', 'image/png', 254211, 'qbCCEUpfwfY4CaHfMFhY0bqhQYA0ugi-', '127.0.0.1', 1566376485),
(32, 'fileStorage', 'D:\\OSPanel\\domains\\telega-market\\storage/web/source/', '\\1\\bNQKCvdJWp2sSiUMqoCga76VsvhTTy8k.png', 'image/png', 254211, 'bNQKCvdJWp2sSiUMqoCga76VsvhTTy8k', '127.0.0.1', 1566376637),
(34, 'fileStorage', '/storage/web/source/', '\\1\\2reJiMfAOhIDObB2naosxfcqi0KQmKiF.jpg', 'image/jpeg', 133621, '2reJiMfAOhIDObB2naosxfcqi0KQmKiF', '127.0.0.1', 1566377656),
(35, 'fileStorage', '/storage/web/source/', '\\1\\diAQf2ND5k6RASsJkETjIXGTsP8lKagO.jpg', 'image/jpeg', 113379, 'diAQf2ND5k6RASsJkETjIXGTsP8lKagO', '127.0.0.1', 1566469618),
(36, 'fileStorage', '/storage/web/source/', '\\1\\NhzSqUCKGmtc-ABBoxdD1DIQK8m8W46R.jpg', 'image/jpeg', 132638, 'NhzSqUCKGmtc-ABBoxdD1DIQK8m8W46R', '127.0.0.1', 1566679803),
(37, 'fileStorage', '/storage/web/source/', '\\1\\rYbkAfodGhxOog75sHtqHMFAn5m7aHRJ.jpg', 'image/jpeg', 113379, 'rYbkAfodGhxOog75sHtqHMFAn5m7aHRJ', '127.0.0.1', 1566680172),
(38, 'fileStorage', '/storage/web/source/', '\\1\\b7QzxUgReCBsV12PgG-2Ob7Z7BItw3K3.jpg', 'image/jpeg', 133621, 'b7QzxUgReCBsV12PgG-2Ob7Z7BItw3K3', '127.0.0.1', 1566680198),
(39, 'fileStorage', '/storage/web/source/', '\\1\\B08rpE9lrsD5eJbw4175BNz9T-M0nQlQ.png', 'image/png', 254211, 'B08rpE9lrsD5eJbw4175BNz9T-M0nQlQ', '127.0.0.1', 1567250096),
(40, 'fileStorage', '/storage/web/source/', '\\1\\nhxpeQYw2V_ToMRhCWP2-tCHWAa_khnH.png', 'image/png', 254211, 'nhxpeQYw2V_ToMRhCWP2-tCHWAa_khnH', '127.0.0.1', 1567250121),
(41, 'fileStorage', '/storage/web/source/', '\\1\\RDRRZyEUzUqQR_tS66YImgLtItf-o6gw.png', 'image/png', 254211, 'RDRRZyEUzUqQR_tS66YImgLtItf-o6gw', '127.0.0.1', 1567251444),
(42, 'fileStorage', '/storage/web/source/', '\\1\\u9181Ujpech0yyTRuwEccfKOxOvrTYLT.png', 'image/png', 31821, 'u9181Ujpech0yyTRuwEccfKOxOvrTYLT', '127.0.0.1', 1567253374),
(43, 'fileStorage', '/storage/web/source/', '\\1\\u1HE8QmzTId10P9RQxDOz3ntJuW7Kewn.jpg', 'image/jpeg', 15963813, 'u1HE8QmzTId10P9RQxDOz3ntJuW7Kewn', '127.0.0.1', 1567502709),
(44, 'fileStorage', '/storage/web/source/', '\\1\\Ipe6bBPbZvfomgsK37WgGeQBSYjKbPEJ.png', 'image/png', 31821, 'Ipe6bBPbZvfomgsK37WgGeQBSYjKbPEJ', '127.0.0.1', 1567502899),
(45, 'fileStorage', '/storage/web/source/', '\\1\\IfDDa8XujDqMjk98rI4UXWqjHMgwUrOF.png', 'image/png', 31821, 'IfDDa8XujDqMjk98rI4UXWqjHMgwUrOF', '127.0.0.1', 1567503066),
(46, 'fileStorage', '/storage/web/source/', '\\1\\5r8Tk9SpvhFiBFQLwALS42bfXRL6AdDC.png', 'image/png', 31821, '5r8Tk9SpvhFiBFQLwALS42bfXRL6AdDC', '127.0.0.1', 1567503170),
(47, 'fileStorage', '/storage/web/source/', '\\1\\U865uvMTKvRrAhHvMgHEpT3N4A-Y0NLn.png', 'image/png', 31821, 'U865uvMTKvRrAhHvMgHEpT3N4A-Y0NLn', '127.0.0.1', 1567503676),
(48, 'fileStorage', '/storage/web/source/', '\\1\\VxkRhcrKQQYo0ysIUrHGjwcvD4D5dLD3.png', 'image/png', 31821, 'VxkRhcrKQQYo0ysIUrHGjwcvD4D5dLD3', '127.0.0.1', 1567504118),
(49, 'fileStorage', '/storage/web/source/', '\\1\\XzPqnNje0fvy17Ln3IdKC0wkowoW4Wf7.png', 'image/png', 31821, 'XzPqnNje0fvy17Ln3IdKC0wkowoW4Wf7', '127.0.0.1', 1567504301),
(50, 'fileStorage', '/storage/web/source/', '\\1\\EyyWMfroO6qhiIVXza1tJ1D5Mgo9CC-w.jpg', 'image/jpeg', 143209, 'EyyWMfroO6qhiIVXza1tJ1D5Mgo9CC-w', '127.0.0.1', 1571304457),
(51, 'fileStorage', '/storage/web/source/', '\\1\\ICQ1M5j_ElA540_BgoBr0aNeP0Xj0tas.png', 'image/png', 31821, 'ICQ1M5j_ElA540_BgoBr0aNeP0Xj0tas', '127.0.0.1', 1571402980),
(52, 'fileStorage', '/storage/web/source/', '\\1\\5fRcJuFuGevkW0TAJm-cklN_ncrX6pW9.png', 'image/png', 31821, '5fRcJuFuGevkW0TAJm-cklN_ncrX6pW9', '127.0.0.1', 1571403442),
(53, 'fileStorage', '/storage/web/source/', '\\1\\Yia4YiKsGuR3t7keFs6pmzC2eRoc36Bj.png', 'image/png', 31821, 'Yia4YiKsGuR3t7keFs6pmzC2eRoc36Bj', '127.0.0.1', 1571403601),
(54, 'fileStorage', '/storage/web/source/', '\\1\\UvjaFfDiNNNBSOilkamWlWK_lQ11MIfG.jpg', 'image/jpeg', 60878, 'UvjaFfDiNNNBSOilkamWlWK_lQ11MIfG', '127.0.0.1', 1571666995),
(55, 'fileStorage', '/storage/web/source/', '\\1\\TkHZx-80g7gw3dLUsydX7T0LVE-pzSeC.jpg', 'image/jpeg', 594874, 'TkHZx-80g7gw3dLUsydX7T0LVE-pzSeC', '127.0.0.1', 1574517129),
(57, 'fileStorage', '/storage/web/source/', '\\1\\getZcKAF797DD6quT4eliqyCOmP-kJ8v.jpg', 'image/jpeg', 132579, 'getZcKAF797DD6quT4eliqyCOmP-kJ8v', '127.0.0.1', 1575815205),
(58, 'fileStorage', '/storage/web/source/', '\\1\\Kgg1Or9RmQ5J1a1xnJ7vQWY0g_pldz8J.jpg', 'image/jpeg', 113379, 'Kgg1Or9RmQ5J1a1xnJ7vQWY0g_pldz8J', '127.0.0.1', 1575820486),
(59, 'fileStorage', '/storage/web/source/', '\\1\\CkwAG-WG2Mtr5elHuJKFfTFDd-RTiR0T.jpg', 'image/jpeg', 9787, 'CkwAG-WG2Mtr5elHuJKFfTFDd-RTiR0T', '127.0.0.1', 1575925119),
(60, 'fileStorage', '/storage/web/source/', '\\1\\jBWRQ1MDwPYBvF3gMrqoiwU_YiHn6OAx.jpg', 'image/jpeg', 25112, 'jBWRQ1MDwPYBvF3gMrqoiwU_YiHn6OAx', '127.0.0.1', 1575986615),
(61, 'fileStorage', '/storage/web/source/', '\\1\\n27Sgf2Wz_1_62sN3RGFO2oIGtlP2P6E.jpg', 'image/jpeg', 60878, 'n27Sgf2Wz_1_62sN3RGFO2oIGtlP2P6E', '127.0.0.1', 1575989880),
(62, 'fileStorage', '/storage/web/source/', '\\1\\lYBb3W_XU7pAKPaP8F2ZDFTMm96bJlEL.jpg', 'image/jpeg', 99693, 'lYBb3W_XU7pAKPaP8F2ZDFTMm96bJlEL', '127.0.0.1', 1576053257),
(63, 'fileStorage', '/storage/web/source/', '\\1\\JMInK0Q-b4sD1yeFXcoZTFXVRaTt2ZAz.jpg', 'image/jpeg', 60878, 'JMInK0Q-b4sD1yeFXcoZTFXVRaTt2ZAz', '127.0.0.1', 1576060596),
(64, 'fileStorage', '/storage/web/source/', '\\1\\oqHKTa9LzdC7mOpKpwioQgxrQNfyeL2b.jpg', 'image/jpeg', 143209, 'oqHKTa9LzdC7mOpKpwioQgxrQNfyeL2b', '127.0.0.1', 1576060874),
(65, 'fileStorage', '/storage/web/source/', '\\1\\PAijWzTsEjy51k_E2HqdNyQ9vcP8eqsN.jpg', 'image/jpeg', 99693, 'PAijWzTsEjy51k_E2HqdNyQ9vcP8eqsN', '127.0.0.1', 1576068200),
(66, 'fileStorage', '/storage/web/source/', '\\1\\px29J9PbrIPXkjIBi2PExGnIqNhuIUMR.jpg', 'image/jpeg', 25112, 'px29J9PbrIPXkjIBi2PExGnIqNhuIUMR', '127.0.0.1', 1576097428),
(67, 'fileStorage', '/storage/web/source/', '\\1\\TkSZiOyIAOoaU9Hade0MRcR5XOmeDvJz.jpg', 'image/jpeg', 9787, 'TkSZiOyIAOoaU9Hade0MRcR5XOmeDvJz', '127.0.0.1', 1576251626),
(68, 'fileStorage', '/storage/web/source/', '\\1\\cJh4WtQQNvgR78TKelHazrMurjeGDips.jpg', 'image/jpeg', 99693, 'cJh4WtQQNvgR78TKelHazrMurjeGDips', '127.0.0.1', 1576251750),
(69, 'fileStorage', '/storage/web/source/', '\\1\\XqcAJo4xSQUGzOxKTVOTjD33xoVUa8UU.jpg', 'image/jpeg', 18472, 'XqcAJo4xSQUGzOxKTVOTjD33xoVUa8UU', '127.0.0.1', 1576330729),
(70, 'fileStorage', '/storage/web/source/', '\\1\\Pqu3NeaBlrIPnQkPHSgsivZkCYSkL4Nb.jpg', 'image/jpeg', 99693, 'Pqu3NeaBlrIPnQkPHSgsivZkCYSkL4Nb', '127.0.0.1', 1576340577),
(71, 'fileStorage', '/storage/web/source/', '\\1\\xW1G1UkRoAg8R_gExkiJSQ0ftP4dVqTx.jpg', 'image/jpeg', 99693, 'xW1G1UkRoAg8R_gExkiJSQ0ftP4dVqTx', '127.0.0.1', 1576341175),
(72, 'fileStorage', '/storage/web/source/', '\\1\\UUUdjfCPfmDPaYvdFQBL_TGy-x4JrEDG.jpg', 'image/jpeg', 132638, 'UUUdjfCPfmDPaYvdFQBL_TGy-x4JrEDG', '127.0.0.1', 1576341889),
(73, 'fileStorage', '/storage/web/source/', '\\1\\xsTNY-k7eShUPiu9KBt02ym9I2FCPzMh.jpg', 'image/jpeg', 133621, 'xsTNY-k7eShUPiu9KBt02ym9I2FCPzMh', '127.0.0.1', 1576342320),
(74, 'fileStorage', '/storage/web/source/', '\\1\\mwo6HjK47EAak9NRD7Ka1WyRZS5Pq9JT.jpg', 'image/jpeg', 143209, 'mwo6HjK47EAak9NRD7Ka1WyRZS5Pq9JT', '127.0.0.1', 1576501654),
(75, 'fileStorage', '/storage/web/source/', '\\1\\3KtbOA0IyyybnYtKd-cSAokS-HC1DGxA.jpg', 'image/jpeg', 143209, '3KtbOA0IyyybnYtKd-cSAokS-HC1DGxA', '127.0.0.1', 1576934463),
(76, 'fileStorage', '/storage/web/source/', '\\1\\jXBONNovI8rrFKulXfHeYCxbaZ5T0Mov.jpg', 'image/jpeg', 60878, 'jXBONNovI8rrFKulXfHeYCxbaZ5T0Mov', '127.0.0.1', 1582025381),
(77, 'fileStorage', '/storage/web/source/', '\\1\\MaCLp9Qvw84H0dsMyrF0diZhugeFz34H.jpg', 'image/jpeg', 9787, 'MaCLp9Qvw84H0dsMyrF0diZhugeFz34H', '127.0.0.1', 1582643525),
(78, 'fileStorage', '/storage/web/source/', '\\1\\VT4JNjHlaZH4qJUQuywneIpVXUUxOrZ_.jpg', 'image/jpeg', 133621, 'VT4JNjHlaZH4qJUQuywneIpVXUUxOrZ_', '127.0.0.1', 1582644063),
(80, 'fileStorage', '/storage/web/source/', '\\1\\TpNaOJ1GioKwG_Z_rXwFnW3Q-HwaF53N.jpg', 'image/jpeg', 60878, 'TpNaOJ1GioKwG_Z_rXwFnW3Q-HwaF53N', '127.0.0.1', 1582715695),
(82, 'fileStorage', '/storage/web/source/', '\\1\\o1K5V__XfOTsYF2U6hVEczpTH-50rJFv.jpg', 'image/jpeg', 60878, 'o1K5V__XfOTsYF2U6hVEczpTH-50rJFv', '127.0.0.1', 1582716065),
(84, 'fileStorage', '/storage/web/source/', '\\1\\qcSB5k1NmZsqIAXygJrvU_2Dh6k8dzqj.jpg', 'image/jpeg', 60878, 'qcSB5k1NmZsqIAXygJrvU_2Dh6k8dzqj', '127.0.0.1', 1582719441),
(85, 'fileStorage', '/storage/web/source/', '\\1\\yQucnri6fDnTpBMtKas6g6eYlmTxUG88.jpg', 'image/jpeg', 9787, 'yQucnri6fDnTpBMtKas6g6eYlmTxUG88', '127.0.0.1', 1582721518),
(86, 'fileStorage', '/storage/web/source/', '\\1\\Yfphf8KcjmchVHU5C07wwPjel5aJTWEN.jpg', 'image/jpeg', 60878, 'Yfphf8KcjmchVHU5C07wwPjel5aJTWEN', '127.0.0.1', 1583151130),
(87, 'fileStorage', '/storage/web/source/', '\\1\\67ywzNYVy6QaIRAOpq5UIMKy68H8xA4K.jpg', 'image/jpeg', 25112, '67ywzNYVy6QaIRAOpq5UIMKy68H8xA4K', '127.0.0.1', 1583161070),
(88, 'fileStorage', '/storage/web/source/', '\\1\\0-1QLJuKcnSLMBIpGu6V6anGYHqL4Squ.jpg', 'image/jpeg', 60878, '0-1QLJuKcnSLMBIpGu6V6anGYHqL4Squ', '127.0.0.1', 1583163793),
(89, 'fileStorage', '/storage/web/source/', '\\1\\LX8Bscz-czzVUkvro9vHoCFwjCrgt13m.jpg', 'image/jpeg', 133621, 'LX8Bscz-czzVUkvro9vHoCFwjCrgt13m', '127.0.0.1', 1583164161),
(90, 'fileStorage', '/storage/web/source/', '\\1\\kjoHJ1ymQBkK8SIxR8vxbT6fQdjXvb3a.jpg', 'image/jpeg', 18472, 'kjoHJ1ymQBkK8SIxR8vxbT6fQdjXvb3a', '127.0.0.1', 1583164431),
(91, 'fileStorage', '/storage/web/source/', '\\1\\XaEZG6SiRrBbvx0iMWiJSiSxvvM6T6pe.jpg', 'image/jpeg', 133621, 'XaEZG6SiRrBbvx0iMWiJSiSxvvM6T6pe', '127.0.0.1', 1583165121),
(92, 'fileStorage', '/storage/web/source/', '\\1\\v32hfoxL1rL0CG8GkFm1f61_CVOt1HM3.jpg', 'image/jpeg', 9787, 'v32hfoxL1rL0CG8GkFm1f61_CVOt1HM3', '127.0.0.1', 1583329745),
(93, 'fileStorage', '/storage/web/source/', '\\1\\jrNwsrR0fD1pu6zpToEg2_Uy_H1NVTh-.jpg', 'image/jpeg', 133621, 'jrNwsrR0fD1pu6zpToEg2_Uy_H1NVTh-', '127.0.0.1', 1583346938),
(94, 'fileStorage', '/storage/web/source/', '\\1\\-fQ9s9P-ln2Pc6UkDhHiJvksv8Is7iQm.jpg', 'image/jpeg', 60878, '-fQ9s9P-ln2Pc6UkDhHiJvksv8Is7iQm', '127.0.0.1', 1583347941),
(95, 'fileStorage', '/storage/web/source/', '\\1\\sKY_O0KsTv192ndbLQw197ryUXNDZgf_.jpg', 'image/jpeg', 113379, 'sKY_O0KsTv192ndbLQw197ryUXNDZgf_', '127.0.0.1', 1583441724),
(96, 'fileStorage', '/storage/web/source/', '\\1\\g2dozafpGNEYasA5bCLFIXvtF16OPNz_.jpg', 'image/jpeg', 18472, 'g2dozafpGNEYasA5bCLFIXvtF16OPNz_', '127.0.0.1', 1587472903),
(97, 'fileStorage', '/storage/web/source/', '\\1\\Byo9S2fGK1vjv5XOebf_IDXGtKFFpELM.jpg', 'image/jpeg', 133621, 'Byo9S2fGK1vjv5XOebf_IDXGtKFFpELM', '127.0.0.1', 1587477053),
(98, 'fileStorage', '/storage/web/source/', '\\1\\PMyyQ7MwbubZ1ij3FCEPHMY5MzgKbk8Z.jpg', 'image/jpeg', 25112, 'PMyyQ7MwbubZ1ij3FCEPHMY5MzgKbk8Z', '127.0.0.1', 1587730600),
(99, 'fileStorage', '/storage/web/source/', '\\1\\b0FXBO2YMXUbHCiOG5LdwoODVhugIUIC.jpg', 'image/jpeg', 143209, 'b0FXBO2YMXUbHCiOG5LdwoODVhugIUIC', '127.0.0.1', 1587732287),
(100, 'fileStorage', '/storage/web/source/', '\\1\\W77eAMTBuOSlOHxYyNNW9AXgzTJz1wFT.jpg', 'image/jpeg', 25112, 'W77eAMTBuOSlOHxYyNNW9AXgzTJz1wFT', '127.0.0.1', 1587999626),
(101, 'fileStorage', '/storage/web/source/', '\\1\\keExwFFzCK1AFkCuWF08JcsMKu-NX_yz.jpg', 'image/jpeg', 160577, 'keExwFFzCK1AFkCuWF08JcsMKu-NX_yz', '127.0.0.1', 1588941237),
(102, 'fileStorage', '/storage/web/source/', '\\1\\ASf6CKiZpj9fAkOLbr2tiE-HRiDQkkfp.jpg', 'image/jpeg', 25112, 'ASf6CKiZpj9fAkOLbr2tiE-HRiDQkkfp', '127.0.0.1', 1588941293),
(103, 'fileStorage', '/storage/web/source/', '\\1\\D6v3HM9rZb2BuGj13m-KWGlhSHizPIg-.jpg', 'image/jpeg', 133621, 'D6v3HM9rZb2BuGj13m-KWGlhSHizPIg-', '127.0.0.1', 1588941368),
(104, 'fileStorage', '/storage/web/source/', '\\1\\6rcbNLKIERm70Jrd7deVzu6fOVsOyHAH.jpg', 'image/jpeg', 132638, '6rcbNLKIERm70Jrd7deVzu6fOVsOyHAH', '127.0.0.1', 1588944144),
(105, 'fileStorage', '/storage/web/source/', '/1/Jy8p73y5nFZ386mzEiUM9igNTyG4EIC_.jpg', 'image/jpeg', 58378, 'Jy8p73y5nFZ386mzEiUM9igNTyG4EIC_', '46.175.252.5', 1610998840),
(106, 'fileStorage', '/storage/web/source/', '/1/Y54MTQ-5ywPve8kPRVxbSAG5Z9StoRgw.jpg', 'image/jpeg', 60878, 'Y54MTQ-5ywPve8kPRVxbSAG5Z9StoRgw', '23.106.56.21', 1610999369);

-- --------------------------------------------------------

--
-- Структура таблицы `i18n_message`
--

CREATE TABLE `i18n_message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(16) NOT NULL DEFAULT '',
  `translation` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `i18n_message`
--

INSERT INTO `i18n_message` (`id`, `language`, `translation`) VALUES
(1075, 'ru', 'Аккаунт'),
(1076, 'ru', 'Приложение'),
(1077, 'ru', 'Настройки приложения'),
(1078, 'ru', 'Хроника приложения'),
(1079, 'ru', 'Вы уверены, что хотите удалить эту запись?'),
(1080, 'ru', 'Вы уверены, что хотите сбросить этот кеш?'),
(1081, 'ru', 'Категории статей'),
(1082, 'ru', 'Статьи'),
(1083, 'ru', 'Скрыть боковую панель'),
(1084, 'ru', 'Тема панели управления'),
(1085, 'ru', 'Коробочный шаблон панели управления'),
(1086, 'ru', 'Использование CPU'),
(1087, 'ru', 'Кеш'),
(1088, 'ru', 'Запись была успешно удалена из кеша'),
(1089, 'ru', 'Кеш был успешно сброшен'),
(1090, 'ru', 'Виджеты карусели'),
(1091, 'ru', 'Слайд был успешно сохранён'),
(1092, 'ru', 'Категория'),
(1093, 'ru', 'Очистить'),
(1094, 'ru', 'Контент'),
(1095, 'ru', 'Количество'),
(1096, 'ru', 'Создать'),
(1097, 'ru', 'Создание {modelClass}'),
(1098, 'ru', 'Тип БД'),
(1099, 'ru', 'Версия БД'),
(1100, 'ru', 'Дата'),
(1101, 'ru', 'Удалить'),
(1102, 'ru', 'Удалить значение по ключу из кеша'),
(1103, 'ru', 'Неактивно'),
(1104, 'ru', 'Редактировать аккаунт'),
(1105, 'ru', 'Редактировать профиль'),
(1106, 'ru', 'Email'),
(1107, 'ru', 'Активно'),
(1108, 'ru', 'Ошибка #{id}'),
(1109, 'ru', 'Ошибка {code}'),
(1110, 'ru', 'Событие'),
(1111, 'ru', 'Внешний IP'),
(1112, 'ru', 'Женский'),
(1113, 'ru', 'Менеджер файлов'),
(1114, 'ru', 'Файловое хранилище'),
(1115, 'ru', 'Записи о файлах'),
(1116, 'ru', 'Файлов в хранилище'),
(1117, 'ru', 'Фиксированный шаблон панели управления'),
(1118, 'ru', 'Сбросить'),
(1119, 'ru', 'Свободно Swap'),
(1120, 'ru', 'Свободно памяти'),
(1121, 'ru', 'Сервисный режим фронтенд части'),
(1122, 'ru', 'Привет, {username}'),
(1123, 'ru', 'Имя хоста'),
(1124, 'ru', 'I18N переводы'),
(1125, 'ru', 'Тексты'),
(1126, 'ru', 'ID'),
(1127, 'ru', 'Если вы оставите это поле пустым, ЧПУ будет сгенерирован автоматически'),
(1128, 'ru', 'Неправильный логин или пароль'),
(1129, 'ru', 'Внутренний IP'),
(1130, 'ru', 'Сбросить тег'),
(1131, 'ru', 'Версия ядра'),
(1132, 'ru', 'Ключ'),
(1133, 'ru', 'Записи'),
(1134, 'ru', 'Ключ-Значение'),
(1135, 'ru', 'Язык'),
(1136, 'ru', 'Уровень'),
(1137, 'ru', 'Средняя нагрузка'),
(1138, 'ru', 'Время события'),
(1139, 'ru', 'Выход'),
(1140, 'ru', 'Журнал'),
(1141, 'ru', 'Главная'),
(1142, 'ru', 'Мужской'),
(1143, 'ru', 'Участник с {0, date, short}'),
(1144, 'ru', 'Память'),
(1145, 'ru', 'Использование памяти'),
(1146, 'ru', 'Виджеты меню'),
(1147, 'ru', 'Сообщение'),
(1148, 'ru', 'Подробнее'),
(1149, 'ru', 'Сеть'),
(1150, 'ru', 'Новый пользователь ({identity}) был зарегистрирован {created_at}'),
(1151, 'ru', 'Событий нет'),
(1152, 'ru', 'Не опубликовано'),
(1153, 'ru', 'Кол-во ядер'),
(1154, 'ru', 'ОС'),
(1155, 'ru', 'Версия ОС'),
(1156, 'ru', 'Выкл'),
(1157, 'ru', 'Вкл'),
(1158, 'ru', 'Операционная система'),
(1159, 'ru', 'Остальные'),
(1160, 'ru', 'Версия РHP'),
(1161, 'ru', 'Страницы'),
(1162, 'ru', 'Пароль'),
(1163, 'ru', 'Подтверждение пароля'),
(1164, 'ru', 'Порт'),
(1165, 'ru', 'Префикс'),
(1166, 'ru', 'Процессор'),
(1167, 'ru', 'Архитектура процессора'),
(1168, 'ru', 'Профиль'),
(1169, 'ru', 'Опубликовано'),
(1170, 'ru', 'В режиме реального времени'),
(1171, 'ru', 'Запомнить меня'),
(1172, 'ru', 'Сбросить'),
(1173, 'ru', 'Сохранить'),
(1174, 'ru', 'Поиск'),
(1175, 'ru', 'Выберите кеш'),
(1176, 'ru', 'Настройки были успешно сохранены'),
(1177, 'ru', 'Вход'),
(1178, 'ru', 'Войти'),
(1179, 'ru', 'ПО'),
(1180, 'ru', 'Извините, но приложению не удалось собрать информацию о вашей системе. Смотрите {link}.'),
(1181, 'ru', 'Текст'),
(1182, 'ru', 'Статические страницы'),
(1183, 'ru', 'Система'),
(1184, 'ru', 'Системная дата'),
(1185, 'ru', 'Информация о системе'),
(1186, 'ru', 'Системный журнал'),
(1187, 'ru', 'Системное время'),
(1188, 'ru', 'Тег'),
(1189, 'ru', 'TagDependency был инвалидирован'),
(1190, 'ru', 'Текстовые блоки'),
(1191, 'ru', 'Текстовые виджеты'),
(1192, 'ru', 'Этот email уже кем-то используется.'),
(1193, 'ru', 'Это имя пользователя уже занято.'),
(1194, 'ru', 'Время'),
(1195, 'ru', 'Хроника'),
(1196, 'ru', 'Часовой пояс'),
(1197, 'ru', 'Навигация'),
(1198, 'ru', 'Общий Swap'),
(1199, 'ru', 'Общая память'),
(1200, 'ru', 'Перевод'),
(1201, 'ru', 'Редактировать'),
(1202, 'ru', 'Редактирование {modelClass}: '),
(1203, 'ru', 'Uptime'),
(1204, 'ru', 'Использованный размер'),
(1205, 'ru', 'Регистраций'),
(1206, 'ru', 'Имя пользователя'),
(1207, 'ru', 'Пользователи'),
(1208, 'ru', 'Смотреть всё'),
(1209, 'ru', 'Просмотр пользователя'),
(1210, 'ru', 'Веб-сервер'),
(1211, 'ru', 'Слайды'),
(1212, 'ru', 'Виджет карусели'),
(1213, 'ru', 'Виджеты меню'),
(1214, 'ru', 'У вас новое событие'),
(1215, 'ru', 'У вас новый пользователь!'),
(1216, 'ru', 'У вас {num} записей в журнале'),
(1217, 'ru', 'Ваш аккаунт был успешно сохранён'),
(1218, 'ru', 'Ваш профиль был успешно сохранён'),
(1219, 'ru', 'I18N'),
(1220, 'ru', 'Перевод'),
(1221, 'ru', 'Текст'),
(1222, 'ru', '\"{attribute}\" должен содержать валидный JSON'),
(1223, 'ru', 'Активно'),
(1224, 'ru', 'Токен доступа к API'),
(1225, 'ru', 'ID Статьи'),
(1226, 'ru', 'Шаблон статьи'),
(1227, 'ru', 'Автор'),
(1228, 'ru', 'Базовый URL'),
(1229, 'ru', 'Базовый Url'),
(1230, 'ru', 'Текст'),
(1231, 'ru', 'Текст'),
(1232, 'ru', 'ID карусели'),
(1233, 'ru', 'Категория'),
(1234, 'ru', 'Комментарий'),
(1235, 'ru', 'Компонент'),
(1236, 'ru', 'Настройки'),
(1237, 'ru', 'Создано'),
(1238, 'ru', 'Создано'),
(1239, 'ru', 'Удалено'),
(1240, 'ru', 'Закрыто на обслуживание.'),
(1241, 'ru', 'E-mail'),
(1242, 'ru', 'Email'),
(1243, 'ru', 'Истекает'),
(1244, 'ru', 'Тип файла'),
(1245, 'ru', 'Имя'),
(1246, 'ru', 'Пол'),
(1247, 'ru', 'ID'),
(1248, 'ru', 'Изображение'),
(1249, 'ru', 'Ключ'),
(1250, 'ru', 'Последний вход'),
(1251, 'ru', 'Фамилия'),
(1252, 'ru', 'Локаль'),
(1253, 'ru', 'Отчество'),
(1254, 'ru', 'Имя'),
(1255, 'ru', 'Неактивно'),
(1256, 'ru', 'Порядок'),
(1257, 'ru', 'Шаблон страницы'),
(1258, 'ru', 'Родительская категория'),
(1259, 'ru', 'Пароль'),
(1260, 'ru', 'Путь'),
(1261, 'ru', 'Аватар'),
(1262, 'ru', 'Опубликовано'),
(1263, 'ru', 'Дата публикации'),
(1264, 'ru', 'Роли'),
(1265, 'ru', 'Размер'),
(1266, 'ru', 'ЧПУ'),
(1267, 'ru', 'Статус'),
(1268, 'ru', 'Эскиз изображения'),
(1269, 'ru', 'Название***'),
(1270, 'ru', 'Токен'),
(1271, 'ru', 'Тип'),
(1272, 'ru', 'Обновлено'),
(1273, 'ru', 'Последнее обновление'),
(1274, 'ru', 'Обновивший'),
(1275, 'ru', 'IP'),
(1276, 'ru', 'Url'),
(1277, 'ru', 'ID пользователя'),
(1278, 'ru', 'Имя пользователя'),
(1279, 'ru', 'Значение'),
(1280, 'ru', 'Тематика'),
(1281, 'ru', 'Участников'),
(1282, 'ru', 'Цена'),
(1283, 'ru', 'Описание'),
(1284, 'ru', 'О нас'),
(1285, 'ru', 'Письмо активации'),
(1286, 'ru', 'Настройки аккаунта'),
(1287, 'ru', 'Статьи'),
(1288, 'ru', 'Приложения'),
(1289, 'ru', 'Панель управления'),
(1290, 'ru', 'Сообщение'),
(1291, 'ru', 'Проверьте ваш e-mail.'),
(1292, 'ru', 'Подтвердите пароль'),
(1293, 'ru', 'Контакты'),
(1294, 'ru', 'E-mail'),
(1295, 'ru', 'Email'),
(1296, 'ru', 'Ошибка в процессе OAuth авторизации.'),
(1297, 'ru', 'Женский'),
(1298, 'ru', 'Главная'),
(1299, 'ru', 'Если вы забыли пароль, вы можете сбросить его <a href=\"{link}\">здесь</a>'),
(1300, 'ru', 'Неправильный логин или пароль.'),
(1301, 'ru', 'Язык'),
(1302, 'ru', 'Войти с помощью'),
(1303, 'ru', 'Вход'),
(1304, 'ru', 'Выход'),
(1305, 'ru', 'Мужской'),
(1306, 'ru', 'Имя'),
(1307, 'ru', 'Нужен аккаунт? Зарегистрируйтесь'),
(1308, 'ru', 'Новый пароль был сохранён'),
(1309, 'ru', 'Страница не найдена'),
(1310, 'ru', 'Пароль'),
(1311, 'ru', 'Сброс пароля для {name}'),
(1312, 'ru', 'Настройки профиля'),
(1313, 'ru', 'Запомнить меня'),
(1314, 'ru', 'Запрос сброса пароля'),
(1315, 'ru', 'Сброс пароля'),
(1316, 'ru', 'Настройки'),
(1317, 'ru', 'Создать аккаунт с помощью'),
(1318, 'ru', 'Регистрация'),
(1319, 'ru', 'Извините, мы не можем сбросить пароль для этого e-mail.'),
(1320, 'ru', 'Тема'),
(1321, 'ru', 'Отправить'),
(1322, 'ru', 'Спасибо. Мы свяжемся с Вами в ближайшее время.'),
(1323, 'ru', 'Ошибка при отправке сообщения.'),
(1324, 'ru', 'Этот e-mail уже занят.'),
(1325, 'ru', 'Этот e-mail уже занят.'),
(1326, 'ru', 'Это имя пользователя уже занято.'),
(1327, 'ru', 'Редактировать'),
(1328, 'ru', 'Настройки пользователя'),
(1329, 'ru', 'Имя пользователя'),
(1330, 'ru', 'Имя пользователя или e-mail'),
(1331, 'ru', 'Проверочный код'),
(1332, 'ru', 'Пользователь с e-mail {email} уже зарегистрирован.'),
(1333, 'ru', 'Добро пожаловать в {app-name}. E-mail с информацией о пользователе был отправлен на вашу почту.'),
(1334, 'ru', 'Ваш аккаунт был успешно активирован. '),
(1335, 'ru', 'Ваш аккаунт был успешно создан. Проверьте ваш e-mail.'),
(1336, 'ru', 'Ваш аккаунт был успешно сохранён'),
(1337, 'ru', 'Ссылка активации: {url}'),
(1338, 'ru', '{app-name} | Информация о пользователе'),
(1339, 'ru', 'Тематика'),
(1340, 'ru', 'Участников'),
(1342, 'ru', 'Цена'),
(1343, 'ru', 'Канал'),
(1344, 'ru', 'Заказ'),
(1345, 'ru', 'Поиск'),
(1346, 'ru', 'Сбросить'),
(1347, 'ru', 'Заказать');

-- --------------------------------------------------------

--
-- Структура таблицы `i18n_source_message`
--

CREATE TABLE `i18n_source_message` (
  `id` int(11) NOT NULL,
  `category` varchar(32) DEFAULT NULL,
  `message` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `i18n_source_message`
--

INSERT INTO `i18n_source_message` (`id`, `category`, `message`) VALUES
(1075, 'backend', 'Account'),
(1076, 'backend', 'Application'),
(1077, 'backend', 'Application settings'),
(1078, 'backend', 'Application timeline'),
(1079, 'backend', 'Are you sure you want to delete this item?'),
(1080, 'backend', 'Are you sure you want to flush this cache?'),
(1081, 'backend', 'Article Categories'),
(1082, 'backend', 'Articles'),
(1083, 'backend', 'Backend sidebar collapsed'),
(1084, 'backend', 'Backend theme'),
(1085, 'backend', 'Boxed backend layout'),
(1086, 'backend', 'CPU Usage'),
(1087, 'backend', 'Cache'),
(1088, 'backend', 'Cache entry has been successfully deleted'),
(1089, 'backend', 'Cache has been successfully flushed'),
(1090, 'backend', 'Carousel Widgets'),
(1091, 'backend', 'Carousel slide was successfully saved'),
(1092, 'backend', 'Category'),
(1093, 'backend', 'Clear'),
(1094, 'backend', 'Content'),
(1095, 'backend', 'Count'),
(1096, 'backend', 'Create'),
(1097, 'backend', 'Create {modelClass}'),
(1098, 'backend', 'DB Type'),
(1099, 'backend', 'DB Version'),
(1100, 'backend', 'Date'),
(1101, 'backend', 'Delete'),
(1102, 'backend', 'Delete a value with the specified key from cache'),
(1103, 'backend', 'Disabled'),
(1104, 'backend', 'Edit account'),
(1105, 'backend', 'Edit profile'),
(1106, 'backend', 'Email'),
(1107, 'backend', 'Enabled'),
(1108, 'backend', 'Error #{id}'),
(1109, 'backend', 'Error {code}'),
(1110, 'backend', 'Event'),
(1111, 'backend', 'External IP'),
(1112, 'backend', 'Female'),
(1113, 'backend', 'File Manager'),
(1114, 'backend', 'File Storage'),
(1115, 'backend', 'File Storage Items'),
(1116, 'backend', 'Files in storage'),
(1117, 'backend', 'Fixed backend layout'),
(1118, 'backend', 'Flush'),
(1119, 'backend', 'Free Swap'),
(1120, 'backend', 'Free memory'),
(1121, 'backend', 'Frontend maintenance mode'),
(1122, 'backend', 'Hello, {username}'),
(1123, 'backend', 'Hostname'),
(1124, 'backend', 'I18n Messages'),
(1125, 'backend', 'I18n Source Messages'),
(1126, 'backend', 'ID'),
(1127, 'backend', 'If you leave this field empty, the slug will be generated automatically'),
(1128, 'backend', 'Incorrect username or password.'),
(1129, 'backend', 'Internal IP'),
(1130, 'backend', 'Invalidate tag'),
(1131, 'backend', 'Kernel version'),
(1132, 'backend', 'Key'),
(1133, 'backend', 'Key Storage Items'),
(1134, 'backend', 'Key-Value Storage'),
(1135, 'backend', 'Language'),
(1136, 'backend', 'Level'),
(1137, 'backend', 'Load average'),
(1138, 'backend', 'Log Time'),
(1139, 'backend', 'Logout'),
(1140, 'backend', 'Logs'),
(1141, 'backend', 'Main'),
(1142, 'backend', 'Male'),
(1143, 'backend', 'Member since {0, date, short}'),
(1144, 'backend', 'Memory'),
(1145, 'backend', 'Memory Usage'),
(1146, 'backend', 'Menu Widgets'),
(1147, 'backend', 'Message'),
(1148, 'backend', 'More info'),
(1149, 'backend', 'Network'),
(1150, 'backend', 'New user ({identity}) was registered at {created_at}'),
(1151, 'backend', 'No events found'),
(1152, 'backend', 'Not Published'),
(1153, 'backend', 'Number of cores'),
(1154, 'backend', 'OS'),
(1155, 'backend', 'OS Release'),
(1156, 'backend', 'Off'),
(1157, 'backend', 'On'),
(1158, 'backend', 'Operating System'),
(1159, 'backend', 'Other'),
(1160, 'backend', 'PHP Version'),
(1161, 'backend', 'Pages'),
(1162, 'backend', 'Password'),
(1163, 'backend', 'Password Confirm'),
(1164, 'backend', 'Port'),
(1165, 'backend', 'Prefix'),
(1166, 'backend', 'Processor'),
(1167, 'backend', 'Processor Architecture'),
(1168, 'backend', 'Profile'),
(1169, 'backend', 'Published'),
(1170, 'backend', 'Real time'),
(1171, 'backend', 'Remember Me'),
(1172, 'backend', 'Reset'),
(1173, 'backend', 'Save'),
(1174, 'backend', 'Search'),
(1175, 'backend', 'Select cache'),
(1176, 'backend', 'Settings was successfully saved'),
(1177, 'backend', 'Sign In'),
(1178, 'backend', 'Sign me in'),
(1179, 'backend', 'Software'),
(1180, 'backend', 'Sorry, application failed to collect information about your system. See {link}.'),
(1181, 'backend', 'Source Message'),
(1182, 'backend', 'Static pages'),
(1183, 'backend', 'System'),
(1184, 'backend', 'System Date'),
(1185, 'backend', 'System Information'),
(1186, 'backend', 'System Logs'),
(1187, 'backend', 'System Time'),
(1188, 'backend', 'Tag'),
(1189, 'backend', 'TagDependency was invalidated'),
(1190, 'backend', 'Text Blocks'),
(1191, 'backend', 'Text Widgets'),
(1192, 'backend', 'This email has already been taken.'),
(1193, 'backend', 'This username has already been taken.'),
(1194, 'backend', 'Time'),
(1195, 'backend', 'Timeline'),
(1196, 'backend', 'Timezone'),
(1197, 'backend', 'Toggle navigation'),
(1198, 'backend', 'Total Swap'),
(1199, 'backend', 'Total memory'),
(1200, 'backend', 'Translation'),
(1201, 'backend', 'Update'),
(1202, 'backend', 'Update {modelClass}: '),
(1203, 'backend', 'Uptime'),
(1204, 'backend', 'Used size'),
(1205, 'backend', 'User Registrations'),
(1206, 'backend', 'Username'),
(1207, 'backend', 'Users'),
(1208, 'backend', 'View all'),
(1209, 'backend', 'View user'),
(1210, 'backend', 'Web Server'),
(1211, 'backend', 'Widget Carousel Items'),
(1212, 'backend', 'Widget Carousels'),
(1213, 'backend', 'Widget Menus'),
(1214, 'backend', 'You have new event'),
(1215, 'backend', 'You have new user!'),
(1216, 'backend', 'You have {num} log items'),
(1217, 'backend', 'Your account has been successfully saved'),
(1218, 'backend', 'Your profile has been successfully saved'),
(1219, 'backend', 'i18n'),
(1220, 'backend', 'i18n Message'),
(1221, 'backend', 'i18n Source Message'),
(1222, 'common', '\"{attribute}\" must be a valid JSON'),
(1223, 'common', 'Active'),
(1224, 'common', 'API access token'),
(1225, 'common', 'Article ID'),
(1226, 'common', 'Article View'),
(1227, 'common', 'Author'),
(1228, 'common', 'Base URL'),
(1229, 'common', 'Base Url'),
(1230, 'common', 'Body'),
(1231, 'common', 'Caption'),
(1232, 'common', 'Carousel ID'),
(1233, 'common', 'Category'),
(1234, 'common', 'Comment'),
(1235, 'common', 'Component'),
(1236, 'common', 'Config'),
(1237, 'common', 'Created At'),
(1238, 'common', 'Created at'),
(1239, 'common', 'Deleted'),
(1240, 'common', 'Down to maintenance.'),
(1241, 'common', 'E-mail'),
(1242, 'common', 'Email'),
(1243, 'common', 'Expire At'),
(1244, 'common', 'File Type'),
(1245, 'common', 'Firstname'),
(1246, 'common', 'Gender'),
(1247, 'common', 'ID'),
(1248, 'common', 'Image'),
(1249, 'common', 'Key'),
(1250, 'common', 'Last login'),
(1251, 'common', 'Lastname'),
(1252, 'common', 'Locale'),
(1253, 'common', 'Middlename'),
(1254, 'common', 'Name'),
(1255, 'common', 'Not Active'),
(1256, 'common', 'Order'),
(1257, 'common', 'Page View'),
(1258, 'common', 'Parent Category'),
(1259, 'common', 'Password'),
(1260, 'common', 'Path'),
(1261, 'common', 'Picture'),
(1262, 'common', 'Published'),
(1263, 'common', 'Published At'),
(1264, 'common', 'Roles'),
(1265, 'common', 'Size'),
(1266, 'common', 'Slug'),
(1267, 'common', 'Status'),
(1268, 'common', 'Thumbnail'),
(1269, 'common', 'Title'),
(1270, 'common', 'Token'),
(1271, 'common', 'Type'),
(1272, 'common', 'Updated At'),
(1273, 'common', 'Updated at'),
(1274, 'common', 'Updater'),
(1275, 'common', 'Upload Ip'),
(1276, 'common', 'Url'),
(1277, 'common', 'User ID'),
(1278, 'common', 'Username'),
(1279, 'common', 'Value'),
(1280, 'common', 'Category ID'),
(1281, 'common', 'Users'),
(1282, 'common', 'Price'),
(1283, 'common', 'Channel Description'),
(1284, 'frontend', 'About'),
(1285, 'frontend', 'Activation email'),
(1286, 'frontend', 'Account Settings'),
(1287, 'frontend', 'Articles'),
(1288, 'frontend', 'Attachments'),
(1289, 'frontend', 'Backend'),
(1290, 'frontend', 'Body'),
(1291, 'frontend', 'Check your email for further instructions.'),
(1292, 'frontend', 'Confirm Password'),
(1293, 'frontend', 'Contact'),
(1294, 'frontend', 'E-mail'),
(1295, 'frontend', 'Email'),
(1296, 'frontend', 'Error while oauth process.'),
(1297, 'frontend', 'Female'),
(1298, 'frontend', 'Home'),
(1299, 'frontend', 'If you forgot your password you can reset it <a href=\"{link}\">here</a>'),
(1300, 'frontend', 'Incorrect username or password.'),
(1301, 'frontend', 'Language'),
(1302, 'frontend', 'Log in with'),
(1303, 'frontend', 'Login'),
(1304, 'frontend', 'Logout'),
(1305, 'frontend', 'Male'),
(1306, 'frontend', 'Name'),
(1307, 'frontend', 'Need an account? Sign up.'),
(1308, 'frontend', 'New password was saved.'),
(1309, 'frontend', 'Page not found'),
(1310, 'frontend', 'Password'),
(1311, 'frontend', 'Password reset for {name}'),
(1312, 'frontend', 'Profile settings'),
(1313, 'frontend', 'Remember Me'),
(1314, 'frontend', 'Request password reset'),
(1315, 'frontend', 'Reset password'),
(1316, 'frontend', 'Settings'),
(1317, 'frontend', 'Sign up with'),
(1318, 'frontend', 'Signup'),
(1319, 'frontend', 'Sorry, we are unable to reset password for email provided.'),
(1320, 'frontend', 'Subject'),
(1321, 'frontend', 'Submit'),
(1322, 'frontend', 'Thank you for contacting us. We will respond to you as soon as possible.'),
(1323, 'frontend', 'There was an error sending email.'),
(1324, 'frontend', 'This email address has already been taken.'),
(1325, 'frontend', 'This email has already been taken.'),
(1326, 'frontend', 'This username has already been taken.'),
(1327, 'frontend', 'Update'),
(1328, 'frontend', 'User Settings'),
(1329, 'frontend', 'Username'),
(1330, 'frontend', 'Username or email'),
(1331, 'frontend', 'Verification Code'),
(1332, 'frontend', 'We already have a user with email {email}'),
(1333, 'frontend', 'Welcome to {app-name}. Email with your login information was sent to your email.'),
(1334, 'frontend', 'Your account has been successfully activated.'),
(1335, 'frontend', 'Your account has been successfully created. Check your email for further instructions.'),
(1336, 'frontend', 'Your account has been successfully saved'),
(1337, 'frontend', 'Your activation link: {url}'),
(1338, 'frontend', '{app-name} | Your login information'),
(1339, 'frontend', 'Category ID'),
(1340, 'frontend', 'Users'),
(1342, 'frontend', 'Exchange price'),
(1343, 'frontend', 'Channel'),
(1344, 'frontend', 'Order'),
(1345, 'frontend', 'Search'),
(1346, 'frontend', 'Reset'),
(1347, 'frontend', 'To order');

-- --------------------------------------------------------

--
-- Структура таблицы `key_storage_item`
--

CREATE TABLE `key_storage_item` (
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `comment` text,
  `updated_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `key_storage_item`
--

INSERT INTO `key_storage_item` (`key`, `value`, `comment`, `updated_at`, `created_at`) VALUES
('backend.layout-boxed', '0', NULL, NULL, NULL),
('backend.layout-collapsed-sidebar', '0', NULL, NULL, NULL),
('backend.layout-fixed', '0', NULL, NULL, NULL),
('backend.theme-skin', 'skin-blue', 'skin-blue, skin-black, skin-purple, skin-green, skin-red, skin-yellow', NULL, NULL),
('frontend.maintenance', 'disabled', 'Set it to \"enabled\" to turn on maintenance mode', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `order_id` int(11) UNSIGNED DEFAULT NULL,
  `type` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `recipient_id` int(11) UNSIGNED NOT NULL,
  `content` text NOT NULL,
  `sent_at` int(11) UNSIGNED NOT NULL,
  `reply_to` int(11) UNSIGNED DEFAULT NULL,
  `status` smallint(6) UNSIGNED NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `message`
--

INSERT INTO `message` (`id`, `order_id`, `type`, `user_id`, `recipient_id`, `content`, `sent_at`, `reply_to`, `status`, `created_at`, `updated_at`) VALUES
(1, 35, 1, 2, 1, 'Другое<br>gfhfhfghfghfghfghfhfg', 0, NULL, 1, NULL, NULL),
(4, 97, 7, 3, 1, 'Не тематическая реклама', 0, NULL, 1, NULL, NULL),
(5, 95, 7, 3, 4, 'guytferugurekhtgert', 0, NULL, 1, NULL, NULL),
(6, 96, 7, 3, 4, 'Нет свободного места, предложить другую дату', 0, NULL, 1, NULL, NULL),
(7, 96, 7, 3, 4, 'Нет свободного места, предложить другую дату', 0, NULL, 1, NULL, NULL),
(8, 98, 7, 3, 1, 'Нет свободного места, предложить другую дату', 0, NULL, 1, NULL, NULL),
(9, 98, 7, 3, 1, 'Нет свободного места, предложить другую дату', 0, NULL, 1, NULL, NULL),
(10, 98, 7, 3, 1, 'Нет свободного места, предложить другую дату', 0, NULL, 1, NULL, NULL),
(11, 98, 7, 3, 1, 'Нет свободного места, предложить другую дату', 0, NULL, 1, NULL, NULL),
(12, 98, 7, 3, 1, 'Нет свободного места, предложить другую дату', 0, NULL, 1, NULL, NULL),
(13, 98, 7, 3, 1, 'Нет свободного места, предложить другую дату', 0, NULL, 1, NULL, NULL),
(14, 98, 7, 3, 1, 'Нет свободного места можно с 2020-05-08 по 2020-05-08', 0, NULL, 1, NULL, NULL),
(15, 99, 7, 3, 1, 'other message', 0, NULL, 1, NULL, NULL),
(16, 100, 7, 3, 1, 'Нет свободного места можно с 2020-04-08 по 2020-04-24', 0, NULL, 1, NULL, NULL),
(17, 100, 7, 3, 1, 'Нет свободного места можно с 2020-04-08 по 2020-04-24', 0, NULL, 1, NULL, NULL),
(18, 100, 7, 3, 1, 'Нет свободного места можно с 2020-04-08 по 2020-04-24', 0, NULL, 1, NULL, NULL),
(19, 102, 7, 3, 4, 'sdfdsfsdfsfsdfsf', 0, NULL, 1, NULL, NULL),
(20, 103, 7, 3, 4, 'Отмена проекта по личным причинам', 0, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `money`
--

CREATE TABLE `money` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `text` varchar(1024) NOT NULL,
  `currency` varchar(512) NOT NULL,
  `type` smallint(6) NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `email_confirm_token` varchar(255) DEFAULT NULL,
  `requisites` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `money`
--

INSERT INTO `money` (`id`, `user_id`, `order_id`, `text`, `currency`, `type`, `value`, `status`, `created_at`, `updated_at`, `currency_id`, `email_confirm_token`, `requisites`) VALUES
(1, 4, 234234, 'admin add cash', '1', 2, 20, 1, NULL, 1593442909, 28, NULL, NULL),
(2, 2, 123123, 'admin add cash', '', 4, 3000, 1, 1594238064, 1594238064, 29, NULL, NULL),
(3, 2, 0, 'Добавление тестового платежа пользоателю', '', 1, 6000, 3, 1594311871, 1594311871, 30, NULL, NULL),
(4, 1, 0, 'Добавление тестового платежа пользоателю', '', 2, 6500, 1, 1594312012, 1594322862, 30, NULL, NULL),
(5, 3, 0, 'Пополнение баланса пользователем', '', 1, 400, 1, 1594909241, 1594909241, 30, NULL, NULL),
(6, 3, 0, 'Пополнение баланса пользователем', '', 1, 500, 1, 1594913176, 1594913176, 30, NULL, NULL),
(7, 3, 0, 'Пополнение баланса пользователем', '', 1, 600, 1, 1594913541, 1594913541, 30, NULL, NULL),
(8, 3, 0, 'Пополнение баланса пользователем', '', 1, 600, 1, 1594913542, 1594913542, 30, NULL, NULL),
(9, 3, 0, 'Пополнение баланса пользователем', '', 1, 300, 1, 1594913561, 1594913561, 30, NULL, NULL),
(10, 4, 0, 'Пополнение баланса пользователем', '', 1, 300, 1, 1598383758, 1598383758, 30, NULL, NULL),
(16, 3, 0, 'Пополнение баланса пользователем', '', 2, 3000, 1, 1602501640, 1602585893, 30, NULL, NULL),
(17, 4, 0, 'Добавление тестового платежа пользоателю', '', 2, 6000, 1, 1602586086, 1602586086, 30, NULL, NULL),
(18, 3, 0, 'Добавление тестового платежа пользоателю', '', 2, 6000, 1, 1602586113, 1602586113, 30, NULL, NULL),
(19, 3, 0, 'Пополнение баланса пользователем', '', 1, 30000, 3, 1602586194, 1602586194, 30, NULL, NULL),
(20, 3, 0, 'Вывод средств пользователем', '', 6, 1795, 3, 1602586276, 1602586276, 30, NULL, NULL),
(21, 3, 0, 'Вывод средств пользователем', '', 6, 30000, 3, 1602756153, 1602756153, 30, NULL, NULL),
(22, 3, 0, 'Пополнение баланса пользователем', '', 1, 300, 3, 1602756632, 1602756632, 30, NULL, NULL),
(23, 3, 0, 'Пополнение баланса пользователем', '', 1, 5000, 3, 1602756662, 1602756662, 30, NULL, NULL),
(24, 3, 0, 'Вывод средств пользователем', '', 6, 110, 3, 1602758565, 1602758565, 30, NULL, NULL),
(25, 3, 0, 'Вывод средств пользователем', '', 6, 790, 3, 1602758596, 1602758596, 30, NULL, NULL),
(26, 3, 0, 'Пополнение баланса пользователем', '', 1, 2000, 3, 1602758671, 1602758671, 30, NULL, NULL),
(27, 3, 0, 'Вывод средств пользователем', '', 6, 500, 3, 1603275821, 1603275821, 30, 'uKfwDHfB3CITHmQNC3xoZKhrnR530FXi', NULL),
(28, 3, 0, 'Вывод средств пользователем', '', 6, 500, 3, 1603275891, 1603275891, 30, 'Ugk9P9uTod5KGgVC-XgeqYPUs7rkmYvO', NULL),
(29, 3, 0, 'Вывод средств пользователем', '', 6, 500, 3, 1603276109, 1603276109, 30, '1SFqbUn3HEGLm2Cbgz3BoPR6bGDq4bv7', NULL),
(30, 3, 0, 'Вывод средств пользователем', '', 6, 101, 3, 1603276173, 1603276173, 30, 'JxOA0GxgBk0opCcciuTwCd1PUr2Oo-s5', NULL),
(31, 3, 0, 'Вывод средств пользователем', '', 6, 101, 3, 1603276293, 1603276293, 30, 'z_FFtS-obXR72poQn6JKfq7sL3uhfJJp', NULL),
(32, 3, 0, 'Вывод средств пользователем', '', 6, 101, 3, 1603276436, 1603276436, 30, 'j2hJdfQJobbux0l-MYY2oVOhOlH1WAQ0', NULL),
(33, 3, 0, 'Вывод средств пользователем', '', 6, 101, 3, 1603277273, 1603277273, 30, 'yJwdOZm_nbZwIYKVqloWR0oghZGJ7sK3', NULL),
(34, 3, 0, 'Вывод средств пользователем', '', 6, 101, 3, 1603277320, 1603277320, 30, 'fQw3-UvH583-AuvJWbEtrIpDFcqPcPoa', NULL),
(35, 3, 0, 'Вывод средств пользователем', '', 6, 101, 3, 1603277397, 1603277397, 30, '8S-vVwzLMvwb756len0egF9Q3t8E-0CU', NULL),
(36, 3, 0, 'Вывод средств пользователем', '', 6, 101, 3, 1603277515, 1603277515, 30, 'FStkUf8MMRJz4NVdJxYD_pidGZD7yVjG', NULL),
(37, 3, 0, 'Вывод средств пользователем', '', 6, 101, 3, 1603277559, 1603277559, 30, 'zLuSikDtaqCOj15_fpJXbVXwQVk8-Uk_', NULL),
(38, 3, 0, 'Пополнение баланса пользователем', '', 1, 3000, 3, 1603884687, 1603884687, 30, NULL, NULL),
(39, 3, 0, 'Вывод средств пользователем', '', 6, 565, 3, 1604003726, 1604003726, 30, 'X3BWToBNZnIW2IyBjf_IHaqEAS2-A-pL', NULL),
(40, 3, 0, 'Вывод средств пользователем', '', 6, 565, 3, 1604003732, 1604003732, 30, '04sx9FY-dUdTxQSfnTMs-4NlPPYIiLS_', NULL),
(41, 4, 0, 'admin add cash', '', 1, 355, 1, 1610999919, 1610999919, 30, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `key_id` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `seen` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `flashed` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `notification`
--

INSERT INTO `notification` (`id`, `key`, `key_id`, `type`, `user_id`, `seen`, `created_at`, `flashed`) VALUES
(13, 'new_order', '1', 'default', 4, 1, '2020-03-02 16:43:18', 1),
(14, 'new_order', '1', 'default', 4, 1, '2020-03-02 16:49:27', 1),
(15, 'take_to_work', '1', 'default', 4, 1, '2020-03-02 16:50:01', 1),
(16, 'new_order', '1', 'default', 4, 1, '2020-03-02 16:53:56', 1),
(17, 'take_to_work', '1', 'default', 3, 1, '2020-03-02 16:56:36', 1),
(18, 'new_order', '1', 'default', 4, 1, '2020-03-02 17:05:26', 1),
(19, 'take_to_work', '1', 'default', 3, 1, '2020-03-02 17:05:58', 1),
(21, 'new_order', '1', 'default', 4, 1, '2020-03-04 14:49:07', 1),
(22, 'new_order', '1', 'default', 4, 1, '2020-03-04 14:50:03', 1),
(23, 'new_order', '1', 'default', 4, 1, '2020-03-04 14:51:42', 1),
(24, 'new_order', '1', 'default', 4, 1, '2020-03-04 14:53:02', 1),
(25, 'new_order', '1', 'default', 4, 1, '2020-03-04 14:53:21', 1),
(26, 'new_order', '1', 'default', 4, 1, '2020-03-04 15:57:33', 1),
(27, 'new_order', '1', 'default', 4, 1, '2020-03-04 15:58:23', 1),
(28, 'new_order', '1', 'default', 4, 1, '2020-03-04 16:00:48', 1),
(29, 'new_order', '1', 'default', 4, 1, '2020-03-04 19:35:41', 1),
(30, 'new_order', '1', 'default', 4, 1, '2020-03-04 19:38:43', 1),
(31, 'new_order', '1', 'default', 4, 1, '2020-03-04 19:39:30', 1),
(32, 'new_order', '1', 'default', 4, 1, '2020-03-04 19:42:01', 1),
(33, 'new_order', '1', 'default', 4, 1, '2020-03-04 19:50:58', 1),
(35, 'new_order', '1', 'default', 3, 1, '2020-03-05 21:57:37', 1),
(36, 'take_to_work', '1', 'default', 4, 1, '2020-03-05 22:00:02', 1),
(37, 'cancelled_admin', '1', 'default', 4, 1, '2020-03-05 22:04:51', 1),
(38, 'cancelled_admin', '1', 'default', 2, 0, '2020-04-20 13:45:01', 0),
(39, 'cancelled_admin', '1', 'default', 2, 0, '2020-04-20 13:45:40', 0),
(41, 'cancelled_admin', '1', 'default', 1, 1, '2020-04-20 14:09:33', 1),
(42, 'new_order', '1', 'default', 3, 1, '2020-04-21 14:41:49', 1),
(45, 'new_order', '1', 'default', 3, 1, '2020-04-27 17:00:39', 1),
(46, 'new_order', '1', 'default', 3, 1, '2020-04-27 17:05:18', 1),
(47, 'new_order', '1', 'default', 3, 1, '2020-04-27 17:05:30', 1),
(48, 'new_order', '1', 'default', 3, 1, '2020-04-27 17:06:14', 1),
(53, 'new_order', '1', 'default', 3, 1, '2020-05-08 14:36:18', 1),
(55, 'new_order', '1', 'default', 3, 1, '2020-05-08 15:22:36', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `advertisement_description` text,
  `image_path` varchar(255) DEFAULT NULL,
  `published_at` int(11) DEFAULT NULL,
  `wishes` text,
  `author_id` int(11) NOT NULL,
  `your_link` varchar(1024) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`order_id`, `total_price`, `advertisement_description`, `image_path`, `published_at`, `wishes`, `author_id`, `your_link`, `status`, `created_at`, `updated_at`) VALUES
(58, 500, 'erterertet', '\\1\\MaCLp9Qvw84H0dsMyrF0diZhugeFz34H.jpg', 1565249822, 'ertertert', 3, 'http://telega-market.localhost/order/create12312', 0, 1582643530, 1582643530),
(59, 250, 'tryrtyry', '\\1\\VT4JNjHlaZH4qJUQuywneIpVXUUxOrZ_.jpg', 1565249822, 'wqewwewewr', 3, 'http://telega-market.localhost/order/create12312', 0, 1582644068, 1582644068),
(61, 250, 'ghjghjgh', '\\1\\TpNaOJ1GioKwG_Z_rXwFnW3Q-HwaF53N.jpg', 1566977822, 'fghfghfgh', 3, 'http://telega-market.localhost/order/create12312', 0, 1582715701, 1582715701),
(63, 250, 'uyuiyuiyui', '\\1\\o1K5V__XfOTsYF2U6hVEczpTH-50rJFv.jpg', 1566977822, 'yuiyuiy', 3, 'http://telega-market.localhost/order/create', 0, 1582716068, 1582716068),
(65, 250, 'fghgfhfgh', '\\1\\qcSB5k1NmZsqIAXygJrvU_2Dh6k8dzqj.jpg', 1571900972, 'fghfghf', 3, 'http://telega-market.localhost/order/create', 0, 1582719445, 1582719445),
(66, 250, '', NULL, 1582721084, '', 3, 'http://telega-market.localhost/order/create12312', 0, 1582721084, 1582721084),
(68, 250, 'ghghjg', '\\1\\yQucnri6fDnTpBMtKas6g6eYlmTxUG88.jpg', 1582721520, '', 3, 'http://telega-market.localhost/order/create12312', 0, 1582721520, 1582721520),
(69, 300, '12312', '\\1\\Yfphf8KcjmchVHU5C07wwPjel5aJTWEN.jpg', 1567504301, 'test', 3, 'http://telega-market.localhost/order/create', 0, 1583151141, 1583151141),
(70, 300, 'dfdfd', '\\1\\67ywzNYVy6QaIRAOpq5UIMKy68H8xA4K.jpg', 1566977822, 'dfdfgdgdf', 3, 'http://telega-market.localhost/order/create12312', 0, 1583161075, 1583161075),
(71, 300, 'dfdfd', '\\1\\67ywzNYVy6QaIRAOpq5UIMKy68H8xA4K.jpg', 1566977822, 'dfdfgdgdf', 3, 'http://telega-market.localhost/order/create12312', 0, 1583161112, 1583161112),
(72, 300, 'ddfdfsdf', '\\1\\0-1QLJuKcnSLMBIpGu6V6anGYHqL4Squ.jpg', 1567504301, 'sdfsdfsdfs', 3, 'vbnvbnvb', 0, 1583163798, 1583163798),
(73, 300, 'erert', '\\1\\LX8Bscz-czzVUkvro9vHoCFwjCrgt13m.jpg', 1566977822, 'ertertetrert', 3, 'http://telega-market.localhost/order/create', 0, 1583164167, 1583164167),
(74, 300, 'dfgdfgd', '\\1\\kjoHJ1ymQBkK8SIxR8vxbT6fQdjXvb3a.jpg', 1571900972, 'dfgdfgd', 3, 'http://telega-market.localhost/order/create12312', 0, 1583164436, 1583164436),
(75, 300, 'werwerw', '\\1\\XaEZG6SiRrBbvx0iMWiJSiSxvvM6T6pe.jpg', 1567504301, 'werwer', 3, 'http://telega-market.localhost/order/create', 0, 1583165125, 1583165125),
(76, 300, 'dfgdfg', '\\1\\v32hfoxL1rL0CG8GkFm1f61_CVOt1HM3.jpg', 1566977822, 'fghfghf', 3, 'http://telega-market.localhost/order/create12312', 0, 1583329747, 1583329747),
(77, 300, 'dfgdfg', '\\1\\v32hfoxL1rL0CG8GkFm1f61_CVOt1HM3.jpg', 1566977822, 'fghfghf', 3, 'http://telega-market.localhost/order/create12312', 0, 1583329803, 1583329803),
(78, 300, 'dfgdfg', '\\1\\v32hfoxL1rL0CG8GkFm1f61_CVOt1HM3.jpg', 1566977822, 'fghfghf', 3, 'http://telega-market.localhost/order/create12312', 0, 1583329902, 1583329902),
(79, 300, 'dfgdfg', '\\1\\v32hfoxL1rL0CG8GkFm1f61_CVOt1HM3.jpg', 1566977822, 'fghfghf', 3, 'http://telega-market.localhost/order/create12312', 0, 1583329969, 1583329969),
(80, 300, 'dfgdfg', '\\1\\v32hfoxL1rL0CG8GkFm1f61_CVOt1HM3.jpg', 1566977822, 'fghfghf', 3, 'http://telega-market.localhost/order/create12312', 0, 1583329982, 1583329982),
(81, 300, 'dfgdfg', '\\1\\v32hfoxL1rL0CG8GkFm1f61_CVOt1HM3.jpg', 1566977822, 'fghfghf', 3, 'http://telega-market.localhost/order/create12312', 0, 1583330001, 1583330001),
(82, 300, 'dfgdfg', '\\1\\v32hfoxL1rL0CG8GkFm1f61_CVOt1HM3.jpg', 1566977822, 'fghfghf', 3, 'http://telega-market.localhost/order/create12312', 0, 1583333853, 1583333853),
(83, 300, 'dfgdfg', '\\1\\v32hfoxL1rL0CG8GkFm1f61_CVOt1HM3.jpg', 1566977822, 'fghfghf', 3, 'http://telega-market.localhost/order/create12312', 0, 1583333903, 1583333903),
(84, 300, 'dfgdfg', '\\1\\v32hfoxL1rL0CG8GkFm1f61_CVOt1HM3.jpg', 1566977822, 'fghfghf', 3, 'http://telega-market.localhost/order/create12312', 0, 1583334048, 1583334048),
(85, 300, 'dfgdfg', '\\1\\jrNwsrR0fD1pu6zpToEg2_Uy_H1NVTh-.jpg', 1568796533, 'dfgdfgd', 3, 'http://telega-market.localhost/order/create12312', 0, 1583346941, 1583346941),
(86, 300, 'dfgdfg', '\\1\\jrNwsrR0fD1pu6zpToEg2_Uy_H1NVTh-.jpg', 1568796533, 'dfgdfgd', 3, 'http://telega-market.localhost/order/create12312', 0, 1583347123, 1583347123),
(87, 300, 'dfgdfg', '\\1\\jrNwsrR0fD1pu6zpToEg2_Uy_H1NVTh-.jpg', 1568796533, 'dfgdfgd', 3, 'http://telega-market.localhost/order/create12312', 0, 1583347170, 1583347170),
(88, 300, 'dfgdfg', '\\1\\jrNwsrR0fD1pu6zpToEg2_Uy_H1NVTh-.jpg', 1568796533, 'dfgdfgd', 3, 'http://telega-market.localhost/order/create12312', 0, 1583347321, 1583347321),
(89, 300, 'dfgdfg', '\\1\\jrNwsrR0fD1pu6zpToEg2_Uy_H1NVTh-.jpg', 1568796533, 'dfgdfgd', 3, 'http://telega-market.localhost/order/create12312', 0, 1583347858, 1583347858),
(90, 300, 'trrt', '\\1\\-fQ9s9P-ln2Pc6UkDhHiJvksv8Is7iQm.jpg', 1566977822, 'rtyrty', 3, 'http://telega-market.localhost/order/create12312', 0, 1583347945, 1583347945),
(91, 300, 'freerter', '\\1\\sKY_O0KsTv192ndbLQw197ryUXNDZgf_.jpg', 1568796533, 'ghgjghjghjg', 4, 'http://telega-market.localhost/order/create', 0, 1583441732, 1583441732),
(92, 300, 'freerter', '\\1\\sKY_O0KsTv192ndbLQw197ryUXNDZgf_.jpg', 1568796533, 'ghgjghjghjg', 4, 'http://telega-market.localhost/order/create', 0, 1583441761, 1583441761),
(93, 300, 'freerter', '\\1\\sKY_O0KsTv192ndbLQw197ryUXNDZgf_.jpg', 1568796533, 'ghgjghjghjg', 4, 'http://telega-market.localhost/order/create', 0, 1583441857, 1583441857),
(94, 300, 'hgghjghj', '\\1\\g2dozafpGNEYasA5bCLFIXvtF16OPNz_.jpg', 1566977822, 'ghghjghj', 4, 'http://telega-market.localhost/order/create', 0, 1587472909, 1587472909),
(95, 300, 'hgghjghj', '\\1\\g2dozafpGNEYasA5bCLFIXvtF16OPNz_.jpg', 1566977822, 'ghghjghj', 4, 'http://telega-market.localhost/order/create', 0, 1587473069, 1587473069),
(96, 300, 'dfgdfgdfg', '\\1\\Byo9S2fGK1vjv5XOebf_IDXGtKFFpELM.jpg', 1568885114, 'dfgdfgdg', 4, 'http://telega-market.localhost/order/create', 0, 1587477059, 1587477059),
(97, 200, 'смисмис', '\\1\\b0FXBO2YMXUbHCiOG5LdwoODVhugIUIC.jpg', 1566977822, 'миимитмитмитмитмитми пипап', 1, 'http://telega-market.localhost/order/create', 0, 1587732295, 1587732295),
(98, 300, 'sdfsdfs', '\\1\\W77eAMTBuOSlOHxYyNNW9AXgzTJz1wFT.jpg', 1565249822, 'dfgdf fgdfg dgdfgd dfgdfggdfg drtyt rtyr r', 1, 'http://telega-market.localhost/', 0, 1587999639, 1587999639),
(99, 300, 'sdfsdfs', '\\1\\W77eAMTBuOSlOHxYyNNW9AXgzTJz1wFT.jpg', 1565249822, 'dfgdf fgdfg dgdfgd dfgdfggdfg drtyt rtyr r', 1, 'http://telega-market.localhost/', 0, 1587999918, 1587999918),
(100, 300, 'sdfsdfs', '\\1\\W77eAMTBuOSlOHxYyNNW9AXgzTJz1wFT.jpg', 1565249822, 'dfgdf fgdfg dgdfgd dfgdfggdfg drtyt rtyr r', 1, 'http://telega-market.localhost/', 0, 1587999930, 1587999930),
(101, 300, 'sdfsdfs', '\\1\\W77eAMTBuOSlOHxYyNNW9AXgzTJz1wFT.jpg', 1565249822, 'dfgdf fgdfg dgdfgd dfgdfggdfg drtyt rtyr r', 1, 'http://telega-market.localhost/', 0, 1587999974, 1587999974),
(102, 300, 'dfdgdfgd', '\\1\\D6v3HM9rZb2BuGj13m-KWGlhSHizPIg-.jpg', 1566977822, 'dfgdsgfsdfg', 4, 'http://telega-market.localhost/order/create', 0, 1588941378, 1588941378),
(103, 300, 'hjhjkhjk', '\\1\\6rcbNLKIERm70Jrd7deVzu6fOVsOyHAH.jpg', 1565249822, 'fgfgdfg', 4, 'http://telega-market.localhost/order/create', 0, 1588944156, 1588944156);

-- --------------------------------------------------------

--
-- Структура таблицы `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `slug` varchar(2048) NOT NULL,
  `title` varchar(512) NOT NULL,
  `body` text NOT NULL,
  `view` varchar(255) DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `page`
--

INSERT INTO `page` (`id`, `slug`, `title`, `body`, `view`, `status`, `created_at`, `updated_at`) VALUES
(1, 'about', 'About', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', NULL, 1, 1561974150, 1561974150);

-- --------------------------------------------------------

--
-- Структура таблицы `rbac_auth_assignment`
--

CREATE TABLE `rbac_auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `rbac_auth_assignment`
--

INSERT INTO `rbac_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('administrator', '1', 1561974186),
('manager', '2', 1561974186),
('user', '3', 1561974186),
('user', '4', 1575814768),
('user', '5', 1675160183);

-- --------------------------------------------------------

--
-- Структура таблицы `rbac_auth_item`
--

CREATE TABLE `rbac_auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `rbac_auth_item`
--

INSERT INTO `rbac_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('administrator', 1, NULL, NULL, NULL, 1561974186, 1561974186),
('editOwnModel', 2, NULL, 'ownModelRule', NULL, 1561974186, 1561974186),
('loginToBackend', 2, NULL, NULL, NULL, 1561974186, 1561974186),
('manager', 1, NULL, NULL, NULL, 1561974186, 1561974186),
('user', 1, NULL, NULL, NULL, 1561974186, 1561974186);

-- --------------------------------------------------------

--
-- Структура таблицы `rbac_auth_item_child`
--

CREATE TABLE `rbac_auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `rbac_auth_item_child`
--

INSERT INTO `rbac_auth_item_child` (`parent`, `child`) VALUES
('user', 'editOwnModel'),
('administrator', 'loginToBackend'),
('manager', 'loginToBackend'),
('administrator', 'manager'),
('administrator', 'user'),
('manager', 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `rbac_auth_rule`
--

CREATE TABLE `rbac_auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `rbac_auth_rule`
--

INSERT INTO `rbac_auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('ownModelRule', 0x4f3a32393a22636f6d6d6f6e5c726261635c72756c655c4f776e4d6f64656c52756c65223a333a7b733a343a226e616d65223b733a31323a226f776e4d6f64656c52756c65223b733a393a22637265617465644174223b693a313536313937343138363b733a393a22757064617465644174223b693a313536313937343138363b7d, 1561974186, 1561974186);

-- --------------------------------------------------------

--
-- Структура таблицы `system_db_migration`
--

CREATE TABLE `system_db_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `system_db_migration`
--

INSERT INTO `system_db_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1561974136),
('m140703_123000_user', 1561974140),
('m140703_123055_log', 1561974140),
('m140703_123104_page', 1561974140),
('m140703_123803_article', 1561974143),
('m140703_123813_rbac', 1561974144),
('m140709_173306_widget_menu', 1561974144),
('m140709_173333_widget_text', 1561974145),
('m140712_123329_widget_carousel', 1561974145),
('m140805_084745_key_storage_item', 1561974146),
('m141012_101932_i18n_tables', 1561974147),
('m150318_213934_file_storage_item', 1561974147),
('m150414_195800_timeline_event', 1561974148),
('m150725_192740_seed_data', 1561974150),
('m150929_074021_article_attachment_order', 1561974150),
('m151008_162401_create_notification_table', 1582038878),
('m160203_095604_user_token', 1561974150),
('m160509_211405_add_flashed_to_notification', 1582038879),
('m160921_171124_alter_notification_table', 1582038880),
('m190130_155645_add_article_slug_index', 1561974151),
('m190805_212700_create_channel_table', 1565126177),
('m190824_165753_create_orders_table', 1566674878),
('m190824_201829_added_exchange_price_to_channel_table', 1566678227),
('m191112_120521_add_status_to_checked_channel', 1573561084),
('m191120_114151_add_url_to_checked_channel_table', 1574251502),
('m191123_132357_add_updated_at_to_checked_channel', 1574515614),
('m191213_124505_create_message_table', 1576242262),
('m191216_135551_add_time_filds_for_orders_table', 1576504765),
('m200602_140304_add_column_reason_to_channel_table', 1591106931),
('m200608_115824_create_money_table', 1591617997),
('m200608_124530_add_balance_column_to_user', 1591621629),
('m200619_163727_create_currency_table', 1592658889),
('m200621_125326_add_base_ccy_to_currency_table', 1592744098),
('m200629_101130_add_new_fields_to_money_table', 1593426598),
('m200710_120637_add_currency_type_user_column_to_user_table', 1594383570),
('m201018_191811_add_email_confirm_token_column_to_money_table', 1603048832),
('m201029_201705_add_requisites_column_to_money_table', 1604002670);

-- --------------------------------------------------------

--
-- Структура таблицы `system_log`
--

CREATE TABLE `system_log` (
  `id` bigint(20) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_time` double DEFAULT NULL,
  `prefix` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `system_log`
--

INSERT INTO `system_log` (`id`, `level`, `category`, `log_time`, `prefix`, `message`) VALUES
(11832, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1602758550.8665, '[frontend][/user/money]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11833, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1602758566.2252, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11834, 1, 'yii\\base\\InvalidConfigException', 1602758567.9177, '[frontend][/notifications/poll?seen=0]', 'yii\\base\\InvalidConfigException: yii\\web\\Request::cookieValidationKey must be configured with a secret key. in D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Request.php:1678\nStack trace:\n#0 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Request.php(1660): yii\\web\\Request->loadCookies()\n#1 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\User.php(539): yii\\web\\Request->getCookies()\n#2 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\User.php(709): yii\\web\\User->renewIdentityCookie()\n#3 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\User.php(192): yii\\web\\User->renewAuthStatus()\n#4 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\User.php(360): yii\\web\\User->getIdentity()\n#5 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Component.php(139): yii\\web\\User->getIsGuest()\n#6 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2-debug\\src\\panels\\UserPanel.php(83): yii\\base\\Component->__get(\'isGuest\')\n#7 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\BaseObject.php(109): yii\\debug\\panels\\UserPanel->init()\n#8 [internal function]: yii\\base\\BaseObject->__construct(Array)\n#9 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\di\\Container.php(400): ReflectionClass->newInstanceArgs(Array)\n#10 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\di\\Container.php(159): yii\\di\\Container->build(\'yii\\\\debug\\\\panel...\', Array, Array)\n#11 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\BaseYii.php(365): yii\\di\\Container->get(\'yii\\\\debug\\\\panel...\', Array, Array)\n#12 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2-debug\\src\\Module.php(247): yii\\BaseYii::createObject(Array)\n#13 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2-debug\\src\\Module.php(218): yii\\debug\\Module->initPanels()\n#14 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\BaseObject.php(109): yii\\debug\\Module->init()\n#15 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php(158): yii\\base\\BaseObject->__construct(Array)\n#16 [internal function]: yii\\base\\Module->__construct(\'debug\', Object(yii\\web\\Application), Array)\n#17 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\di\\Container.php(400): ReflectionClass->newInstanceArgs(Array)\n#18 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\di\\Container.php(159): yii\\di\\Container->build(\'yii\\\\debug\\\\Modul...\', Array, Array)\n#19 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\BaseYii.php(365): yii\\di\\Container->get(\'yii\\\\debug\\\\Modul...\', Array, Array)\n#20 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php(427): yii\\BaseYii::createObject(Array, Array)\n#21 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(315): yii\\base\\Module->getModule(\'debug\')\n#22 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Application.php(69): yii\\base\\Application->bootstrap()\n#23 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(273): yii\\web\\Application->bootstrap()\n#24 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\BaseObject.php(109): yii\\base\\Application->init()\n#25 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(206): yii\\base\\BaseObject->__construct(Array)\n#26 D:\\OSPanel\\domains\\telega-market\\frontend\\web\\index.php(22): yii\\base\\Application->__construct(Array)\n#27 {main}'),
(11835, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1602758597.1752, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11836, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1602758662.5714, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11837, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1602758672.618, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11838, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1602759691.9181, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11839, 1, 'yii\\base\\InvalidConfigException', 1603040704.9436, '[frontend][/debug/default/toolbar?tag=5f8c75bda10bb]', 'yii\\base\\InvalidConfigException: yii\\web\\Request::cookieValidationKey must be configured with a secret key. in D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Request.php:1678\nStack trace:\n#0 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Request.php(1660): yii\\web\\Request->loadCookies()\n#1 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\User.php(539): yii\\web\\Request->getCookies()\n#2 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\User.php(709): yii\\web\\User->renewIdentityCookie()\n#3 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\User.php(192): yii\\web\\User->renewAuthStatus()\n#4 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\User.php(360): yii\\web\\User->getIdentity()\n#5 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Component.php(139): yii\\web\\User->getIsGuest()\n#6 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2-debug\\src\\panels\\UserPanel.php(83): yii\\base\\Component->__get(\'isGuest\')\n#7 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\BaseObject.php(109): yii\\debug\\panels\\UserPanel->init()\n#8 [internal function]: yii\\base\\BaseObject->__construct(Array)\n#9 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\di\\Container.php(400): ReflectionClass->newInstanceArgs(Array)\n#10 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\di\\Container.php(159): yii\\di\\Container->build(\'yii\\\\debug\\\\panel...\', Array, Array)\n#11 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\BaseYii.php(365): yii\\di\\Container->get(\'yii\\\\debug\\\\panel...\', Array, Array)\n#12 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2-debug\\src\\Module.php(247): yii\\BaseYii::createObject(Array)\n#13 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2-debug\\src\\Module.php(218): yii\\debug\\Module->initPanels()\n#14 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\BaseObject.php(109): yii\\debug\\Module->init()\n#15 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php(158): yii\\base\\BaseObject->__construct(Array)\n#16 [internal function]: yii\\base\\Module->__construct(\'debug\', Object(yii\\web\\Application), Array)\n#17 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\di\\Container.php(400): ReflectionClass->newInstanceArgs(Array)\n#18 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\di\\Container.php(159): yii\\di\\Container->build(\'yii\\\\debug\\\\Modul...\', Array, Array)\n#19 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\BaseYii.php(365): yii\\di\\Container->get(\'yii\\\\debug\\\\Modul...\', Array, Array)\n#20 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php(427): yii\\BaseYii::createObject(Array, Array)\n#21 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(315): yii\\base\\Module->getModule(\'debug\')\n#22 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Application.php(69): yii\\base\\Application->bootstrap()\n#23 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(273): yii\\web\\Application->bootstrap()\n#24 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\BaseObject.php(109): yii\\base\\Application->init()\n#25 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(206): yii\\base\\BaseObject->__construct(Array)\n#26 D:\\OSPanel\\domains\\telega-market\\frontend\\web\\index.php(22): yii\\base\\Application->__construct(Array)\n#27 {main}'),
(11840, 1, 'yii\\console\\UnknownCommandException', 1603048658.2005, '[console][]', 'yii\\base\\InvalidRouteException: Unable to resolve the request \"migate/create\". in D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php:537\nStack trace:\n#0 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\console\\Application.php(180): yii\\base\\Module->runAction(\'migate/create\', Array)\n#1 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\console\\Application.php(147): yii\\console\\Application->runAction(\'migate/create\', Array)\n#2 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(386): yii\\console\\Application->handleRequest(Object(yii\\console\\Request))\n#3 D:\\OSPanel\\domains\\telega-market\\console\\yii(26): yii\\base\\Application->run()\n#4 {main}\n\nNext yii\\console\\UnknownCommandException: Unknown command \"migate/create\". in D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\console\\Application.php:183\nStack trace:\n#0 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\console\\Application.php(147): yii\\console\\Application->runAction(\'migate/create\', Array)\n#1 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(386): yii\\console\\Application->handleRequest(Object(yii\\console\\Request))\n#2 D:\\OSPanel\\domains\\telega-market\\console\\yii(26): yii\\base\\Application->run()\n#3 {main}'),
(11841, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1603275616.5374, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11842, 1, 'Error', 1603275647.6409, '[frontend][/user/money/withdrawal-of-funds]', 'Error: Class \'frontend\\modules\\user\\controllers\\SignupService\' not found in D:\\OSPanel\\domains\\telega-market\\frontend\\modules\\user\\controllers\\MoneyController.php:56\nStack trace:\n#0 [internal function]: frontend\\modules\\user\\controllers\\MoneyController->actionWithdrawalOfFunds()\n#1 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(57): call_user_func_array(Array, Array)\n#2 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Controller.php(157): yii\\base\\InlineAction->runWithParams(Array)\n#3 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php(528): yii\\base\\Controller->runAction(\'withdrawal-of-f...\', Array)\n#4 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Application.php(103): yii\\base\\Module->runAction(\'user/money/with...\', Array)\n#5 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(386): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#6 D:\\OSPanel\\domains\\telega-market\\frontend\\web\\index.php(22): yii\\base\\Application->run()\n#7 {main}'),
(11843, 1, 'TypeError', 1603275821.3798, '[frontend][/user/money/withdrawal-of-funds]', 'TypeError: Argument 1 passed to common\\services\\money\\MoneyService::sentEmailConfirm() must be an instance of common\\models\\User, instance of yii\\web\\User given, called in D:\\OSPanel\\domains\\telega-market\\frontend\\modules\\user\\controllers\\MoneyController.php on line 61 and defined in D:\\OSPanel\\domains\\telega-market\\common\\services\\money\\MoneyService.php:19\nStack trace:\n#0 D:\\OSPanel\\domains\\telega-market\\frontend\\modules\\user\\controllers\\MoneyController.php(61): common\\services\\money\\MoneyService->sentEmailConfirm(Object(yii\\web\\User))\n#1 [internal function]: frontend\\modules\\user\\controllers\\MoneyController->actionWithdrawalOfFunds()\n#2 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(57): call_user_func_array(Array, Array)\n#3 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Controller.php(157): yii\\base\\InlineAction->runWithParams(Array)\n#4 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php(528): yii\\base\\Controller->runAction(\'withdrawal-of-f...\', Array)\n#5 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Application.php(103): yii\\base\\Module->runAction(\'user/money/with...\', Array)\n#6 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(386): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#7 D:\\OSPanel\\domains\\telega-market\\frontend\\web\\index.php(22): yii\\base\\Application->run()\n#8 {main}'),
(11844, 1, 'yii\\base\\UnknownPropertyException', 1603275891.9135, '[frontend][/user/money/withdrawal-of-funds]', 'yii\\base\\UnknownPropertyException: Getting unknown property: yii\\web\\User::email in D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Component.php:154\nStack trace:\n#0 D:\\OSPanel\\domains\\telega-market\\common\\services\\money\\MoneyService.php(21): yii\\base\\Component->__get(\'email\')\n#1 D:\\OSPanel\\domains\\telega-market\\frontend\\modules\\user\\controllers\\MoneyController.php(61): common\\services\\money\\MoneyService->sentEmailConfirm(Object(yii\\web\\User), Object(frontend\\modules\\user\\models\\WithdrawalOfFundsForm))\n#2 [internal function]: frontend\\modules\\user\\controllers\\MoneyController->actionWithdrawalOfFunds()\n#3 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(57): call_user_func_array(Array, Array)\n#4 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Controller.php(157): yii\\base\\InlineAction->runWithParams(Array)\n#5 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php(528): yii\\base\\Controller->runAction(\'withdrawal-of-f...\', Array)\n#6 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Application.php(103): yii\\base\\Module->runAction(\'user/money/with...\', Array)\n#7 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(386): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#8 D:\\OSPanel\\domains\\telega-market\\frontend\\web\\index.php(22): yii\\base\\Application->run()\n#9 {main}'),
(11845, 1, 'yii\\base\\UnknownPropertyException', 1603276109.8731, '[frontend][/user/money/withdrawal-of-funds]', 'yii\\base\\UnknownPropertyException: Getting unknown property: yii\\web\\User::email in D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Component.php:154\nStack trace:\n#0 D:\\OSPanel\\domains\\telega-market\\common\\services\\money\\MoneyService.php(21): yii\\base\\Component->__get(\'email\')\n#1 D:\\OSPanel\\domains\\telega-market\\frontend\\modules\\user\\controllers\\MoneyController.php(61): common\\services\\money\\MoneyService->sentEmailConfirm(Object(yii\\web\\User), Object(frontend\\modules\\user\\models\\WithdrawalOfFundsForm))\n#2 [internal function]: frontend\\modules\\user\\controllers\\MoneyController->actionWithdrawalOfFunds()\n#3 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(57): call_user_func_array(Array, Array)\n#4 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Controller.php(157): yii\\base\\InlineAction->runWithParams(Array)\n#5 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php(528): yii\\base\\Controller->runAction(\'withdrawal-of-f...\', Array)\n#6 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Application.php(103): yii\\base\\Module->runAction(\'user/money/with...\', Array)\n#7 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(386): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#8 D:\\OSPanel\\domains\\telega-market\\frontend\\web\\index.php(22): yii\\base\\Application->run()\n#9 {main}'),
(11846, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1603276159.0402, '[frontend][/user/money]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11847, 1, 'yii\\base\\UnknownPropertyException', 1603276173.2787, '[frontend][/user/money/withdrawal-of-funds]', 'yii\\base\\UnknownPropertyException: Getting unknown property: yii\\web\\User::email in D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Component.php:154\nStack trace:\n#0 D:\\OSPanel\\domains\\telega-market\\common\\services\\money\\MoneyService.php(21): yii\\base\\Component->__get(\'email\')\n#1 D:\\OSPanel\\domains\\telega-market\\frontend\\modules\\user\\controllers\\MoneyController.php(61): common\\services\\money\\MoneyService->sentEmailConfirm(Object(yii\\web\\User), Object(frontend\\modules\\user\\models\\WithdrawalOfFundsForm))\n#2 [internal function]: frontend\\modules\\user\\controllers\\MoneyController->actionWithdrawalOfFunds()\n#3 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(57): call_user_func_array(Array, Array)\n#4 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Controller.php(157): yii\\base\\InlineAction->runWithParams(Array)\n#5 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php(528): yii\\base\\Controller->runAction(\'withdrawal-of-f...\', Array)\n#6 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Application.php(103): yii\\base\\Module->runAction(\'user/money/with...\', Array)\n#7 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(386): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#8 D:\\OSPanel\\domains\\telega-market\\frontend\\web\\index.php(22): yii\\base\\Application->run()\n#9 {main}'),
(11848, 1, 'yii\\base\\UnknownMethodException', 1603276293.9442, '[frontend][/user/money/withdrawal-of-funds]', 'yii\\base\\UnknownMethodException: Calling unknown method: frontend\\modules\\user\\models\\WithdrawalOfFundsForm::getUser() in D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Component.php:300\nStack trace:\n#0 D:\\OSPanel\\domains\\telega-market\\common\\services\\money\\MoneyService.php(21): yii\\base\\Component->__call(\'getUser\', Array)\n#1 D:\\OSPanel\\domains\\telega-market\\frontend\\modules\\user\\controllers\\MoneyController.php(61): common\\services\\money\\MoneyService->sentEmailConfirm(Object(frontend\\modules\\user\\models\\WithdrawalOfFundsForm))\n#2 [internal function]: frontend\\modules\\user\\controllers\\MoneyController->actionWithdrawalOfFunds()\n#3 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(57): call_user_func_array(Array, Array)\n#4 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Controller.php(157): yii\\base\\InlineAction->runWithParams(Array)\n#5 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php(528): yii\\base\\Controller->runAction(\'withdrawal-of-f...\', Array)\n#6 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Application.php(103): yii\\base\\Module->runAction(\'user/money/with...\', Array)\n#7 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(386): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#8 D:\\OSPanel\\domains\\telega-market\\frontend\\web\\index.php(22): yii\\base\\Application->run()\n#9 {main}'),
(11849, 1, 'yii\\base\\ViewNotFoundException', 1603277398.0902, '[frontend][/user/money/withdrawal-of-funds]', 'yii\\base\\ViewNotFoundException: The view file does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/mail\\user-signup-comfirm-html.php in D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\View.php:233\nStack trace:\n#0 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\View.php(156): yii\\base\\View->renderFile(\'D:\\\\OSPanel\\\\doma...\', Array, Object(yii\\swiftmailer\\Mailer))\n#1 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\mail\\BaseMailer.php(302): yii\\base\\View->render(\'user-signup-com...\', Array, Object(yii\\swiftmailer\\Mailer))\n#2 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\mail\\BaseMailer.php(188): yii\\mail\\BaseMailer->render(\'user-signup-com...\', Array, \'layouts/html\')\n#3 D:\\OSPanel\\domains\\telega-market\\common\\services\\money\\MoneyService.php(26): yii\\mail\\BaseMailer->compose(Array, Array)\n#4 D:\\OSPanel\\domains\\telega-market\\frontend\\modules\\user\\controllers\\MoneyController.php(61): common\\services\\money\\MoneyService->sentEmailConfirm(Object(frontend\\modules\\user\\models\\WithdrawalOfFundsForm))\n#5 [internal function]: frontend\\modules\\user\\controllers\\MoneyController->actionWithdrawalOfFunds()\n#6 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(57): call_user_func_array(Array, Array)\n#7 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Controller.php(157): yii\\base\\InlineAction->runWithParams(Array)\n#8 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php(528): yii\\base\\Controller->runAction(\'withdrawal-of-f...\', Array)\n#9 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Application.php(103): yii\\base\\Module->runAction(\'user/money/with...\', Array)\n#10 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(386): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#11 D:\\OSPanel\\domains\\telega-market\\frontend\\web\\index.php(22): yii\\base\\Application->run()\n#12 {main}'),
(11850, 1, 'yii\\base\\UnknownPropertyException', 1603277515.7749, '[frontend][/user/money/withdrawal-of-funds]', 'yii\\base\\UnknownPropertyException: Getting unknown property: yii\\web\\User::username in D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Component.php:154\nStack trace:\n#0 D:\\OSPanel\\domains\\telega-market\\frontend\\mail\\withdraw-of-funds-html.php(28): yii\\base\\Component->__get(\'username\')\n#1 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\View.php(348): require(\'D:\\\\OSPanel\\\\doma...\')\n#2 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\View.php(257): yii\\base\\View->renderPhpFile(\'D:\\\\OSPanel\\\\doma...\', Array)\n#3 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\View.php(156): yii\\base\\View->renderFile(\'D:\\\\OSPanel\\\\doma...\', Array, Object(yii\\swiftmailer\\Mailer))\n#4 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\mail\\BaseMailer.php(302): yii\\base\\View->render(\'withdraw-of-fun...\', Array, Object(yii\\swiftmailer\\Mailer))\n#5 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\mail\\BaseMailer.php(188): yii\\mail\\BaseMailer->render(\'withdraw-of-fun...\', Array, \'layouts/html\')\n#6 D:\\OSPanel\\domains\\telega-market\\common\\services\\money\\MoneyService.php(26): yii\\mail\\BaseMailer->compose(Array, Array)\n#7 D:\\OSPanel\\domains\\telega-market\\frontend\\modules\\user\\controllers\\MoneyController.php(61): common\\services\\money\\MoneyService->sentEmailConfirm(Object(frontend\\modules\\user\\models\\WithdrawalOfFundsForm))\n#8 [internal function]: frontend\\modules\\user\\controllers\\MoneyController->actionWithdrawalOfFunds()\n#9 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(57): call_user_func_array(Array, Array)\n#10 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Controller.php(157): yii\\base\\InlineAction->runWithParams(Array)\n#11 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php(528): yii\\base\\Controller->runAction(\'withdrawal-of-f...\', Array)\n#12 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Application.php(103): yii\\base\\Module->runAction(\'user/money/with...\', Array)\n#13 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(386): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#14 D:\\OSPanel\\domains\\telega-market\\frontend\\web\\index.php(22): yii\\base\\Application->run()\n#15 {main}'),
(11851, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1603277560.7393, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11852, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1603884659.1734, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11853, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1603884687.9709, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11854, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1603884710.1654, '[frontend][/user/money/index?page=2]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11855, 1, 'yii\\base\\UnknownPropertyException', 1603886104.3817, '[frontend][/user/money/index?page=2]', 'yii\\base\\UnknownPropertyException: Setting unknown property: yii\\data\\Sort::id in D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\BaseObject.php:163\nStack trace:\n#0 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\BaseYii.php(558): yii\\base\\BaseObject->__set(\'id\', Array)\n#1 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\BaseObject.php(107): yii\\BaseYii::configure(Object(yii\\data\\Sort), Array)\n#2 [internal function]: yii\\base\\BaseObject->__construct(Array)\n#3 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\di\\Container.php(400): ReflectionClass->newInstanceArgs(Array)\n#4 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\di\\Container.php(159): yii\\di\\Container->build(\'yii\\\\data\\\\Sort\', Array, Array)\n#5 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\BaseYii.php(365): yii\\di\\Container->get(\'yii\\\\data\\\\Sort\', Array, Array)\n#6 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\data\\BaseDataProvider.php(259): yii\\BaseYii::createObject(Array)\n#7 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\data\\ActiveDataProvider.php(177): yii\\data\\BaseDataProvider->setSort(Array)\n#8 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Component.php(180): yii\\data\\ActiveDataProvider->setSort(Array)\n#9 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\BaseYii.php(558): yii\\base\\Component->__set(\'sort\', Array)\n#10 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\BaseObject.php(107): yii\\BaseYii::configure(Object(yii\\data\\ActiveDataProvider), Array)\n#11 D:\\OSPanel\\domains\\telega-market\\frontend\\modules\\user\\models\\search\\MoneySearch.php(51): yii\\base\\BaseObject->__construct(Array)\n#12 D:\\OSPanel\\domains\\telega-market\\frontend\\modules\\user\\controllers\\MoneyController.php(32): frontend\\modules\\user\\models\\search\\MoneySearch->search(Array)\n#13 [internal function]: frontend\\modules\\user\\controllers\\MoneyController->actionIndex()\n#14 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(57): call_user_func_array(Array, Array)\n#15 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Controller.php(157): yii\\base\\InlineAction->runWithParams(Array)\n#16 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Module.php(528): yii\\base\\Controller->runAction(\'index\', Array)\n#17 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\web\\Application.php(103): yii\\base\\Module->runAction(\'user/money/inde...\', Array)\n#18 D:\\OSPanel\\domains\\telega-market\\vendor\\yiisoft\\yii2\\base\\Application.php(386): yii\\web\\Application->handleRequest(Object(yii\\web\\Request))\n#19 D:\\OSPanel\\domains\\telega-market\\frontend\\web\\index.php(22): yii\\base\\Application->run()\n#20 {main}'),
(11856, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1603886252.7124, '[frontend][/user/money/index?page=2]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11857, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1603886260.9519, '[frontend][/user/money/index?page=1]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11858, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1603886264.2141, '[frontend][/user/money/index?page=1]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11859, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1603886278.1347, '[frontend][/user/money/index?page=1]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11860, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1603886352.8184, '[frontend][/user/money/index?page=1]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11861, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1603913544.457, '[frontend][/user/money/index?page=1]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11862, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1604000199.4148, '[frontend][/user/money/index?page=1]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11863, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1604000605.4124, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11864, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1604003728.8958, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11865, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1604003732.9195, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11866, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1604003809.1965, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11867, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1604003823.1637, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11868, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1604003828.8324, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru-RU/app.php Fallback file does not exist as well: D:\\OSPanel\\domains\\telega-market\\frontend/messages/ru/app.php'),
(11869, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1610998992.4052, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru-RU/app.php Fallback file does not exist as well: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru/app.php'),
(11870, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1610999006.7351, '[frontend][/user/money/balance-replenishment]', 'The message file for category \'app\' does not exist: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru-RU/app.php Fallback file does not exist as well: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru/app.php'),
(11871, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1610999007.0781, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru-RU/app.php Fallback file does not exist as well: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru/app.php'),
(11872, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1610999014.7898, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru-RU/app.php Fallback file does not exist as well: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru/app.php'),
(11873, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1611001714.5575, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru-RU/app.php Fallback file does not exist as well: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru/app.php'),
(11874, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1611002262.2249, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru-RU/app.php Fallback file does not exist as well: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru/app.php'),
(11875, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1611002293.5686, '[frontend][/user/money/balance-replenishment]', 'The message file for category \'app\' does not exist: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru-RU/app.php Fallback file does not exist as well: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru/app.php'),
(11876, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1611002293.7518, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru-RU/app.php Fallback file does not exist as well: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru/app.php'),
(11877, 1, 'yii\\i18n\\PhpMessageSource::loadFallbackMessages', 1611002315.8761, '[frontend][/user/money/index]', 'The message file for category \'app\' does not exist: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru-RU/app.php Fallback file does not exist as well: /home/wm9982/domains/shopik.hhos.ru/public_html/frontend/messages/ru/app.php');

-- --------------------------------------------------------

--
-- Структура таблицы `system_rbac_migration`
--

CREATE TABLE `system_rbac_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `system_rbac_migration`
--

INSERT INTO `system_rbac_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1561974152),
('m150625_214101_roles', 1561974186),
('m150625_215624_init_permissions', 1561974186),
('m151223_074604_edit_own_model', 1561974186),
('m190701_102313_init_roles', 1561976615);

-- --------------------------------------------------------

--
-- Структура таблицы `timeline_event`
--

CREATE TABLE `timeline_event` (
  `id` int(11) NOT NULL,
  `application` varchar(64) NOT NULL,
  `category` varchar(64) NOT NULL,
  `event` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `timeline_event`
--

INSERT INTO `timeline_event` (`id`, `application`, `category`, `event`, `data`, `created_at`) VALUES
(1, 'frontend', 'user', 'signup', '{\"public_identity\":\"webmaster\",\"user_id\":1,\"created_at\":1561974148}', 1561974148),
(2, 'frontend', 'user', 'signup', '{\"public_identity\":\"manager\",\"user_id\":2,\"created_at\":1561974148}', 1561974148),
(3, 'frontend', 'user', 'signup', '{\"public_identity\":\"user\",\"user_id\":3,\"created_at\":1561974148}', 1561974148),
(4, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1574515704),
(5, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1574518055),
(6, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575289944),
(7, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575289973),
(8, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575290018),
(9, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575290270),
(10, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575290421),
(11, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575291570),
(12, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575291681),
(13, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575291704),
(14, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575292025),
(15, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575292038),
(16, 'backend', 'user', 'signup', '{\"public_identity\":\"User2\",\"user_id\":4,\"created_at\":1575814768}', 1575814768),
(17, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575983297),
(18, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575983331),
(19, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575983499),
(20, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575983651),
(21, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575983899),
(22, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575984529),
(23, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575986649),
(24, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1575990043),
(25, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576053451),
(26, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576060673),
(27, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576060935),
(28, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576065652),
(29, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576066990),
(30, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576067336),
(31, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576067892),
(32, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576067894),
(33, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576068273),
(34, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576251802),
(35, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576331030),
(36, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576331233),
(37, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576333114),
(38, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576333427),
(39, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576333563),
(40, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576340608),
(41, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576341226),
(42, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576341430),
(43, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1576342361),
(44, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1583162293),
(45, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1583163339),
(46, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1583163832),
(47, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1583163849),
(48, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1583164201),
(49, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1583164459),
(50, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1583164596),
(51, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1583165158),
(52, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1583326680),
(53, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1583442002),
(54, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1587473311),
(55, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1588940839),
(56, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1588941413),
(57, 'frontend', 'order', 'take-work', '{\"foo\":\"bar\"}', 1588955771),
(58, 'frontend', 'user', 'signup', '{\"public_identity\":\"test-user3\",\"user_id\":5,\"created_at\":1675160183}', 1675160183);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(32) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL,
  `access_token` varchar(40) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `oauth_client` varchar(255) DEFAULT NULL,
  `oauth_client_user_id` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '2',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `logged_at` int(11) DEFAULT NULL,
  `balance` int(11) DEFAULT NULL,
  `currency_type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `access_token`, `password_hash`, `oauth_client`, `oauth_client_user_id`, `email`, `status`, `created_at`, `updated_at`, `logged_at`, `balance`, `currency_type`) VALUES
(1, 'webmaster-telega', 'iRYjQQX-uLST1zxfVTydoCDJbNuGLEz6', 'QSFpSgsDaF-xq9DhWPn67NnlwYhy7gpUNuxAnXxO', '$2y$13$vApBqeCvXp0BQC2s8fiWm.qyuIm1toWoIwa7u74XLOORE/SUJhOP.', NULL, NULL, 'webmaster@example.com', 2, 1561974148, 1594723194, 1675160292, 6500, 30),
(2, 'manager', 'jhmpS_aoPfB-UUmTBBtmDqDvpTAm5Gvv', 'UvX3rkRbZiV3YdW8pAEh9xUwbfr1P5RGNemh-kLC', '$2y$13$Mz1sCo8a.3qmQmfGvtDOyOOTmAl6hjoQBcJAIJ32Iz8IZAJ7Wk42C', NULL, NULL, 'manager@example.com', 2, 1561974149, 1561974149, 1576052335, NULL, NULL),
(3, 'user', 'de0uSCfWnSQ1FBdK3uWv1BgDEq7zZteQ', 'aNjvtXZqtrqf1TWccXGjm_TdMg4wL1r9Uke7lY89', '$2y$13$BTnTQ.xwCG4kNaCVG0KyjOMT5jyWLV8PGXMR/dqhHb1nHBNx1BkMa', NULL, NULL, 'user@example.com', 2, 1561974150, 1604003732, 1601979002, 1562, 30),
(4, 'User2', 'dlp9hvfMO4ID-GDnjeFVBcldA_q8NJhg', 'TflDXCHhaCjELPYBJ34p-paSm1RATg-1P9SToF17', '$2y$13$GlT70F7jDkfkNx457soHD..RmgQaZuzx5V7VufqwVIs72EZQlicE.', NULL, NULL, 'yaskool@yandex.ru', 2, 1575814768, 1611002293, 1610998723, 6568397, NULL),
(5, 'test-user3', '9ixEtIz0LWEWkRCfDPcuC2iOcSL92SE1', 'e_XjrdH7vMREMWGKag_feAnikwZViDiiWUSBZefT', '$2y$13$nzVnwnRRBr.IX9NnzMTX7.ClBWJnbnyQzivo.WB9Ozmoawli4KtFu', NULL, NULL, 'fenixoleg7@gmail.com', 2, 1675160183, 1675160183, 1675160183, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user_profile`
--

CREATE TABLE `user_profile` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `avatar_path` varchar(255) DEFAULT NULL,
  `avatar_base_url` varchar(255) DEFAULT NULL,
  `locale` varchar(32) NOT NULL,
  `gender` smallint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_profile`
--

INSERT INTO `user_profile` (`user_id`, `firstname`, `middlename`, `lastname`, `avatar_path`, `avatar_base_url`, `locale`, `gender`) VALUES
(1, 'John', 'dfdfgdfgd', 'Doe', '\\1\\FbQRdB8vpOli063K2_ezFWSN4VDA6BBE.jpg', '/storage/web/source', 'en-US', 1),
(2, 'oleg', '', '', '\\1\\2reJiMfAOhIDObB2naosxfcqi0KQmKiF.jpg', '/storage/web/source/', 'en-US', 1),
(3, 'oleg', 'fghfgh', 'fghfgh', '\\1\\keExwFFzCK1AFkCuWF08JcsMKu-NX_yz.jpg', '/storage/web/source/', 'en-US', 1),
(4, 'user2', '', '', '\\1\\ASf6CKiZpj9fAkOLbr2tiE-HRiDQkkfp.jpg', '/storage/web/source/', 'ru-RU', 1),
(5, NULL, NULL, NULL, NULL, NULL, 'ru-RU', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `token` varchar(40) NOT NULL,
  `expire_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `widget_carousel`
--

CREATE TABLE `widget_carousel` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `status` smallint(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `widget_carousel`
--

INSERT INTO `widget_carousel` (`id`, `key`, `status`) VALUES
(1, 'index', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `widget_carousel_item`
--

CREATE TABLE `widget_carousel_item` (
  `id` int(11) NOT NULL,
  `carousel_id` int(11) NOT NULL,
  `base_url` varchar(1024) DEFAULT NULL,
  `path` varchar(1024) DEFAULT NULL,
  `asset_url` varchar(1024) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `url` varchar(1024) DEFAULT NULL,
  `caption` varchar(1024) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `order` int(11) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `widget_carousel_item`
--

INSERT INTO `widget_carousel_item` (`id`, `carousel_id`, `base_url`, `path`, `asset_url`, `type`, `url`, `caption`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, 1, '/storage/web/source/', '\\1\\rYbkAfodGhxOog75sHtqHMFAn5m7aHRJ.jpg', 'http://telega-market.localhost/img/yii2-starter-kit.gif', 'image/jpeg', '/', '', 1, 0, NULL, 1566680263),
(2, 1, '/storage/web/source/', '\\1\\b7QzxUgReCBsV12PgG-2Ob7Z7BItw3K3.jpg', '/storage/web/source//\\1\\b7QzxUgReCBsV12PgG-2Ob7Z7BItw3K3.jpg', 'image/jpeg', '', '', 1, NULL, 1566680203, 1566680254);

-- --------------------------------------------------------

--
-- Структура таблицы `widget_menu`
--

CREATE TABLE `widget_menu` (
  `id` int(11) NOT NULL,
  `key` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `items` text NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `widget_menu`
--

INSERT INTO `widget_menu` (`id`, `key`, `title`, `items`, `status`) VALUES
(1, 'frontend-index', 'Frontend index menu', '[\n    {\n        \"label\": \"Get started with Yii2\",\n        \"url\": \"http://www.yiiframework.com\",\n        \"options\": {\n            \"tag\": \"span\"\n        },\n        \"template\": \"<a href=\\\"{url}\\\" class=\\\"btn btn-lg btn-success\\\">{label}</a>\"\n    },\n    {\n        \"label\": \"Yii2 Starter Kit on GitHub\",\n        \"url\": \"https://github.com/yii2-starter-kit/yii2-starter-kit\",\n        \"options\": {\n            \"tag\": \"span\"\n        },\n        \"template\": \"<a href=\\\"{url}\\\" class=\\\"btn btn-lg btn-primary\\\">{label}</a>\"\n    },\n    {\n        \"label\": \"Find a bug?\",\n        \"url\": \"https://github.com/yii2-starter-kit/yii2-starter-kit/issues\",\n        \"options\": {\n            \"tag\": \"span\"\n        },\n        \"template\": \"<a href=\\\"{url}\\\" class=\\\"btn btn-lg btn-danger\\\">{label}</a>\"\n    }\n]', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `widget_text`
--

CREATE TABLE `widget_text` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `widget_text`
--

INSERT INTO `widget_text` (`id`, `key`, `title`, `body`, `status`, `created_at`, `updated_at`) VALUES
(1, 'backend_welcome', 'Welcome to backend', '<p>Welcome to Yii2 Starter Kit Dashboard</p>', 1, 1561974150, 1561974150),
(2, 'ads-example', 'Google Ads Example Block', '<div class=\"lead\">\n                <script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script>\n                <ins class=\"adsbygoogle\"\n                     style=\"display:block\"\n                     data-ad-client=\"ca-pub-9505937224921657\"\n                     data-ad-slot=\"2264361777\"\n                     data-ad-format=\"auto\"></ins>\n                <script>\n                (adsbygoogle = window.adsbygoogle || []).push({});\n                </script>\n            </div>', 0, 1561974150, 1561974150);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_article_slug` (`slug`),
  ADD KEY `fk_article_author` (`created_by`),
  ADD KEY `fk_article_updater` (`updated_by`),
  ADD KEY `fk_article_category` (`category_id`);

--
-- Индексы таблицы `article_attachment`
--
ALTER TABLE `article_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_article_attachment_article` (`article_id`);

--
-- Индексы таблицы `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_article_category_slug` (`slug`),
  ADD KEY `fk_article_category_section` (`parent_id`);

--
-- Индексы таблицы `channel`
--
ALTER TABLE `channel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_channel_category` (`category_id`);

--
-- Индексы таблицы `channel_category`
--
ALTER TABLE `channel_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `checked_channel`
--
ALTER TABLE `checked_channel`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `file_storage_item`
--
ALTER TABLE `file_storage_item`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `i18n_message`
--
ALTER TABLE `i18n_message`
  ADD PRIMARY KEY (`id`,`language`);

--
-- Индексы таблицы `i18n_source_message`
--
ALTER TABLE `i18n_source_message`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `key_storage_item`
--
ALTER TABLE `key_storage_item`
  ADD PRIMARY KEY (`key`),
  ADD UNIQUE KEY `idx_key_storage_item_key` (`key`);

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `money`
--
ALTER TABLE `money`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_confirm_token` (`email_confirm_token`);

--
-- Индексы таблицы `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Индексы таблицы `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rbac_auth_assignment`
--
ALTER TABLE `rbac_auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Индексы таблицы `rbac_auth_item`
--
ALTER TABLE `rbac_auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `rbac_auth_item_child`
--
ALTER TABLE `rbac_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `rbac_auth_rule`
--
ALTER TABLE `rbac_auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `system_db_migration`
--
ALTER TABLE `system_db_migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `system_log`
--
ALTER TABLE `system_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_log_level` (`level`),
  ADD KEY `idx_log_category` (`category`);

--
-- Индексы таблицы `system_rbac_migration`
--
ALTER TABLE `system_rbac_migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `timeline_event`
--
ALTER TABLE `timeline_event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_created_at` (`created_at`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `widget_carousel`
--
ALTER TABLE `widget_carousel`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `widget_carousel_item`
--
ALTER TABLE `widget_carousel_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_item_carousel` (`carousel_id`);

--
-- Индексы таблицы `widget_menu`
--
ALTER TABLE `widget_menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `widget_text`
--
ALTER TABLE `widget_text`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_widget_text_key` (`key`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `article_attachment`
--
ALTER TABLE `article_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `article_category`
--
ALTER TABLE `article_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `channel`
--
ALTER TABLE `channel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `channel_category`
--
ALTER TABLE `channel_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `checked_channel`
--
ALTER TABLE `checked_channel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT для таблицы `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT для таблицы `file_storage_item`
--
ALTER TABLE `file_storage_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT для таблицы `i18n_source_message`
--
ALTER TABLE `i18n_source_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1348;

--
-- AUTO_INCREMENT для таблицы `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `money`
--
ALTER TABLE `money`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT для таблицы `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT для таблицы `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `system_log`
--
ALTER TABLE `system_log`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11878;

--
-- AUTO_INCREMENT для таблицы `timeline_event`
--
ALTER TABLE `timeline_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `widget_carousel`
--
ALTER TABLE `widget_carousel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `widget_carousel_item`
--
ALTER TABLE `widget_carousel_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `widget_menu`
--
ALTER TABLE `widget_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `widget_text`
--
ALTER TABLE `widget_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `fk_article_author` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_article_category` FOREIGN KEY (`category_id`) REFERENCES `article_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_article_updater` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `article_attachment`
--
ALTER TABLE `article_attachment`
  ADD CONSTRAINT `fk_article_attachment_article` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `article_category`
--
ALTER TABLE `article_category`
  ADD CONSTRAINT `fk_article_category_section` FOREIGN KEY (`parent_id`) REFERENCES `article_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `channel`
--
ALTER TABLE `channel`
  ADD CONSTRAINT `fk_channel_category` FOREIGN KEY (`category_id`) REFERENCES `channel_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `i18n_message`
--
ALTER TABLE `i18n_message`
  ADD CONSTRAINT `fk_i18n_message_source_message` FOREIGN KEY (`id`) REFERENCES `i18n_source_message` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `rbac_auth_assignment`
--
ALTER TABLE `rbac_auth_assignment`
  ADD CONSTRAINT `rbac_auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `rbac_auth_item`
--
ALTER TABLE `rbac_auth_item`
  ADD CONSTRAINT `rbac_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `rbac_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `rbac_auth_item_child`
--
ALTER TABLE `rbac_auth_item_child`
  ADD CONSTRAINT `rbac_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rbac_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `rbac_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_profile`
--
ALTER TABLE `user_profile`
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `widget_carousel_item`
--
ALTER TABLE `widget_carousel_item`
  ADD CONSTRAINT `fk_item_carousel` FOREIGN KEY (`carousel_id`) REFERENCES `widget_carousel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
