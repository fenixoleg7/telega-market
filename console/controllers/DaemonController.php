<?php
namespace console\controllers;
 
use Yii;
use yii\helpers\Url;
use yii\console\Controller;
use frontend\models\Twixxr;
 
/**
 * Test controller
 */
class DaemonController extends Controller {
 
    public function actionIndex() {
        echo "Yes, cron service is running.";
    }
 
    public function actionTestDaemon() {
      // called every two minutes
      // */2 * * * * ~/sites/www/yii2/yii test
      // строка, которую будем записывать
      $text = date("F j, Y, g:i a")." CRON отработал\n";
       
      //Открываем файл
      $fp = fopen("test.txt", "a+");
       
      // записываем в файл текст
      fwrite($fp, $text);
       
      // закрываем
      fclose($fp);
    }

}