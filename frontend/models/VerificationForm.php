<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\CheckedChannel;
use common\models\Orders;


/**
 * ContactForm is the model behind the contact form.
 */
class VerificationForm extends Model
{
    public $url_channel;
    public $id_order;
    public $id_channel;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            ['url_channel', 'required'],
            ['url_channel', 'url', 'defaultScheme' => 'https'],
            [['id_order', 'id_channel'], 'integer'],

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'url_channel' => 'Канал',
            'id_order' => 'Id заказа',
            'id_channel' => 'Id канала',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {

            return true;
        } else {
            return false;
        }
    }

    public function verificationCheckedChannel()
    {
        if ($this->validate()) {


            $model = CheckedChannel::find()
                ->where(['order_id' => $this->id_order])
                ->andWhere(['channel_id' => $this->id_channel])
                ->one();
            $model->status = 3;
            $model->url = $this->url_channel;
            $model->save();
            return true;
        } else {
            return false;
        }
    }
}
