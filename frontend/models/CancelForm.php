<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\CheckedChannel;
use common\models\Orders;


/**
 * ContactForm is the model behind the contact form.
 */
class CancelForm extends Model
{
    public $reason;
    public $id_order;
    public $id_channel;
    public $reason_list; 
    public $other;
    public $from_date;
    public $to_date;
    public $type;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            ['reason', 'required'],
            [['reason', 'other', 'from_date', 'to_date', 'type'], 'string', 'max' => 60],
            [['id_order', 'id_channel'], 'integer'],

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'reason' => 'Причина',
            'id_order' => 'Id заказа',
            'id_channel' => 'Id канала',
        ];
    }

    public function init()
    {
        parent::init();

        $this->reason_list = [
            '1' => 'Сомнительного содержания пост',
            '2' => 'Не тематическая реклама',
            '3' => 'Нет свободного места, предложить другую дату',
            '4' => 'Другое'
        ];
    }

    public function getReasonText($key)
    {
        return $this->reason_list[$key];
    }

}
