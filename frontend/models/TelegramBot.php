<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telegram_bot".
 *
 * @property int $id
 * @property string|null $username
 * @property int $user_id
 * @property int $status
 * @property int|null $chat_id
 * @property string|null $message
 */
class TelegramBot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telegram_bot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'status', 'chat_id'], 'integer'],
            [['message'], 'string'],
            [['username'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'chat_id' => Yii::t('app', 'Chat ID'),
            'message' => Yii::t('app', 'Message'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TelegramBotQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TelegramBotQuery(get_called_class());
    }
}
