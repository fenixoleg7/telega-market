<?php

namespace frontend\models\search;

use common\models\Channel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ArticleSearch represents the model behind the search form about `common\models\Article`.
 */
class ChannelSearch extends Channel
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title, user','exchange_price','category_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Channel::find()->published();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if($params['ChannelSearch']['users'] !=''){

            $users_arr = explode(',',$params['ChannelSearch']['users']);
            $query->andFilterWhere([
                'between', 'users', (int)$users_arr[0], (int)$users_arr[1]
            ]);
        }

        if($params['ChannelSearch']['exchange_price'] !=''){

            $exchange_price_arr = explode(',',$params['ChannelSearch']['exchange_price']);
            $query->andFilterWhere([
                'between', 'exchange_price', (int)$exchange_price_arr[0], (int)$exchange_price_arr[1]
            ]);
        }
        if($params['ChannelSearch']['category_id'] !=''){
            $query->andWhere(['category_id'=>$params['ChannelSearch']['category_id']]);
        }

        if($params['ChannelSearch']['title'] !=''){
            $query->andFilterWhere(['like', 'title', $params['ChannelSearch']['title']]);
        }


        return $dataProvider;
    }

    public function channels($params)
    {

        $query = Channel::find()->published()->andWhere(['id'=>$params[0]]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    public static function getTotal($provider, $columnName)
    {
        $total = 0;
        foreach ($provider as $item) {
            $total += $item[$columnName];
        }
        return $total;
    }
}
