<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\helpers\MoneyHelper;
use trntv\yii\datetime\DateTimeWidget;



/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MonneySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Moneys');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="money-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if( Yii::$app->session->hasFlash('success') ): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h2>Заявка на вывод средств создана</h2>
            <p>В целях безопасности, в ближайшее время ві получите письмо от
            техподдержки. Для подтверждения заявки пройдите по ссілке в письме. Если в течении нескольких минут ві не получитее
            письмо, обратитесь в нашу службу поддержкию</p>
        </div>
    <?php endif;?>

    <div class="btn-block-money">
        <?= $this->render('_balanceReplenishment', [
            'model' => $balanceReplenishmentForm
        ]) ?>

        <?= $this->render('_withdrawalOfFunds', [
            'model' => $withdrawalOfFundsForm
        ]) ?>

    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'order_id',
            'text',
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    return MoneyHelper::typeName($model->type);
                },
            ],
            'value',
            [
                'attribute' => 'currency',
                'value' => function ($model) {
                    return $model->currencyValue->code;
                },
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return MoneyHelper::statusName($model->status);
                },
            ],
            [
            'attribute' => 'created_at',
            'options' => ['style' => 'width: 10%'],
            'format' => 'datetime',
            'filter' => DateTimeWidget::widget([
                'model' => $searchModel,
                'attribute' => 'created_at',
                'phpDatetimeFormat' => 'dd.MM.yyyy',
                'momentDatetimeFormat' => 'DD.MM.YYYY',
            ]),
        ],

            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['style' => 'width: 5%'],
                'template' => '{view}',
            ]
        ],
    ]); ?>


</div>
