<?php
/**
 * Created by PhpStorm.
 * User: Fenixol
 * Date: 22.11.2019
 * Time: 11:35
 */

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

 Modal::begin([
        'header' => '<h2>Пополнение баланса</h2>',
        'toggleButton' => ['label' => Yii::t('backend', 'Balance replenishment'), 'class' => 'btn btn-success w165'],
    ]);


    $form = ActiveForm::begin([
        'id' => 'balance-replenishment-form',
        'action' => ['money/balance-replenishment'],
        'options' => ['class' => 'form-horizontal'],
    ]);
    echo   '<div class="container-fluid">';
    echo '<p> ! '.Yii::t('backend', 'Now the balance can be replenished using non-cash').'</p>';
    ?>

    <?= $form->field($model, 'value')->textInput(['class' =>'form-control col-md-6','type' => 'number'])->label(false) ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Пополнить', ['class' => 'btn btn-success']) ?>
            <?= Html::Button('Нет', ['class' => 'btn btn-danger']) ?>
        </div>
    </div>
    </div>
    <?php ActiveForm::end() ?>
    <?

Modal::end();?>


