<?php
/**
 * Created by PhpStorm.
 * User: Fenixol
 * Date: 22.11.2019
 * Time: 11:35
 */

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

 Modal::begin([
        'header' => '<h2>'.Yii::t('backend', 'Withdrawal of funds').'</h2>',
        'toggleButton' => ['label' => Yii::t('backend', 'Withdrawal of funds'), 'class' => 'btn btn-success w165'],
    ]);


    $form = ActiveForm::begin([
        'id' => 'withdrawal-of-funds-form',
        'action' => ['money/withdrawal-of-funds'],
        'options' => ['class' => 'form-horizontal'],
    ]);
    echo   '<div class="container-fluid">';
    
    ?>



    <?= $form->field($model, 'type')->radioList([1 => 'Яндекс деньги', 2 => 'WebMoney', 3 => 'QIWI', 4 => 'Банковская карта'],
                            ['class' => "list-type-withdrawal"])->label(false); ?>

    <div class="withdrawal-text">
    <p>Комиссия системы за вывод составляет 3.0%</p>
    <p>Минимальная сумма вывода 1000 рублей</p>
    <p></p>
    </div>      

    <?= $form->field($model, 'requisites')->textInput(['class' =>'form-control col-md-6','type' => 'text']) ?>

    <?= $form->field($model, 'value')->textInput(['class' =>'form-control col-md-6','type' => 'number']) ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton(Yii::t('frontend', 'Withdraw'), ['class' => 'btn btn-success']) ?>
            
        </div>
    </div>
    </div>
    <?php ActiveForm::end() ?>
    <?

Modal::end();?>


