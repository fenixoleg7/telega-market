<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Orders;
use common\models\Channel;
use common\models\CheckedChannel;
use common\models\User;
use trntv\yii\datetime\DateTimeWidget;
use yii\web\JsExpression;
use aneeshikmat\yii2\Yii2TimerCountDown\Yii2TimerCountDown;
use frontend\modules\user\models\ComplainAdvenrtisingForm;



/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\user\models\search\CheckedChannelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('frontend', 'My ads');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="checked-channel-index">

    <h1> <?= Yii::t('frontend', 'My ads') ?> </h1>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => true,
        'emptyCell'=>'',
        'pjax' => true,
        'striped' => true,
        'hover' => true,
        'panel' => ['type' => 'primary', 'heading' => 'Grid Grouping Example'],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            [
                'attribute' => Yii::t('frontend', 'Order'),
                'width' => '310px',
                'value' => function ($model, $key, $index, $widget) {
                    return $model->order_id;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Orders::find()->orderBy('order_id')->asArray()->all(), 'order_id','order_id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Any supplier'],
                'group' => true,  // enable grouping,
                'groupedRow' => true,                    // move grouped column to a single grouped row
                'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
                'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
            ],
            
            [
                'attribute' => Yii::t('frontend', 'Image'),
                'format' => 'html',
                'value' => function ($model) {
                    $url = Html::encode($model->order->imageUrl);
                    $url = str_replace('\\', '/', $url);
                    return Html::img($url, ['width' => '50px'],false);
                },
            ],
            [
                'attribute' => Yii::t('frontend', 'Text'),
                'width' => '200px',
                'value' => function ($model, $key, $index, $widget) {
                    return $model->order->advertisement_description;
                },
                'group' => true,  // enable grouping,
            ],

            [
                'attribute' => Yii::t('frontend', 'Channel'),
                'width' => '100px',
                'format' => 'html',
                'value' => function ($model, $key, $index, $widget) {
                    return Html::a(
                        $model->channel->title,
                        $model->channel->slug,
                        ['class' => 'btn btn-link']);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Channel::find()->orderBy('title')->asArray()->all(), 'id', 'title'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Any category'],
                'group' => true,  // enable grouping
                'subGroupOf' => 1 // supplier column index is the parent group
            ],
            [
                'attribute' => Yii::t('frontend', 'Link'),
                'value' => function ($model, $key, $index, $widget) {
                    return $model->url;
                },
                'group' => true,  // enable grouping

            ],

            [
                'attribute' => Yii::t('frontend', 'Price'),
                'value' => function ($model, $key, $index, $widget) {
                    return $model->channel->price;
                },
                'width' => '150px',
                'hAlign' => 'right',
                'format' => ['decimal', 2],
                'pageSummary' => true,
            ],
            [
                'attribute' => Yii::t('frontend', 'Status'),
                'value' => function ($model) {
                    if($model->status == Yii::$app->params['STATUS_VERIFICATION']){
                            $checked_cont = '<div class="bs-callout bs-callout-danger">'; 
                            $checked_cont = $checked_cont.Html::a('To accept', ['/user/advertising/to-accept', 'id'=>$model->id], ['class' => 'btn btn-success']);
                                
                            $checked_cont = $checked_cont.$this->render('_complain', [
                                 'data_form' => new ComplainAdvenrtisingForm(),
                                 'order' => $model->order_id,
                                 'channel' => $model->channel_id,
                             ]);
                            $checked_cont = $checked_cont.'</div>';
                            return $checked_cont;
                        
                    }else{
                        return $model->statuslabel;
                    }

                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => CheckedChannel::getStatusList(),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Any status'],
                'format' => 'raw',
            ],
            [
                'attribute' => Yii::t('frontend', 'Left'),
                //'contentOptions' => function ($model) {return ['id' => 'time-down-counter-'.$model->id]},
               'contentOptions' =>function ($model, $key, $index, $column){
                    return ['id' => 'time-down-counter-'.$model->id];
                },
                'value' => function ($model) {
                      
                     if($model->status == Yii::$app->params['STATUS_VERIFICATION']){
                       return  Yii2TimerCountDown::widget([
                            'countDownIdSelector' => 'time-down-counter-'.$model->id,
                            'countDownDate' => strtotime('+1 day',$model->updated_at) * 1000,// You most * 1000 to convert time to milisecond
                            'countDownResSperator' => ':',
                            'addSpanForResult' => false,
                            'addSpanForEachNum' => false,
                            'countDownOver' => 'Expired',
                            'countDownReturnData' => 'from-days',
                            'templateStyle' => 0,
                            'getTemplateResult' => 0,
                            //'callBack' => $callBackScript
                        ]);
                    }
                },
                'format' => 'html',
            ],
            [
            'attribute' => 'published_at',
            'options' => ['style' => 'width: 10%'],
            'format' => 'datetime',
            'filter' => DateTimeWidget::widget([
                'model' => $searchModel,
                'attribute' => 'published_at',
                'phpDatetimeFormat' => 'dd.MM.yyyy',
                'momentDatetimeFormat' => 'DD.MM.YYYY',
                'clientEvents' => [
                    'dp.change' => new JsExpression('(e) => $(e.target).find("input").trigger("change.yiiGridView")'),
                ],
            ]),
        ],
        [
            'attribute' => 'created_at',
            'options' => ['style' => 'width: 10%'],
            'format' => 'datetime',
            'filter' => DateTimeWidget::widget([
                'model' => $searchModel,
                'attribute' => 'created_at',
                'phpDatetimeFormat' => 'dd.MM.yyyy',
                'momentDatetimeFormat' => 'DD.MM.YYYY',
                'clientEvents' => [
                    'dp.change' => new JsExpression('(e) => $(e.target).find("input").trigger("change.yiiGridView")'),
                ],
            ]),
        ],

            //'created_at',
            //'published_at',
            //'updated_at',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

   

</div>
