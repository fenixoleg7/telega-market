<?php
/**
 * Created by PhpStorm.
 * User: Fenixol
 * Date: 22.11.2019
 * Time: 11:35
 */

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

 Modal::begin([
        'header' => '<h2>To complain</h2>',
        'toggleButton' => ['label' => 'To complain', 'class' => 'btn btn-warning'],
    ]);


    $form = ActiveForm::begin([
        'id' => 'data_form',
        'action' => ['/user/advertising/to-complain'],
        'options' => ['class' => 'form-horizontal'],
    ]);
    
    ?>
    <div class="container-fluid">
    <?= $form->field($data_form, 'id_order')->hiddenInput(['value' => $order])->label(false) ?>
    <?= $form->field($data_form, 'id_channel')->hiddenInput(['value' => $channel])->label(false) ?>
    <?= $form->field($data_form, 'message')->radioList([
        'Не выдержано время в топе' => 'Не выдержано время в топе',
        'Нее выдержано время на стене' => 'Нее выдержано время на стене', 
        'Другое' => 'Другое'
        ],
        [
            'item' => function ($index, $label, $name, $checked, $value) {
                return
                '<div class="radio"><label>' . Html::radio($name, $checked, ['value' => $value,'id' => 'id_' . $index]) . $label . '</label></div>';
            },
        ])->label(false);?>

    <?= $form->field($data_form, 'other')->textarea(['rows' => 3, 'class'=>'message-textarea form-control','disabled' => true])->label(false);?>
    
        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Send', ['class' => 'btn btn-danger']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end() ?>
    <?

Modal::end();?>


