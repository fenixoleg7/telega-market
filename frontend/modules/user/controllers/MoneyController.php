<?php

namespace frontend\modules\user\controllers;

use Yii;
use frontend\modules\user\models\search\MoneySearch;
use frontend\modules\user\models\BalanceReplenishmentForm;
use frontend\modules\user\models\ComplainAdvenrtisingForm;
use frontend\modules\user\models\WithdrawalOfFundsForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\services\money\MoneyService;


/**
 * AdvertisingController implements the CRUD actions for CheckedChannel model.
 */
class MoneyController extends Controller
{

	/**
     * Lists all CheckedChannel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MoneySearch();
        $balanceReplenishmentForm = new BalanceReplenishmentForm();
        $withdrawalOfFundsForm = new WithdrawalOfFundsForm();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'balanceReplenishmentForm'  => $balanceReplenishmentForm,
            'withdrawalOfFundsForm' => $withdrawalOfFundsForm
        ]);
    }

    public function actionBalanceReplenishment(){

        $model = new BalanceReplenishmentForm();
        $model->setUser(Yii::$app->user->identity);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
    }

    public function actionWithdrawalOfFunds(){

        $model = new WithdrawalOfFundsForm();
        $model->setUser(Yii::$app->user->identity);
        $moneyService = new MoneyService();

        if ($model->load(Yii::$app->request->post()) && $model->save() ) {

            Yii::$app->session->setFlash('success');
            $moneyService->sentEmailConfirm($model);
            return $this->redirect(['index']);
        }
    }

    public function actionWithdrawConfirm($token){

        $signupService = new MoneyService();

        try{
            $signupService->confirmation($token);
            Yii::$app->session->setFlash('success', 'You have successfully confirmed your registration.');
        } catch (\Exception $e){
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->goHome();
    }

}