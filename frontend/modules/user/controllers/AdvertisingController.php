<?php

namespace frontend\modules\user\controllers;

use Yii;
use common\models\CheckedChannel;
use common\models\Orders;
use common\models\Messsage;
use frontend\modules\user\models\search\CheckedChannelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\modules\user\models\ComplainAdvenrtisingForm;
use yii\helpers\Url;

/**
 * AdvertisingController implements the CRUD actions for CheckedChannel model.
 */
class AdvertisingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CheckedChannel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CheckedChannelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CheckedChannel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CheckedChannel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CheckedChannel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CheckedChannel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

   public function actionToAccept($id)
    {
        $model = $this->findModel($id);
        $model->status = Yii::$app->params['STATUS_PUBLISHED'];
        $model->save();
        return $this->redirect(['index']);
    }

    public function actionToComplain(){
        $model = new ComplainAdvenrtisingForm();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->complain()) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body' => 'Спасибо. Мы свяжемся с Вами в ближайшее время.',
                    'options' => ['class' => 'alert-success']
                ]);
                
            }else{
                Yii::$app->getSession()->setFlash('alert', [
                    'body' => 'Ошибка при отправке сообщения.',
                    'options' => ['class' => 'alert-danger']
                ]);
            }
        }

        Yii::$app->response->redirect(Url::to('/user/advertising/index'));
    }
    /**
     * Finds the CheckedChannel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CheckedChannel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CheckedChannel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }
}
