<?php

namespace frontend\modules\user\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Money;
use Yii;

/**
 * CheckedChannelSearch represents the model behind the search form of `common\models\CheckedChannel`.
 */
class MoneySearch extends Money
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Money::find()
        ->where(['user_id' => Yii::$app->user->getId()]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>[
                'defaultOrder'=>['id'=> SORT_DESC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'channel_id' => $this->channel_id,
        //     'order_id' => $this->order_id,
        //     'status' => $this->status,
        //     'created_at' => $this->created_at,
        //     'orders.published_at' => $this->published_at,
        //     'updated_at' => $this->updated_at,
        // ]);

        // $query->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
