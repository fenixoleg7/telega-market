<?php

namespace frontend\modules\user\models;

use common\models\User;
use common\models\Money;
use Yii;
use yii\base\Model;
use yii\web\JsExpression;

/**
 * Account form
 */
class BalanceReplenishmentForm extends Model
{
    /**
     * @var string
     */
    public $value;

    private $user;

    /**
     * @param $user
     */
    
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['value', 'required'],
            [['value'], 'number', 'min' => 100, 'max' => $this->user->balance],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'value' => 'Сумма',
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        $model = new Money();
        $model->user_id = $this->user->id;
        $model->text = 'Пополнение баланса пользователем';
        $model->currency_id = $this->user->currency_type;
        $model->type = Money::TYPE_ADD_MONEY;
        $model->value = $this->value;
        $model->status = Money::STATUS_CONSIDERATION_MONEY;
        $model->save();

        $this->user->balance = $this->user->balance + $this->value;
        return $this->user->save();
    }
}
