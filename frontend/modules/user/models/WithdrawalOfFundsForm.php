<?php

namespace frontend\modules\user\models;

use common\models\User;
use common\models\Money;
use Yii;
use yii\base\Model;
use yii\web\JsExpression;

/**
 * Account form
 */
class WithdrawalOfFundsForm extends Model
{
    /**
     * @var string
     */
    public  $type;
    public  $requisites;
    public  $value;
    public $user;

    
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['value, requisites, type', 'required'],
            [['value'], 'number', 'min' => 100, 'max' => Yii::$app->user->identity->getBalance()],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'value' => Yii::t('frontend', 'Amount'),
            'requisites' => Yii::t('frontend', 'Requisites')
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        $model = new Money();
        $model->user_id = $this->user->id;
        $model->text = 'Вывод средств пользователем';
        $model->currency_id = $this->user->currency_type;
        $model->requisites = $this->requisites;
        $model->type = Money::TYPE_WITHDRAW_MONEY;
        $model->value = $this->value;
        $model->status = Money::STATUS_CONSIDERATION_MONEY;
        $model->email_confirm_token = Yii::$app->security->generateRandomString();
        $model->save();

        //print_r($this->user->balance);
        //print_r($this->value);
        $this->user->balance = $this->user->balance - $this->value;
        //print_r($this->user->balance);

        return $this->user->save();
    }
}
