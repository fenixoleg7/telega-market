<?php

namespace frontend\modules\user\models;

use Yii;
use yii\base\Model;
use common\models\CheckedChannel;
use common\models\Message;


/**
 * ContactForm is the model behind the contact form.
 */
class ComplainAdvenrtisingForm extends Model
{
    public $id_order;
    public $id_channel;
    public $message;
    public $other;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['id_order','id_channel'], 'integer'],
            [['message','other'],'string', 'max' => 250]

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id_order' => 'Id заказа',
            'message' => 'Сообщение',
            'other' => 'Другое'
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {

            return true;
        } else {
            return false;
        }
    }

    public function complain()
    {
        if ($this->validate()) {
            $model = CheckedChannel::find()
                ->where(['order_id' => $this->id_order])
                ->andWhere(['channel_id' => $this->id_channel])
                ->one();

            $model->status = Yii::$app->params['STATUS_COMPLAIN'];
            $model->save();

            $message = new Message();
            $message->order_id = $this->id_order;
            $message->type = Yii::$app->params['MESSAGE_TYPE_COMPLAIN'];
            $message->user_id = Yii::$app->user->getId();
            $message->content = $this->message.'<br>'.$this->other;
            $message->status = Yii::$app->params['STATUS_DRAFT'];
            $message->save();

            return true;
        } else {
            return false;
        }
    }
}
