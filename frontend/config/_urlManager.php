<?php

use Sitemaped\Sitemap;

return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        // Notifications
        'notifications/poll' => '/notifications/notifications/poll',
        'notifications/rnr' => '/notifications/notifications/rnr',
        'notifications/read' => '/notifications/notifications/read',
        'notifications/read-all' => '/notifications/notifications/read-all',
        'notifications/delete-all' => '/notifications/notifications/delete-all',
        'notifications/delete' => '/notifications/notifications/delete',
        'notifications/flash' => '/notifications/notifications/flash',
        // Pages
        ['pattern' => 'page/<slug>', 'route' => 'page/view'],

        // Articles
        ['pattern' => 'article/index', 'route' => 'article/index'],
        ['pattern' => 'article/attachment-download', 'route' => 'article/attachment-download'],
        ['pattern' => 'article/<slug>', 'route' => 'article/view'],

        // Channels
        ['pattern' => 'channel/index', 'route' => 'channel/index'],
        ['pattern' => 'channel/create', 'route' => 'channel/create'],
        ['pattern' => 'channel/update', 'route' => 'channel/update'],
        ['pattern' => 'channel/delete', 'route' => 'channel/delete'],
        ['pattern' => 'channel/<slug>', 'route' => 'channel/view'],
        ['pattern' => 'channel/thumbnail-upload/1', 'route' => 'channel/thumbnail-upload'],
        ['pattern' => 'channel/thumbnail-delete/1', 'route' => 'channel/thumbnail-upload'],

        // Orders
        ['pattern' => 'order/index', 'route' => 'order/index'],
        ['pattern' => 'order/create', 'route' => 'order/create'],
        ['pattern' => 'order/update', 'route' => 'order/update'],
        ['pattern' => 'order/delete', 'route' => 'order/delete'],
        ['pattern' => 'order/take-work', 'route' => 'order/take-work'],
        ['pattern' => 'order/verification-order', 'route' => 'order/verification-order'],
        ['pattern' => 'order/cancel-order', 'route' => 'order/cancel-order'],

        ['pattern' => 'order/thumbnail-upload', 'route' => 'channel/thumbnail-upload'],
        ['pattern' => 'order/thumbnail-delete', 'route' => 'channel/thumbnail-upload'],
        ['pattern' => 'order/<slug>', 'route' => 'order/view'],


        // Sitemap
        ['pattern' => 'sitemap.xml', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_XML]],
        ['pattern' => 'sitemap.txt', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_TXT]],
        ['pattern' => 'sitemap.xml.gz', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_XML, 'gzip' => true]],

        // TelegramBot
        ['pattern' => 'telegram-bot/index', 'route' => 'telegram-bot/index'],
        ['pattern' => 'telegram-bot/update-telegram', 'route' => 'telegram-bot/update-telegram'],
        ['pattern' => 'telegram-bot/register-webhook-telegram', 'route' => 'telegram-bot/register-webhook-telegram'],
    ]

];
