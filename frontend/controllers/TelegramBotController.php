<?php

namespace frontend\controllers;

use app\models\TelegramBot;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * TelegramBotController implements the CRUD actions for TelegramBot model.
 */
class TelegramBotController extends Controller
{


    /**
     * Lists all TelegramBot models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new TelegramBot();

        if ($this->request->isPost) {
            $model->user_id = Yii::$app->user->id;
            if ($model->load($this->request->post()) && $model->save()) {
                //print_r($model);

                header("Location: https://t.me/TelegaMarketBot");
                die();
                //  return $this->render('index', [
                //      'code_success' => 1,
                //      'model' => $model
                //  ]);
            }
        } else{
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }
    public function actionUpdateTelegram()
    {
        $update = json_decode(file_get_contents('php://input'), JSON_OBJECT_AS_ARRAY);

        $chat_id = $update['message']['chat']['id'];
        $chat_username = $update['message']['chat']['username'];

        $model = $this->findModelFromUserName($chat_username);
        if($model){
            $model->chat_id = $chat_id;
            $model->message = $update;
            if ( $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
             return $this->render('update', [
                 'model' => $model,
             ]);
        }

    }
    public function actionRegisterWebhookTelegram()
    {
        $token = Yii::$app->params['TOKEN_FOR_TELEGRAM_BOT'];
        $method = 'setWebhook';
        $url = 'https://api.telegram.org/bot'.$token. '/' .$method;
        $options = [
            'url' =>'https://'
        ];
        $response = file_get_contents($url. '?'. http_build_query($options));
        var_dump($response);
    }

    /**
     * Displays a single TelegramBot model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TelegramBot model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new TelegramBot();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TelegramBot model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TelegramBot model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TelegramBot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return TelegramBot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TelegramBot::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }
    protected function findModelFromUserName($username)
    {
        if (($model = TelegramBot::findOne(['username' => $username])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }
}
