<?php

namespace frontend\controllers;

use Yii;
use common\models\Orders;
use common\models\CheckedChannel;
use common\models\Message;
use frontend\models\CancelForm;
use frontend\models\search\ChannelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\ChannelCategory;
use yii\filters\AccessControl;
use frontend\models\VerificationForm;
use yii\helpers\Url;
use common\commands\AddToTimelineCommand;
use common\components\Notification;

/**
 * OrderController implements the CRUD actions for Orders model.
 */
class OrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'thumbnail-upload' => [
                'class' => UploadAction::class,
                'responsePathParam' => 'path',
                'responseBaseUrlParam' => 'base_url',
                'responseUrlParam' => 'url',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(215, 215);
                    $file->put($img->encode());

                },
                'responseUrlParam' => 'id',
            ],
            'thumbnail-delete' => [
                'class' => DeleteAction::class
            ]
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ChannelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categories' => ChannelCategory::find()->all(),
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        

        $selected = Yii::$app->request->post('selection');

        $searchModel = new ChannelSearch();
        $dataProvider = $searchModel->channels([$selected]);

        $model = new Orders();
        $model->author_id = Yii::$app->user->id;
        $model->total_price = $searchModel->getTotal($dataProvider->models, 'exchange_price');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            foreach ( $selected as $key=>$val){

                $cheked = new CheckedChannel();
                $cheked->published_at = $model->published_at;
                $cheked->channel_id = $val;
                $cheked->order_id = $model->order_id;
                $cheked->status = Yii::$app->params['STATUS_DRAFT'];
                $cheked->save();
                Notification::notify(Notification::KEY_NEW_ORDER, $cheked->channel->author_id, 1);

            }
            return $this->redirect(['view', 'id' => $model->order_id]);

        }

        return $this->render('create', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);

        $channels = $model->channels;
        $selected = array();
        foreach ($channels as $channel){
            $selected[]=$channel->channel_id;
        }
        $searchModel = new ChannelSearch();
        $dataProvider = $searchModel->channels([$selected]);

        //$model->total_price = $searchModel->getTotal($dataProvider->models, 'exchange_price');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->order_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionTakeWork()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $order_id = $data['order'];
            $channel_id = $data['channel'];
            $model = CheckedChannel::find()
            ->where(['order_id' => $order_id])
            ->andWhere(['channel_id' => $channel_id])
            ->one();
            $model->status = Yii::$app->params['TAKEN_TO_WORK'];
            $model->save();

            $addToTimelineCommand = new AddToTimelineCommand([
                'category' => 'order',
                'event' => 'take-work',
                'data' => ['foo' => 'bar']
            ]);
            Yii::$app->commandBus->handle($addToTimelineCommand);

            Notification::notify(Notification::KEY_TAKEN_TO_WORK, $model->order->author_id, 1);

            return $this->renderPartial('/channel/_ajax-buttons-group', ['order'=>$order_id, 'channel'=>$channel_id, 'status' => Yii::$app->params['TAKEN_TO_WORK']], false, false);

        }
        echo 'error';

    }

    public function actionCancelOrder()
    {
        //if (Yii::$app->request->isAjax) {
        $model = new CancelForm();
        $message = new Message();
        //var_dump(Yii::$app->request->post());

        if ($model->load(Yii::$app->request->post())) {

            //find order autor
            $order = $this->findModel($model->id_order);

            // create new message for user
            $message->order_id =  $model->id_order;
            $message->type =  Yii::$app->params['STATUS_CANCELLED_ADMIN'];
            $message->user_id = Yii::$app->user->id;
            $message->recipient_id = $order->author_id;

            if($model->reason == 3 ){
                $message->content = 'Нет свободного места можно с '.$model->from_date.' по '.$model->to_date;
            }elseif($model->reason == 4 || $model->type == 'cancel'){
                $message->content = $model->other;
            }else{
                $message->content = $model->getReasonText($model->reason);
            }
            $message->status = 1;
            $message->save();

            $modelCheckedChannel = CheckedChannel::find()
                ->where(['order_id' => $model->id_order])
                ->andWhere(['channel_id' => $model->id_channel])
                ->one();
            $modelCheckedChannel->status = Yii::$app->params['STATUS_CANCELLED_ADMIN'];
            $modelCheckedChannel->save();

            Notification::notify(Notification::KEY_STATUS_CANCELLED_ADMIN, $modelCheckedChannel->order->author_id, 1);

             Yii::$app->response->redirect(Url::to('/channel/index'));
        }else{
            Yii::$app->getSession()->setFlash('alert', [
                'body' => 'Ошибка при отправке сообщения.',
                'options' => ['class' => 'alert-danger']
            ]);
            Yii::$app->response->redirect(Url::to('/channel/index'));
        }

    }

    public function actionVerificationOrder(){
        $model = new VerificationForm();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->verificationCheckedChannel()) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body' => 'Спасибо. Мы свяжемся с Вами в ближайшее время.',
                    'options' => ['class' => 'alert-success']
                ]);

                Yii::$app->response->redirect(Url::to('/channel/index'));
            }else{
                Yii::$app->getSession()->setFlash('alert', [
                    'body' => 'Ошибка при отправке сообщения.',
                    'options' => ['class' => 'alert-danger']
                ]);
                Yii::$app->response->redirect(Url::to('/channel/index'));
            }
        }

        Yii::$app->response->redirect(Url::to('/channel/index'));
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
