<?php

namespace frontend\controllers;

use Yii;
use common\models\Channel;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\ChannelCategory;
use yii\filters\AccessControl;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;


/**
 * ChannelController implements the CRUD actions for Channel model.
 */
class ChannelController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'thumbnail-upload' => [
                'class' => UploadAction::class,
                'responsePathParam' => 'path',
                'responseBaseUrlParam' => 'base_url',
                'responseUrlParam' => 'url',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(215, 215);
                    $file->put($img->encode());

                },
                'responseUrlParam' => 'id',
            ],
            'thumbnail-delete' => [
                'class' => DeleteAction::class
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'delete' => ['POST'],
            //     ],
            // ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Channel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Channel::find()->where(['author_id'=>Yii::$app->user->id]),
        ]);

        $channels = Channel::find()
            ->where(['author_id'=>Yii::$app->user->id])
            ->andWhere('status!=:status',[':status'=>Channel::STATUS_DELETED_OWNER])
            ->all();
        
        // $res = Yii::$app->telegram->sendMessage([
        //     'chat_id' => 818070227,
        //     'text' => "Hello Oleg!!!"
        // ]);  
         $telegram = Yii::$app->telegram;
        //  $res = $telegram->sendMessage([
        //      'chat_id' => $telegram->input->message->chat->id,
        //      'text' => "salam"
        //  ]); 
        
        // print_r($res); 

        $urlQuery = 'https://api.telegram.org/bot5844686465:AAFz0RIh9avUsKaw-bqlrgA_jKH_J40o4JY/getUpdates';
        $result = file_get_contents($urlQuery);
        echo '<pre>';
        print_r(json_decode($result, true));
        echo '</pre>';
        echo '============================';

        $resultObject = json_decode($result, true);
        echo '<pre>';
        print_r($resultObject["result"][0]["message"]["chat"]["id"]);
        echo '</pre>';


        //echo($resultObject.chat.id);


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'channels' => $channels,
        ]);

    }

    /**
     * Displays a single Channel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Channel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Channel();
        $model->author_id = Yii::$app->user->id;


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'categories' => ChannelCategory::find()->all(),
        ]);
    }

    /**
     * Updates an existing Channel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'categories' => ChannelCategory::find()->all(),
        ]);
    }

    /**
     * Deletes an existing Channel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $shannel = $this->findModel($id);
        $shannel->status = Channel::STATUS_DELETED_OWNER;
        $shannel->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Channel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Channel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Channel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
