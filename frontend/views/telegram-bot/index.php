<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="site-index">
    <div class="jumbotron">
        <h2 class="title">Уведомления В Telegram</h2>
    </div>
    <div class="body-content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="include_bot_notify">
                    <?php if($code_success == 1) : ?>
                    <div class="success_wrapper">
                        <span class="bg-blue_light pt-8px pb-8px pr-8px pl-8px br-8px">Отлично! Бот уведомлений работает 👍</span>
                    </div>
                    <?php else : ?>
                        <div class="include_wrapper">
                            <div class="font-16px desck lh-22px color-body mb-8px">Вы еще не подключили бот уведомлений. Получайте оперативные уведомления и управляйте заказами</div>
                        </div>
                        <div class="block-blue">
                            <div class="bold mb-16px">Как это сделать:</div>
                            <ol>
                                <li>Напишите юзернейм <strong>личного</strong> Telegram-аккаунта</li>
                                <li>Нажмите <strong>Подключить бот</strong></li>
                                <li>В Telegam-бот нажмите <strong>Start</strong> внизу или напишите команду <strong>/start</strong></li>
                            </ol>
                            <?php
                            $form = ActiveForm::begin([
                                'id' => 'create-bot-form',
                                'options' => ['class' => 'form-horizontal'],
                            ]) ?>
                            <div class="ror">
                                <div class="col-xs-6">
                                    <?= $form->field($model, 'username',[
                                        'inputOptions' => [
                                            'placeholder' => "@tegramacount",
                                            'class' => 'form-control'
                                        ],
                                    ])->label(false); ?>
                                </div>
                                <div class="col-xs-6">
                                    <?= Html::submitButton('Подключить бот', ['class' => 'btn btn-primary']) ?>
                                </div>
                            </div>
                                

                                
                            <?php ActiveForm::end() ?>
                        </div>
                    <?php endif ?>
                </div>
            </div> 
        </div>   
    </div>
</div>
