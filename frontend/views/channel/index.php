<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use aneeshikmat\yii2\Yii2TimerCountDown\Yii2TimerCountDown;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Channels';
$this->params['breadcrumbs'][] = $this->title;
echo newerton\fancybox3\FancyBox::widget();
?>
<div class="channel-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Channel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <div class="panel-group" id="accordion">
        <? foreach($channels as $channel):?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title channel-row" data-toggle="collapse" data-parent="#accordion" data-target="#chanel-<?= $channel->id; ?>">


                            <?= Html::img($channel->getImageUrl(), ['alt' => 'some', 'height' => '50px', 'width' => '50px']) ;?>

                            <div class="title">
                                <?= $channel->title; ?>
                            </div>

                            <div class="project-counters" >
                                Проектов: <span class="label label-default"><?= $channel->getCountOrders(); ?></span>
                                <? if($channel->getCountNewOrders()>0):?>
                                    Новые : <span class="label label-success"><?= $channel->getCountNewOrders(); ?></span>
                                <? endif ?>
                            </div>

                            <div class="links-block">
                                <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/channel/update', 'id' => $channel->id]),['class'=>'btn btn-default'] );?>
                                <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['/channel/delete', 'id' => $channel->id]),['class'=>'btn btn-default'] );?>
                            </div>

                    </h4>
                </div>
                <div id="chanel-<?= $channel->id; ?>" class="panel-collapse collapse">
                    <? if ($channel->getCountOrders()>0):?>

                    <input id="access_token" type="hidden" name="access_token" value="<?=Yii::$app->request->getCsrfToken()?>" />

                    <div class="panel-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th><?= Yii::t('frontend', 'Link') ?></th>
                                <th><?= Yii::t('frontend', 'Left') ?></th>
                                <th><?= Yii::t('frontend', 'Cost') ?></th>
                                <th><?= Yii::t('frontend', 'Image') ?></th>
                                <th><?= Yii::t('frontend', 'Advertising texts') ?></th>
                                <th><?= Yii::t('frontend', 'Status') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach($channel->orders as $order):?>
                                <tr>
                                    <td><?= $order->your_link; ?></td>
                                    <td>
                                        <? if($order->getStatus($channel->id)<3):?>
                                         
                                            <div id="time-down-counter-<?= $order->order_id ?>"></div>
                                            <?= Yii2TimerCountDown::widget([
                                                'countDownIdSelector' => "time-down-counter-".$order->order_id,
                                                'countDownDate' => strtotime('+1 day',$order->checkedStatus->updated_at ) * 1000,// You most * 1000 to convert time to milisecond
                                                'countDownResSperator' => ':',
                                                'addSpanForResult' => false,
                                                'addSpanForEachNum' => false,
                                                'countDownOver' => 'Expired',
                                                'countDownReturnData' => 'from-days',
                                                'templateStyle' => 0,
                                                'getTemplateResult' => 0,
                                                //'callBack' => $callBackScript
                                            ]) ?>
                                        <? endif?>
                                    </td>
                                    <td><?= $channel->price; ?></td>
                                    <td><?

                                        echo Html::a(Html::img($order->getImageUrl(), ['alt' => 'some', 'class' => 'fancy-box', 'height' => '50px', 'width' => '50px']), $order->getImageUrl(),['data-fancybox' => true]);
                                        ?>
                                    </td>
                                    <td><?= $order->advertisement_description; ?></td>
                                    <td class="ajax-btns-group">
                                        <?= $this->render('_ajax-buttons-group', [
                                            'status' => $order->checkedStatus->status,
                                            // 'verificaton_form' => $verificaton_form,
                                            'order' => $order->order_id,
                                            'channel'=> $channel->id
                                        ]) ?>
                                    </td>


                                </tr>

                            <? endforeach;?>

                            </tbody>
                        </table>

                    </div>
                    <?endif;?>
                </div>
            </div>
        <? endforeach; ?>

    </div>
</div>
