<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use trntv\filekit\widget\Upload;
use yii\web\JsExpression;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Channel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="channel-form">

    <?php $form = ActiveForm::begin(); ?>

<!--    --><?//= $form->field($model, 'author_id')->textInput() ?>

<!--    --><?//= $form->field($model, 'users')->textInput() ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>


    <?php echo $form->field($model, 'type')->dropDownlist([
        \common\models\Channel::TYPE_CHANNEL => 'Канал',
        \common\models\Channel::TYPE_CHAT => 'Чат'
    ], ['prompt' => '']) ?>


    <?php echo $form->field($model, 'category_id')->dropDownList(\yii\helpers\ArrayHelper::map(
        $categories,
        'id',
        'title'
    ), ['prompt' => '']) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'channel_description')->textarea(['rows' => 6]) ?>


    <?php echo $form->field($model, 'thumbnail')->widget(
        Upload::class,
        [
            'url' => ['thumbnail-upload']
        ]
    )?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
