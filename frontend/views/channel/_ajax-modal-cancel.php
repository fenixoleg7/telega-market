<?php
/**
 * Created by PhpStorm.
 * User: Fenixol
 * Date: 22.11.2019
 * Time: 11:35
 */

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;


if($type == 'reject') {
    Modal::begin([
        'header' => '<h2>Проект отклонить</h2>',
        'toggleButton' => ['label' => 'Reject', 'class' => 'btn btn-danger active cancel-order'],
    ]);
}else{
    Modal::begin([
        'header' => '<h2>Отмена проекта</h2>',
        'toggleButton' => ['label' => 'Cancel', 'class' => 'btn btn-danger active cancel-order'],
    ]);
}





    $form = ActiveForm::begin([
        'id' => 'cancel-form',
        'action' => ['order/cancel-order'],
        'options' => ['class' => 'form-horizontal'],
    ]);
    echo   '<div class="container-fluid">';
    echo '<p> Вы хотите отказаться от выполнения проекта?</p>';
    ?>

    <? if($type == 'reject'):?>
    <?= $form->field($cancel_form, 'reason')->dropDownList($cancel_form->reason_list,['prompt'=>'Выберете причину','onchange'=>'
            var sel = ($(this).val());
            if(sel == 4){
                jQuery(".other-block").show();
                jQuery(".other-date").hide();
            }else if(sel == 3){
                jQuery(".other-date").show();
                jQuery(".other-block").hide();
            }else{
                jQuery(".other-block").hide();
                jQuery(".other-date").hide();
            }'], ['id' => 'select']);?>

    <div class="other-block">
        <?= $form->field($cancel_form, 'other')->textArea()?>
    </div>

    <div class="other-date">

       <?= DatePicker::widget([
                'model' => $cancel_form,
                'attribute' => 'from_date',
                'attribute2' => 'to_date',
                'options' => ['placeholder' => 'Start date'],
                'options2' => ['placeholder' => 'End date'],
                'type' => DatePicker::TYPE_RANGE,
                'form' => $form,
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'autoclose' => true,
                    'startDate' => '-1m',
                ]
            ]);?>

    </div>

    <? else: ?>
        <?= $form->field($cancel_form, 'reason')->hiddenInput(['value' => 4])->label(false) ?>
        <?= $form->field($cancel_form, 'other')->textArea()?>

    <? endif; ?>
    
    <?= $form->field($cancel_form, 'id_order')->hiddenInput(['value' => $order])->label(false) ?>
    <?= $form->field($cancel_form, 'id_channel')->hiddenInput(['value' => $channel])->label(false) ?>
    <?= $form->field($cancel_form, 'type')->hiddenInput(['value' => $type])->label(false) ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Да', ['class' => 'btn btn-success']) ?>
            <?= Html::Button('Нет', ['class' => 'btn btn-danger']) ?>
        </div>
    </div>
    </div>
    <?php ActiveForm::end() ?>
    <?

Modal::end();?>
