<?php
/**
 * Created by PhpStorm.
 * User: Fenixol
 * Date: 22.11.2019
 * Time: 11:35
 */

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

 Modal::begin([
        'header' => '<h2>Подтверждение</h2>',
        'toggleButton' => ['label' => 'Send for verification', 'class' => 'btn btn-primary active send-verification'],
    ]);


    $form = ActiveForm::begin([
        'id' => 'verificaation-form',
        'action' => ['order/verification-order'],
        'options' => ['class' => 'form-horizontal'],
    ]);
    echo   '<div class="container-fluid">';
    echo '<p> Вы разместили рекламный обзор в соответствии с рекламными требованиями и обязуетесь не удалять  и не изменять рекламный пост в течениие суток после размещения</p>';
    ?>

    <?= $form->field($verificaton_form, 'url_channel')->textInput(['class' =>'form-control col-md-6'])->label(false) ?>

    <?= $form->field($verificaton_form, 'id_order')->hiddenInput(['value' => $order])->label(false) ?>
    <?= $form->field($verificaton_form, 'id_channel')->hiddenInput(['value' => $channel])->label(false) ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Да', ['class' => 'btn btn-success']) ?>
            <?= Html::Button('Нет', ['class' => 'btn btn-danger']) ?>
        </div>
    </div>
    </div>
    <?php ActiveForm::end() ?>
    <?

Modal::end();?>


