<?php
/**
 * Created by PhpStorm.
 * User: Fenixol
 * Date: 22.11.2019
 * Time: 11:34
 */

use frontend\models\VerificationForm;
use frontend\models\CancelForm;

 if($status == Yii::$app->params['STATUS_DRAFT']): ; ?>
    <button type="button" value="<?= $order; ?>" data-channel="<?= $channel; ?>" class="btn btn-primary active take-work">
        <?= Yii::t('frontend', 'Take work') ?>
    </button>

     <?= $this->render('_ajax-modal-cancel', [
         'cancel_form' => new CancelForm(),
         'order' => $order,
         'channel' => $channel,
         'type' => 'reject'
     ]) ?>


<? elseif($status == Yii::$app->params['TAKEN_TO_WORK']): ?>

     <?= $this->render('_ajax-modal-verification', [
         'verificaton_form' => new VerificationForm(),
         'order' => $order,
         'channel' => $channel
     ]) ?>

    <?= $this->render('_ajax-modal-cancel', [
         'cancel_form' => new CancelForm(),
         'order' => $order,
         'channel' => $channel,
         'type' => 'cancel'
     ]) ?>
    <!--  <button type="button" value="<?//= $order; ?>" data-channel="<?//= $channel; ?>" class="btn btn-danger active cancel-order">
        <?//= Yii::t('frontend', 'Cancel') ?>
    </button> -->

<? elseif($status == Yii::$app->params['STATUS_VERIFICATION']): ?>
    <?= Yii::t('frontend', 'On check') ?>

<? elseif($status == Yii::$app->params['STATUS_PUBLISHED']): ?>
    <?= Yii::t('frontend', 'Publick') ?>

<? elseif($status == Yii::$app->params['STATUS_COMPLAIN']): ?>
    <?= Yii::t('frontend', 'Complain') ?>

<? elseif($status == Yii::$app->params['STATUS_CANCELLED_ADMIN']): ?>
    <?= Yii::t('frontend', 'Cancel admin') ?>

<? elseif($status == Yii::$app->params['STATUS_CANCELLED_SISTEM']): ?>
    <?= Yii::t('frontend', 'Cancel sistem') ?>
<? endif; ?>
