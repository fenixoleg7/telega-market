<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use frontend\widgets\Menu;
use machour\yii2\notifications\widgets\NotificationsWidget;


NotificationsWidget::widget([
    'theme' => NotificationsWidget::THEME_GROWL,
    'clientOptions' => [
        'location' => 'br',
    ],
    'counters' => [
        '.notifications-header-count',
        '.notifications-icon-count'
    ],
    'markAllSeenSelector' => '#notification-seen-all',
    'listSelector' => '#notifications',
]);


/* @var $this \yii\web\View */
/* @var $content string */

$this->beginContent('@frontend/views/layouts/_clear.php')
?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]); ?>
    
    <?php 
    echo Menu::widget([
        'options' => ['class' => 'navbar-nav navbar-right nav'],
        'linkTemplate' => '<a href="{url}"><span>{label}</span>{icon}{badge}</a>',
        'submenuTemplate' => "\n<ul class='dropdown-menu' role='menu'>\n{items}\n</ul>\n",
        'items' => [
            ['label' => 'Главная', 'url' => ['/site/index']],
            ['label' => 'Каталог', 'url' => ['/order/index']],
            ['label' => 'О нас', 'url' => ['/page/view', 'slug'=>'about']],
            ['label' => 'Статьи', 'url' => ['/article/index']],
            ['label' => 'Контакты', 'url' => ['/site/contact']],
            ['label' => 'Регистрация', 'url' => ['/user/sign-in/signup'], 'visible'=>Yii::$app->user->isGuest],
            ['label' => 'Вход', 'url' => ['/user/sign-in/login'], 'visible'=>Yii::$app->user->isGuest],
            ['label' => '',
            'visible'=>!Yii::$app->user->isGuest,
             'options'=>['class'=>'dropdown'],
             'url' => ['#'],
             'template' => '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>{label}</span>{icon}{badge}</a>',
             'icon' => '<i class="fa fa-bell-o"></i>',
             'badge' => '0',
             'badgeBgClass' => 'label label-warning notifications-icon-count',
             'items' => [
                    [
                        'options' => ['class' => 'header-notification'],
                        'template' => 'You have <span class="notifications-header-count">0</span> notifications',
                    ],
                    [
                        'label' => '',
                        'template' => '<ul class="menu"><div id="notifications"></div></ul>',
                    ],
                    [
                        'options' => ['class' => 'footer-notification'],
                        'label' => 'You have notifications',
                        'template' => '<a href="#">View all</a> / <a href="#" id="notification-seen-all">Mark all as seen</a>'
                    ],
                ],
            ],
            [
                'label' => Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->getBalance(), 
                'visible'=>!Yii::$app->user->isGuest,
                'template' => '<a href="#"><span> {label} </span>{icon}{badge}</a>',
                'icon' => '<i class="fa fa-money"></i>',

            ],
            [
                'label' => Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->getPublicIdentity(),
                'visible'=>!Yii::$app->user->isGuest,
                'template' => '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>{label}</span><span class="caret"></span></a>',
                'items'=>[
                    [
                        'label' => 'Настройки',
                        'url' => ['/user/default/index']
                    ],
                    [
                        'label' => 'Панель управления',
                        'url' =>'/backend/web/',
                        'visible'=>Yii::$app->user->can('manager')
                    ],
                    [
                        'label' => 'Настройка уведомления',
                        'url' => ['/telegram-bot/index'],
                    ],
                    [
                        'label' => 'Моя реклама',
                        'url' => ['/user/advertising/index'],
                    ],
                    [
                        'label' => 'Мои каналы и чаты',
                        'url' =>  ['/channel/index'],
                    ],
                    [
                        'label' => 'Финансы',
                        'url' => ['/user/money/index'],
                    ],
                    [
                        'label' => 'Выход',
                        'url' => ['/user/sign-in/logout'],
                        'template' => '<a href="{url}" data-method="post">{label}</a>',
                    ]
                ]
            ],
            [
                'label'=>'Язык',
                'template' => '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>{label}</span><span class="caret"></span></a>',
                'items'=>array_map(function ($code) {
                    return [
                        'label' => Yii::$app->params['availableLocales'][$code],
                        'url' => ['/site/set-locale', 'locale'=>$code],
                        'active' => Yii::$app->language === $code
                    ];
                }, array_keys(Yii::$app->params['availableLocales']))
            ]
        ],

    ]);
    ?>

    <!-- <ul class="navbar-nav navbar-right">
        <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning notifications-icon-count">0</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">You have <span class="notifications-header-count">0</span> notifications</li>
                <li>
                    <ul class="menu">
                        <div id="notifications"></div>
                    </ul>
                </li>
                <li class="footer">
                    <a href="#">View all</a> / <a href="#" id="notification-seen-all">Mark all as seen</a></li>
            </ul>
        </li>
    </ul> -->
    
    <?php NavBar::end(); ?>

    <?php echo $content ?>

</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?php echo date('Y') ?></p>
        <p class="pull-right"><?php echo Yii::powered() ?></p>
    </div>
</footer>
<?php $this->endContent() ?>