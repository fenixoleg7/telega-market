<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use trntv\filekit\widget\Upload;
use trntv\yii\datetime\DateTimeWidget;
use kartik\date\DatePicker;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">


    <?php $form = ActiveForm::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'format' => 'html',
                'value' => function ($model1) {
                    $url = Html::encode($model1->getImageUrl());
                    $url = str_replace('\\', '/', $url);
                    return Html::img($url, ['width' => '50px'],false);
                }
            ],
            [
                'attribute' => 'title',
                'label' => 'Канал',
            ],
            'channel_description:ntext',
            'users',
            [
                'attribute' =>'exchange_price',
            ],
            [
                'class' => 'yii\grid\CheckboxColumn',
                'checkboxOptions' => function($model1) {
                    return [
                            'value' => $model1->id,
                            'class' => 'price-checkbox',
                            'data-price' => $model1->exchange_price,
                            'checked' => true
                        ];
                },
            ],

        ],
    ]); ?>

    <?= $form->field($model, 'total_price')->textInput() ?>

    <?= $form->field($model, 'your_link')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'advertisement_description')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'thumbnail')->widget(
        Upload::class,
        [
            'url' => ['thumbnail-upload']
        ]
    )?>

    <?php echo $form->field($model, 'published_at')->widget(
    DateTimeWidget::class,
    [
        'phpDatetimeFormat' => 'yyyy-MM-dd\'T\'HH:mm:ssZZZZZ',
    ]
) ?>


    <?= $form->field($model, 'wishes')->textarea(['rows' => 6]) ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
