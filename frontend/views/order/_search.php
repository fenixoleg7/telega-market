<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use kartik\form\ActiveForm;
use kartik\slider\Slider;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\ChannelCategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-search">

    <?php $form = ActiveForm::begin([
        'id' => 'channel_search',
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'category_id')->dropDownList(\yii\helpers\ArrayHelper::map(
                $categories,
                'id',
                'title'
            ), ['prompt' => '']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'title')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?=  $form->field($model, 'users',[
                    'template' => '<div>{label}</div><div class="labels-block"><b class="badge">10</b><b class="badge">1000</b></div>{input}'
            ])->widget(Slider::classname(), [
                'value'=>'250,650',
                'sliderColor'=>Slider::TYPE_GREY,
                'pluginOptions'=>[
                    'min'=>10,
                    'max'=>1000,
                    'step'=>5,
                    'range'=>true
                ],

            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'exchange_price',[
                'template' => '<div>{label}</div><div class="labels-block"><b class="badge">10</b><b class="badge">1000</b></div>{input}'
            ])->widget(Slider::classname(), [
                'value'=>'250,650',
                'sliderColor'=>Slider::TYPE_GREY,
                'pluginOptions'=>[
                    'min'=>10,
                    'max'=>1000,
                    'step'=>5,
                    'range'=>true
                ],

            ]); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('frontend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('frontend', 'Reset'),['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
