<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ChannelCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">


    <?php echo $this->render('_search', [
            'model' => $searchModel,
            'categories' => $categories
    ]); ?>

    <?php Pjax::begin(); ?>

    <?=Html::beginForm(['create'],'post');?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'format' => 'html',
                'value' => function ($model) {
                    $url = Html::encode($model->getImageUrl());
                    $url = str_replace('\\', '/', $url);
                    return Html::img($url, ['width' => '50px'],false);
                }
            ],
            [
                'attribute' => 'title',
                'label' => Yii::t('frontend', 'Channel'),
            ],
            'channel_description:ntext',
            'users',
            'exchange_price',
            [
                'class' => 'yii\grid\CheckboxColumn',
                'visible' => (Yii::$app->user->isGuest) ? false : true,
                'checkboxOptions' => function($model) {
                    return ['value' => $model->id];
                },
            ],

        ],
    ]); ?>


    <p>
        <?=Html::submitButton(Yii::t('frontend', 'To order'), ['class' => 'btn btn-primary']);?>
    </p>
    <?= Html::endForm();?>

    <?php Pjax::end(); ?>
</div>

