$(document).ready(function(){

    $('input.price-checkbox').click(function() {
        var price = $(this).data("price");
        var total= $('#orders-total_price').val();
        var new_total = null;
        if (this.checked) {
            new_total = (new_total == 0) ? Number(price) : Number(total) + Number(price);
        }else{
            new_total = total - price;
        }
        $('#orders-total_price').val(new_total);
    });

    $('input.select-on-check-all').click(function() {

        var new_total = null;
        var sThisVal = '';

        if (this.checked) {
            $('input.price-checkbox').each(function () {
                sThisVal = $(this).data("price") ;
                new_total =  Number(new_total) + Number(sThisVal);
            });
        }

        $('#orders-total_price').val(new_total);
    });

/* Channel page */
    $("button.take-work").click(function() {
        var par = $(this).parent();
        console.log('click-take-work');
        $.ajax({
            type: "POST",
            url: "/order/take-work",
            dataType: 'html',
            data: {
                order: $(this).val(),
                channel: $(this).data('channel'),
                _csrf: $("#access_token").val()
            },
            success: function(data) {
                par.html(data);
                location.reload();
            },
            error: function() {
                alert('error');
            }
        });

    });

    // $("body").on("click", ".send-verification", function(){
    //     $('.modal').modal('show'); 
    // });

    $(".radio").change(function() {
    if ($('#id_2').prop("checked")) {
      $('.message-textarea').prop('disabled', false);
    } else {
      $('.message-textarea').prop('disabled', true);
    }

  });


    // $(".cancel-order").click(function() {
    //     var par = $(this).parent();
    //     $.ajax({
    //         type: "POST",
    //         url: "/order/cancel-order",
    //         dataType: 'html',
    //         data: {
    //             order: $(this).val(),
    //             channel: $(this).data('channel'),
    //             _csrf: $("#access_token").val()
    //         },
    //         success: function(data) {
    //             par.html(data);
    //         },
    //         error: function() {
    //             alert('error');
    //         }
    //     });

});


/* End Channel page */


