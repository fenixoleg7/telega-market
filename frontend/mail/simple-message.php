<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message bing composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo Yii::$app->charset ?>"/>
    <title><?php echo Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background:url('') #44c5f4 center top no-repeat;font-family:'arial',sans-serif;font-size:19px;line-height:1.36"><tbody><tr><td align="center">
      <table border="0" cellpadding="0" cellspacing="0" width="600"><tbody><tr><td align="center" style="padding:40px 0 0 0">
            <img alt="" src="" class="">
          </td></tr><tr><td style="background:url('') center repeat-y">

            <table style="background:url('') center bottom no-repeat;padding:0 12px 0 12px;width:100%"><tbody><tr><td align="center" style="padding-top:20px">
                  <p style="font-size:23px;font-weight:700">
                    Псс, Админ!
                  </p>
                </td></tr><tr><td style="padding:0 25px 0 25px">

                  <?php if(isset($content)) echo $content; ?>
                  <?php echo $new_content ?>

                </td></tr><tr><td align="center" style="padding:5px 0 10px 0">
                  <img src="" class="">
                </td></tr><tr><td align="center" style="background:url('') center no-repeat;height:110px">
                  <p style="margin-top:0">
                    Вы можете подтвердить проект перейдя <a href="#">по ссылке.</a>

                  </p>
                </td></tr></tbody></table>
          </td></tr></tbody></table>
    </td></tr><tr><td align="center" valign="bottom" style="background:url('') center bottom no-repeat;height:290px">

    </td></tr></tbody></table>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
