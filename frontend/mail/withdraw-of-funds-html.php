<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message bing composed */
/* @var $content string main view render result */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['money/withdraw-confirm', 'token' => $money->email_confirm_token]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo Yii::$app->charset ?>"/>
    <title><?php echo Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background:url('') #44c5f4 center top no-repeat;font-family:'arial',sans-serif;font-size:19px;line-height:1.36"><tbody><tr><td align="center">
            <table border="0" cellpadding="0" cellspacing="0" width="600"><tbody><tr><td align="center" style="padding:40px 0 0 0">
                        <img alt="" src="" class="">
                    </td></tr><tr><td style="background:url('') center repeat-y">

                        <table style="background:url('') center bottom no-repeat;padding:0 12px 0 12px;width:100%"><tbody><tr><td align="center" style="padding-top:20px">
                                    <p style="font-size:23px;font-weight:700">
                                       Добрый день
                                    </p>
                                    <?php echo $use->identity->attributes->username ?>
                                </td></tr><tr><td style="padding:0 25px 0 25px">

                                    <p>Вы запросили вывод средств в сервисе Telega-market</p>
                                    <ul>
                                        <li>Вывод на: <?php echo $money->type ?></li>
                                        <li>Кошелек:<?php echo $money->requisites ?></li>
                                        <li>Сумма:<?php echo $money->value ?> </li>
                                    </ul>

                                </td></tr><tr><td align="center" style="padding:5px 0 10px 0">
                                    <img src="" class="">
                                </td></tr><tr><td align="center" style="background:url('') center no-repeat;height:110px">
                                    <p style="margin-top:0">
                                        Для безопасности, просим вас подтвердить ваш email для вывода средств путем перехода по ссылке:
                                        <a href="#">по ссылке.</a>

                                    </p>
                                </td></tr></tbody></table>
                    </td></tr></tbody></table>
        </td></tr><tr><td align="center" valign="bottom" style="background:url('') center bottom no-repeat;height:290px">

        </td></tr></tbody></table>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
