<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message bing composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo Yii::$app->charset ?>"/>
    <title><?php echo Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background:url('') #44c5f4 center top no-repeat;font-family:'arial',sans-serif;font-size:19px;line-height:1.36"><tbody><tr><td align="center">
      <table border="0" cellpadding="0" cellspacing="0" width="620"><tbody><tr><td align="center" style="padding:40px 0 0 0">
            <img alt="TeleGa" src="" class="CToWUd">
          </td></tr><tr><td style="background:url('') center repeat-y">
            <table style="background:url('') center bottom no-repeat;padding:0 12px 0 12px"><tbody><tr><td align="center" style="padding-top:20px">
                  <p style="font-size:23px;font-weight:700">
                    Добрый день,<br> <a href="mailto:ts-7@yandex.r" target="_blank">ts-7@yandex.r</a>u!
                  </p>
                </td></tr><tr><td style="padding:0 25px 0 25px">
                  <b>У вас новая заявка на размещение для канала @krasot_a.</b><br><p>Вам нужно принять или отклонить заявку в течение 24 часов.</p><br><p>Обращаем ваше внимание, что срок размещения рекламных материалов после принятия заявки составляет 36 часов.</p><br><p>По истечении данного времени проект автоматически отменяется, и оплата возвращается к рекламодателю.</p>
                  <p>Информация о проекте:</p>
                  <table style="padding-left:2px"><tbody><tr><td valign="top">
                        <img src="" class="CToWUd">
                      </td><td>
                        Ссылка: <a href="#" target="_blank" data-saferedirecturl="">https://t.me/joinchat/<wbr>AAAAAFJTmJIFpjmXXQrcUA</a>
                      </td></tr><tr><td valign="top">
                        <img src="" class="CToWUd">
                      </td><td>
                        Текст: <img goomoji="1f198" data-goomoji="1f198" style="margin:0 0.2ex;vertical-align:middle;max-height:24px" alt="🆘" src="https://mail.google.com/mail/e/1f198" data-image-whitelisted="" class="CToWUd"> Установлены сопутствующие болезни, которые увеличивают вероятность летального исхода при Covid-19

В Москве вчера скончались семь пациентов c подтвержденной пневмонией и положительным тестом на коронавирус. Все они имели сопутствующие болезни!

Читать подробнее... (<a href="" target="_blank" data-saferedirecturl="">https://t.me/joinchat/<wbr>AAAAAFJTmJIFpjmXXQrcUA</a>)
                      </td></tr><tr><td valign="top">
                        <img src="https://ci3.googleusercontent.com/proxy/v_e2-P48GCMAG_tKS5A1jrR-55xArV_RXvw2pGXue5FTrwxnq172YlIbtzZ7TbCdvk8MPDDdPJjSiKW2T584ASZYcGqNiUga_dapd473w7py3Tdu3KfqaOogDlupuqbmWSD2QXZNijHCzipWWo7FVk2Ws52XSi2l2A=s0-d-e1-ft#https://telega.in/assets/marker-y-56d06eb769f0a2fb96f80320989ad3e1cc703de63eea113cc2f7477f389c5256.png" class="CToWUd">
                      </td><td>
                        Комментарий: -
                      </td></tr></tbody></table>
                </td></tr><tr><td align="center" style="padding:5px 0 10px 0">
                  <img src="" class="CToWUd">
                </td></tr><tr><td align="center" style="background:url('') center no-repeat">
                  <p style="margin-top:0">
                    Желаем вам продуктивной работы<br> и отличных результатов.
                  </p>
                  <p>
                    <a href="https://telega.in/channels" style="background:url('') 50% 100% repeat-x;color:#000;display:inline-block;font-size:31px;font-weight:700;line-height:43px;text-decoration:none" target="_blank" data-saferedirecturl="">Вернуться в сервис</a>
                  </p>
                  <p style="margin-top:0">
                    <a href="#" target="_blank" data-saferedirecturl="" class="CToWUd"></a>
                    <a href="#" target="_blank" data-saferedirecturl=""><img src=""></a>
                  </p>
                </td></tr><tr><td align="center" style="padding-bottom:40px">
                  <div style="color:#342f10;font-size:14px">
                    <div>
                      P.S.: Если у вас есть вопросы касательно работы сервиса, обязательно <a href="#" style="background:url('') 0 100% repeat-x;color:inherit;display:inline-block;line-height:1.7;text-decoration:none" target="_blank" data-saferedirecturl="">напишите нам</a>.
                    </div>
                  </div>
                </td></tr></tbody></table>
          </td></tr></tbody></table>
    </td></tr><tr>
    	<td align="center" valign="bottom" style="background:#ececec;height:290px">

    </td>
</tr></tbody></table>
<?php echo $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
