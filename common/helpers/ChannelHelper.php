<?php

namespace common\helpers;

use common\models\Channel;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class ChannelHelper
{
    public static function statusList(): array
    {
        return [
            Channel::STATUS_NEW => 'Новый',
            Channel::STATUS_PUBLISH => 'Опубликован',
            Channel::STATUS_DELETED_ADMIN => 'Деактивирован администрацией',
            Channel::STATUS_DELETED_OWNER => 'Деактивирован владельцем'
        ];
    }

    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusLabel($status): string
    {
        switch ($status) {
            case Channel::STATUS_NEW:
                $class = 'label label-primary';
                break;
            case Channel::STATUS_PUBLISH:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-danger';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}