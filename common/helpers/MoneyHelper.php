<?php

namespace common\helpers;

use common\models\Money;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class MoneyHelper
{
    public static function typeList(): array
    {
        return [
            Money::TYPE_ADD_MONEY => 'Пополнение баланса',
            Money::TYPE_ADD_ADMIN_MONEY => 'Пополнение баланса администратором',
            Money::TYPE_SALE_MONEY => 'Оплата',
            Money::TYPE_REFUNDED_MONEY => 'Возврат средств',
            Money::TYPE_DECLINED_MONEY => 'Операция отменена',
            Money::TYPE_WITHDRAW_MONEY => 'Вывод средств',
            Money::TYPE_CHARGED_ADMIN_MONEY => 'Списание средств администратором'
        ];
    }

    public static function statusList(): array
    {
        return [
            Money::STATUS_OK_MONEY => 'Выполнена',
            Money::STATUS_REJECTED_MONEY => 'Отклонена',
            Money::STATUS_CONSIDERATION_MONEY => 'На рассмотрение',
        ];
    }

    public static function typeName($type): string
    {
        return ArrayHelper::getValue(self::typeList(), $type);
    }

    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusLabel($status): string
    {
        switch ($status) {
            case Channel::STATUS_NEW:
                $class = 'label label-primary';
                break;
            case Channel::STATUS_PUBLISH:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-danger';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}