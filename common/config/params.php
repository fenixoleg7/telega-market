<?php
  return [

    // токен телеграмм бота

    'TOKEN_FOR_TELEGRAM_BOT' => '5844686465:AAFz0RIh9avUsKaw-bqlrgA_jKH_J40o4JY',

    // статусы по заказу рекламы
    'STATUS_DRAFT'               => 1, // 'Новая заявка'
    'TAKEN_TO_WORK'              => 2, // 'Взята в работу'
    'STATUS_VERIFICATION'        => 3, // 'На проверке',
    'STATUS_VERIFICATION_OWNER'  => 4, // 'На проверке заказчиком',
    'STATUS_PUBLISHED'           => 5, // 'Опубликована',//
    'STATUS_COMPLAIN'            => 6, // 'Жалоба',
    'STATUS_CANCELLED_ADMIN'     => 7, // 'Отклонена админом',
    'STATUS_CANCELLED_SISTEM'    => 8, // 'Отклонена системой',
    'MESSAGE_TYPE_COMPLAIN'		 => 9, 

    // системные параметры
    'adminEmail' => env('ADMIN_EMAIL'),
        'robotEmail' => env('ROBOT_EMAIL'),
        'availableLocales' => [
            'en-US' => 'English (US)',
            'ru-RU' => 'Русский (РФ)',
//            'uk-UA' => 'Українська (Україна)',
//            'es' => 'Español',
//            'fr' => 'Français',
//            'vi' => 'Tiếng Việt',
//            'zh-CN' => '简体中文',
//            'pl-PL' => 'Polski (PL)',
        ],

    // типы валют    
    'CURRENCY_RU' => '1',
    'CURRENCY_UA' => '2',

    // статусы системы оплаты
    'STATUS_PAMENT_PROFILE' =>'1',
    'STATUS_PAMENT_ORDER'   =>'2',
    'STATUS_CANCELLATION'   =>'3',
    'STATUS_RETURN_MONEY'   =>'4'
 ];