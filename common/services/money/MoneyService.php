<?php
/**
 * Created by PhpStorm.
 * User: Fenixol
 * Date: 18.10.2020
 * Time: 22:41
 */

namespace common\services\money;

use Yii;
use common\models\User;
use common\models\Money;

class MoneyService
{


    public function sentEmailConfirm($money)
    {
        $email = Yii::$app->user->identity->attributes->email;

        $sent = Yii::$app->mailer
            ->compose(
                ['html' => 'withdraw-of-funds-html'],
                ['user' => Yii::$app->user, 'money' => $money])
            ->setTo($email)
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Подтверждение на вывод средств')
            ->send();

        if (!$sent) {
            throw new \RuntimeException('Sending error.');
        }
    }


    public function confirmation($token): void
    {
        if (empty($token)) {
            throw new \DomainException('Empty confirm token.');
        }

        $money = Money::findOne(['email_confirm_token' => $token]);
        if (!money) {
            throw new \DomainException('Money is not found.');
        }

        $money->email_confirm_token = null;
        $money->status = Money::STATUS_OK_MONEY ;
        if (!$money->save()) {
            throw new \RuntimeException('Saving error.');
        }

//        if (!Yii::$app->getUser()->login($user)){
//            throw new \RuntimeException('Error authentication.');
//        }
    }
}