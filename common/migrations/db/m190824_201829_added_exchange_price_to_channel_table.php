<?php

use yii\db\Migration;

/**
 * Class m190824_201829_added_exchange_price_to_channel_table
 */
class m190824_201829_added_exchange_price_to_channel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('channel', 'exchange_price', $this->integer());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('channel', 'exchange_price');
    }

}
