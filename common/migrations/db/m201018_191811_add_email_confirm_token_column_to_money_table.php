<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%money}}`.
 */
class m201018_191811_add_email_confirm_token_column_to_money_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%money}}', 'email_confirm_token', $this->string()->unique());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%money}}', 'email_confirm_token');
    }
}
