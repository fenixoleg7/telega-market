<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%currency}}`.
 */
class m200619_163727_create_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%currency}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_ip' => $this->text()->notNull(),
            'updated_ip' => $this->text()->notNull(),
            'created_by' => $this->text()->notNull(),
            'updated_by' => $this->text()->notNull(),
            'code' => $this->text()->notNull(),
            'name' => $this->text()->notNull(),
            'symbol' => $this->text()->notNull(),
            'rate' => $this->integer()->notNull()->unsigned(),
            'decimal_places' => $this->integer()->notNull()->unsigned(),
            'is_default' => $this->integer()->notNull()->unsigned(),
            'sort' => $this->integer()->notNull()->unsigned(),
            'status' => $this->smallInteger()->notNull()->unsigned(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%currency}}');
    }
}
