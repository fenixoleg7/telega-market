<?php

use yii\db\Migration;

/**
 * Class m230328_204005_create_telegram_bot
 */
class m230328_204005_create_telegram_bot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('telegram_bot', [
            'id' => $this->primaryKey(),
            'username' => $this->string(32),
            'user_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'chat_id' => $this->integer(),
            'message' => $this->text(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('telegram_bot');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230328_204005_create_telegram_bot cannot be reverted.\n";

        return false;
    }
    */
}
