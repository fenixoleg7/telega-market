<?php

use yii\db\Migration;

/**
 * Class m200621_125326_add_base_ccy_to_currency_table
 */
class m200621_125326_add_base_ccy_to_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->addColumn('currency','base_ccy', $this->string(4));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('currency', 'base_ccy');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200621_125326_add_base_ccy_to_currency_table cannot be reverted.\n";

        return false;
    }
    */
}
