<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m190824_165753_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('orders', [
            'order_id' => $this->primaryKey(),
            'total_price' => $this->integer()->notNull(),
            'advertisement_description' => $this->text(),
            'image_path' => $this->string(),
            'published_at' => $this->integer(),
            'wishes' => $this->text(),
            'author_id' => $this->integer()->notNull(),
            'your_link' => $this->string(1024)->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
        ]);

        $this->createTable('checked_channel', [
            'id' => $this->primaryKey(),
            'channel_id' => $this->integer(),
            'order_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('orders');
        $this->dropTable('checked_channel');

    }
}
