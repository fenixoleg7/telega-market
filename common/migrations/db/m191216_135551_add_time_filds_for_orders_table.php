<?php

use yii\db\Migration;

/**
 * Class m191216_135551_add_time_filds_for_orders_table
 */
class m191216_135551_add_time_filds_for_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('orders', 'created_at', $this->integer());
        $this->addColumn('orders', 'updated_at', $this->integer());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders', 'updated_at');
        $this->dropColumn('orders', 'created_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191216_135551_add_time_filds_for_orders_table cannot be reverted.\n";

        return false;
    }
    */
}
