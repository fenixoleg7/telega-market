<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%money}}`.
 */
class m201029_201705_add_requisites_column_to_money_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%money}}', 'requisites', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%money}}', 'requisites');
    }
}
