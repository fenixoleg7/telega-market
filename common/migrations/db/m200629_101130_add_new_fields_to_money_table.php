<?php

use yii\db\Migration;

/**
 * Class m200629_101130_add_new_fields_to_money_table
 */
class m200629_101130_add_new_fields_to_money_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->addColumn('money', 'created_at', $this->integer());
         $this->addColumn('money', 'updated_at', $this->integer());
         $this->addColumn('money', 'currency_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('money', 'created_at');
       $this->dropColumn('money', 'updated_at');
       $this->dropColumn('money', 'currency_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200629_101130_add_new_fields_to_money_table cannot be reverted.\n";

        return false;
    }
    */
}
