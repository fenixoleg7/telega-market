<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%message}}`.
 */
class m191213_124505_create_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%message}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->unsigned(),
            'type' => $this->integer()->notNull()->unsigned(),
            'user_id' => $this->integer()->notNull()->unsigned(),
            'recipient_id' => $this->integer()->notNull()->unsigned(),
            'content' => $this->text()->notNull(),
            'sent_at' => $this->integer()->notNull()->unsigned(),

            'reply_to' => $this->integer()->unsigned(),
            'status' => $this->smallInteger()->notNull()->unsigned(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%message}}');
    }
}
