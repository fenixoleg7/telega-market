<?php

use yii\db\Migration;

/**
 * Class m200608_124530_add_balance_column_to_user
 */
class m200608_124530_add_balance_column_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user','balance', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
         $this->dropColumn('user', 'balance');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200608_124530_add_balance_column_to_user cannot be reverted.\n";

        return false;
    }
    */
}
