<?php

use yii\db\Migration;

/**
 * Class m200602_140304_add_column_reason_to_channel_table
 */
class m200602_140304_add_column_reason_to_channel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->addColumn('channel', 'reason', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('channel', 'reason');
    }


}
