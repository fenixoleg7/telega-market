<?php

use yii\db\Migration;

/**
 * Class m191123_132357_add_updated_at_to_checked_channel
 */
class m191123_132357_add_updated_at_to_checked_channel extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('checked_channel','updated_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('checked_channel', 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191123_132357_add_updated_at_to_checked_channel cannot be reverted.\n";

        return false;
    }
    */
}
