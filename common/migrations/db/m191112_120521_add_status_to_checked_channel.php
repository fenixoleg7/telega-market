<?php

use yii\db\Migration;

/**
 * Class m191112_120521_add_status_to_checked_channel
 */
class m191112_120521_add_status_to_checked_channel extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('checked_channel','status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('checked_channel', 'status');
    }

}
