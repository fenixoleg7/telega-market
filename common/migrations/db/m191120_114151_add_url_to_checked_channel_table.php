<?php

use yii\db\Migration;

/**
 * Class m191120_114151_add_url_to_checked_channel_table
 */
class m191120_114151_add_url_to_checked_channel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('checked_channel','url', $this->string());
        $this->addColumn('checked_channel','created_at', $this->integer());
        $this->addColumn('checked_channel','published_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('checked_channel', 'url');
        $this->dropColumn('checked_channel', 'created_at');
        $this->dropColumn('checked_channel', 'published_at');
    }
}
