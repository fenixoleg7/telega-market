<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%money}}`.
 */
class m200608_115824_create_money_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%money}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'order_id'=> $this->integer()->notNull(),
            'text' => $this->string(1024)->notNull(),
            'currency' => $this->string(512)->notNull(),
            'type' => $this->smallInteger()->notNull()->defaultValue(0),
            'value' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%money}}');
    }
}
