<?php

use yii\db\Migration;

/**
 * Handles the creation of table `channel`.
 */
class m190805_212700_create_channel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%channel}}', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer()->notNull(),
            'users' => $this->integer()->notNull(),
            'slug' => $this->string(1024)->notNull(),
            'title' => $this->string(512)->notNull(),
            'type' => $this->smallInteger()->notNull()->defaultValue(0),
            'category_id' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'channel_description' => $this->text(),
            'avatar_path' => $this->string(),
            'avatar_base_url' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
        ]);

        $this->createTable('{{%channel_category}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(1024)->notNull(),
            'title' => $this->string(512)->notNull(),
            'body' => $this->text(),
            'parent_id' => $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
        ]);

        $this->addForeignKey('fk_channel_category', '{{%channel}}', 'category_id', '{{%channel_category}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_channel_category', '{{%channel}}');
        $this->dropTable('{{%channel}}');
    }
}
