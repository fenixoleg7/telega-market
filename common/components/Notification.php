<?php
namespace common\components;

use Yii;
use common\models\Meeting;//example models
use common\models\Orders;//example models
use common\models\User;
use machour\yii2\notifications\models\Notification as BaseNotification;

class Notification extends BaseNotification
{

    /**
     * A new order -  notification - 'Новая заявка',
     */
    const KEY_NEW_ORDER = 'new_order';

    /**
     * Order was take to work - notification - 'Взята в работу'
     */
    const KEY_TAKEN_TO_WORK = 'take_to_work';

    /**
     * На проверке
     */
    const KEY_STATUS_VERIFICATION = 'verification';

    /**
     * На проверке заказчиком
     */
    const KEY_STATUS_VERIFICATION_OWNER = 'verification_owner';

    /**
     * Опубликована
     */
    const KEY_STATUS_PUBLISHED = 'published';

    /**
     * Жалоба
     */
    const KEY_STATUS_COMPLAIN = 'complain';

    /**
     * Отклонена админом
     */
    const KEY_STATUS_CANCELLED_ADMIN = 'cancelled_admin';

    /**
     * Отклонена системой
     */
    const KEY_STATUS_CANCELLED_SISTEM = 'cancelled_sistem';

    /**
     * @var array Holds all usable notifications
     */
    public static $keys = [
        self::KEY_NEW_ORDER,
        self::KEY_TAKEN_TO_WORK,
        self::KEY_STATUS_VERIFICATION,
        self::KEY_STATUS_VERIFICATION_OWNER,
        self::KEY_STATUS_PUBLISHED,
        self::KEY_STATUS_COMPLAIN,
        self::KEY_STATUS_CANCELLED_ADMIN,
        self::KEY_STATUS_CANCELLED_SISTEM

    ];

    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        switch ($this->key) {

            case self::KEY_NEW_ORDER:
                return Yii::t('app', 'New order');

            case self::KEY_TAKEN_TO_WORK:
                return Yii::t('app', 'Taken to work');

            case self::KEY_STATUS_VERIFICATION:
                return Yii::t('app', 'Verification');

            case self::KEY_STATUS_VERIFICATION_OWNER:
                return Yii::t('app', 'Verification owner');

            case self::KEY_STATUS_PUBLISHED:
                return Yii::t('app', 'Published');

            case self::KEY_STATUS_COMPLAIN:
                return Yii::t('app', 'Complaint');

            case self::KEY_STATUS_CANCELLED_ADMIN:
                return Yii::t('app', 'Cancelled administrator');

            case self::KEY_STATUS_CANCELLED_SISTEM:
                return Yii::t('app', 'Cancelled sistem');
        }
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        switch ($this->key) {
            case self::KEY_NEW_ORDER:
                return Yii::t('app', 'You got a new order');

            case self::KEY_TAKEN_TO_WORK:
                return Yii::t('app', 'Your order taken to work');

            case self::KEY_STATUS_VERIFICATION:
                return Yii::t('app', 'Your order on verification');

            case self::KEY_STATUS_VERIFICATION_OWNER:
                return Yii::t('app', 'Order taken by owner');

            case self::KEY_STATUS_PUBLISHED:
                return Yii::t('app', 'Your order on published');

            case self::KEY_STATUS_COMPLAIN:
                return Yii::t('app', 'You have been filed a complaint');

            case self::KEY_STATUS_CANCELLED_ADMIN:
                return Yii::t('app', 'Your order has been rejected by the administrator');

            case self::KEY_STATUS_CANCELLED_SISTEM:
                return Yii::t('app', 'Your order has been rejected by the sistem');
        }
    }

    /**
     * @inheritdoc
     */
    public function getRoute()
    {
        switch ($this->key) {

            case self::KEY_NEW_ORDER:
                return ['/channel/index'];

            case self::KEY_TAKEN_TO_WORK:
                return ['/user/advertising/index'];

            case self::KEY_STATUS_VERIFICATION:
                return ['/user/advertising/index'];

            case self::KEY_STATUS_VERIFICATION_OWNER:
                return ['/user/advertising/index'];

            case self::KEY_STATUS_PUBLISHED:
                return ['/user/advertising/index'];

            case self::KEY_STATUS_COMPLAIN:
                return ['/user/advertising/index'];

            case self::KEY_STATUS_CANCELLED_ADMIN:
                return ['/user/advertising/index'];

            case self::KEY_STATUS_CANCELLED_SISTEM:
                return ['/user/advertising/index']; 
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
     if ($insert) {

        $message = '';
        $subject = '';
        $template = '';
        $content_message =  array();

        switch ($this->key) {
            case self::KEY_NEW_ORDER:
                $message = Yii::t('app', 'You got a new order');
                $subject = Yii::t('app', 'New Order');
                $template = 'new-order';
                $content_message['new_content'] =  $message;
            case self::KEY_TAKEN_TO_WORK:
                $message = Yii::t('app', 'Your order taken to work');
                $subject = Yii::t('app','Taken to Work');
                $template = 'simple-message';
                $content_message['new_content'] =  $message;
            case self::KEY_STATUS_VERIFICATION:
                $message = Yii::t('app', 'Your order on verification');
                $subject = Yii::t('app','Status');
            case self::KEY_STATUS_VERIFICATION_OWNER:
                $message = Yii::t('app', 'Order taken by owner');
                $subject = Yii::t('app','Status');
            case self::KEY_STATUS_PUBLISHED:
                $message = Yii::t('app', 'Your order on published');
                $subject = Yii::t('app','Status');
            case self::KEY_STATUS_COMPLAIN:
                $message = Yii::t('app', 'You have been filed a complaint');
                $subject = Yii::t('app','Status');
            case self::KEY_STATUS_CANCELLED_ADMIN:
                $message = Yii::t('app', 'Your order has been rejected by the administrator');
                $subject = Yii::t('app','Status');
            case self::KEY_STATUS_CANCELLED_SISTEM:
                $message = Yii::t('app', 'Your order has been rejected by the sistem');
                $subject = Yii::t('app','Status');
        }

        /* Отправка писем пользователям по событиям */

     //    $user = User::findOne($this->user_id); 

     //    Yii::$app->mailer->compose($template,$content_message)
     //     ->setFrom(['admin-shopik@shopik.hhos.ru' => 'Письмо с сайта'])
     //     ->setTo($user->email)
     //     ->setSubject($subject)
     //     ->setTextBody($message)
     //     //->setHtmlBody('<b>текст сообщения в формате HTML</b>')
     //     ->send();
     } 
     parent::afterSave($insert, $changedAttributes);
    }

}