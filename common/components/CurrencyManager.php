<?php

namespace common\components;

use Yii;
use yii\base\BaseObject;
use common\models\Currency;

class CurrencyManager extends BaseObject
{
    public $currencies;
    public $defaultCurrency;
    public $siteCurrency;
    
    public function init()
    {
        parent::init();
        
        $this->currencies = Currency::find()
            ->where(['status' => Currency::STATUS_ACTIVE])
            ->orderBy(['sort' => SORT_ASC])
            ->asArray()
            ->all();
        foreach ($this->currencies as $item) {
            if ($item['is_default'] == 1) {
                $this->defaultCurrency = $item;
            }
        }
        if (!Yii::$app->session['currency']) {
            Yii::$app->session['currency'] = $this->defaultCurrency;
        }
        $this->siteCurrency = Yii::$app->session['currency'];
    }
    
    public function listCurrencies()
    {
        $result = [];
        foreach ($this->currencies as $item) {
            $result[$item['code']] = $item['code'];
        }
        return $result;
    }
    
    public function showPrice($offer, $symbol = true)
    {
        if ($offer['currency_code'] == $this->siteCurrency['code']) {
            $price = $offer['price'];
        } else {
            foreach ($this->currencies as $item) {
                if ($item['code'] == $offer['currency_code']) {
                    $rate = $item['rate'];
                }
            }
            $price = ($offer['price'] * $rate) / $this->siteCurrency['rate'];
        }
                
        if ($symbol === true) {
            return number_format($price, $this->siteCurrency['decimal_places'], '.', '') . ' ' . $this->siteCurrency['symbol'];
        } else {
            //return round($price, $this->siteCurrency['decimal_places']);
            return number_format($price, $this->siteCurrency['decimal_places'], '.', '');
        }
    }
    
    public function showOldPrice($offer, $symbol = true)
    {
        if ($offer['currency_code'] == $this->siteCurrency['code']) {
            $price = $offer['old_price'];
        } else {
            foreach ($this->currencies as $item) {
                if ($item['code'] == $offer['currency_code']) {
                    $rate = $item['rate'];
                }
            }
            $price = ($offer['old_price'] * $rate) / $this->siteCurrency['rate'];
        }
                
        if ($symbol === true) {
            return number_format($price, $this->siteCurrency['decimal_places'], '.', '') . '&nbsp;' . $this->siteCurrency['symbol'];
        } else {
            return round($price, $this->siteCurrency['decimal_places']);
        }
    }
}