<?php

namespace common\models;
use trntv\filekit\behaviors\UploadBehavior;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "orders".
 *
 * @property int $order_id
 * @property int $total_price
 * @property string $advertisement_description
 * @property string $image_path
 * @property int $published_at
 * @property string $wishes
 * @property int $author_id
 * @property string $your_link
 * @property int $status
 */
class Orders extends \yii\db\ActiveRecord
{
    public $thumbnail;

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => UploadBehavior::class,
                'attribute' => 'thumbnail',
                'pathAttribute' => 'image_path',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total_price', 'author_id', 'your_link'], 'required'],

            [['published_at'], 'default', 'value' => function () {
                return date(DATE_ISO8601);
            }],
            [['published_at'], 'filter', 'filter' => 'strtotime', 'skipOnEmpty' => true],
            [['total_price', 'author_id', 'status'], 'integer'],
            [['advertisement_description', 'wishes'], 'string'],
            [['image_path'], 'string', 'max' => 255],
            [['your_link'], 'string', 'max' => 1024],
            [['thumbnail'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'total_price' => 'Total Price',
            'advertisement_description' => 'Advertisement Description',
            'image_path' => 'Image Path',
            'published_at' => 'Published At',
            'wishes' => 'Wishes',
            'author_id' => 'Author ID',
            'your_link' => 'Your Link',
            'status' => 'Status',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\OrdersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\OrdersQuery(get_called_class());
    }

    public function getImageUrl($default = null)
    {
        return  Yii::getAlias('/storage/web/source/'. $this->image_path);

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersCheckedChannel()
    {
        return $this->hasMany(CheckedChannel::class, ['order_id' => 'order_id']);
    }
    public function getChannel()
    {
        return $this->hasMany(Channel::className(), ['id' => 'channel_id'])
            ->via('ordersCheckedChannel');

    }

    public function getChannelsList()
    {
        return $this->hasMany(Channel::className(), ['id' => 'channel_id'])
            ->via('ordersCheckedChannel')
            ->select(['id','title'])
            ->asArray()
            ->all();
    }
    public function getChannelsListTitle()
    {
        return $this->hasOne(Channel::className(), ['id' => 'channel_id'])
            ->via('ordersCheckedChannel')
            ->select('title');

    }

    public function getCheckedStatus()
    {
        return $this->hasOne(CheckedChannel::class, ['order_id' => 'order_id']);
    }

    public function getStatus($channel_id)
    {
         $checked_channel = CheckedChannel::findOne([
            'order_id' =>$this->order_id,
            'channel_id' =>$channel_id,
         ]);

         return $checked_channel->status;

    }

    
    public function getAuthor()
    {
        $owner = $this->findModelUser($this->author_id);
        return Html::a(
            $owner->username,
            ['/user/view', 'id' => $this->author_id],
            ['class' => 'btn btn-link']
        );
    }

    protected function findModelUser($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
