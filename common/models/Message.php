<?php

namespace common\models;

use Yii;
use common\models\query\MessageQuery;

/**
 * This is the model class for table "message".
 *
 * @property int $id
 * @property int|null $order_id
 * @property int $type
 * @property int $user_id
 * @property int $recipient_id
 * @property string $content
 * @property int $sent_at
 * @property int|null $reply_to
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'type', 'user_id', 'recipient_id', 'sent_at', 'reply_to', 'status', 'created_at', 'updated_at'], 'integer'],
            [['type', 'user_id', 'content', 'status'], 'required'],
            [['content'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'type' => Yii::t('app', 'Type'),
            'user_id' => Yii::t('app', 'User ID'),
            'recipient_id' => Yii::t('app', 'Recipient ID'),
            'content' => Yii::t('app', 'Content'),
            'sent_at' => Yii::t('app', 'Sent At'),
            'reply_to' => Yii::t('app', 'Reply To'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return MessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MessageQuery(get_called_class());
    }
}
