<?php

namespace common\models;

use Yii;

use common\models\query\ChannelQuery;
use trntv\filekit\behaviors\UploadBehavior;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;

/**
 * This is the model class for table "channel".
 *
 * @property int $id
 * @property int $author_id
 * @property int $users
 * @property string $slug
 * @property string $title
 * @property int $type
 * @property int $category_id
 * @property int $price
 * @property int $exchange_price
 * @property string $channel_description
 * @property string $avatar_path
 * @property string $avatar_base_url
 * @property int $status
  * @property text $reason
 *
 * @property ChannelCategory $category
 */
class Channel extends \yii\db\ActiveRecord
{
    const STATUS_PUBLISH = 1;
    const STATUS_NEW = 0; 
    const STATUS_DELETED_ADMIN = 2;
    const STATUS_DELETED_OWNER = 3;
    const STATUS_DELETED = 4;
    const TYPE_CHANNEL = 0;
    const TYPE_CHAT = 1;

    public $thumbnail;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'channel';
    }

    public function behaviors()
    {
        return [

            [
                'class' => UploadBehavior::class,
                'attribute' => 'thumbnail',
                'pathAttribute' => 'avatar_path',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author_id', 'slug', 'title', 'price'], 'required'],
            [['author_id', 'users', 'type', 'category_id', 'price','exchange_price', 'status'], 'integer'],
            [['channel_description'], 'string'],
            [['reason'], 'string','max' => 500],
            [['category_id'], 'exist', 'targetClass' => ChannelCategory::class, 'targetAttribute' => 'id'],
            [['slug'], 'string', 'max' => 255],
            [['title'], 'string', 'max' => 512],
            [['avatar_path', 'avatar_base_url'], 'string', 'max' => 255],
            [['thumbnail'], 'safe'],
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => [self::STATUS_NEW, self::STATUS_PUBLISH, self::STATUS_DELETED_ADMIN, self::STATUS_DELETED_OWNER, self::STATUS_DELETED ]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_id' => 'Author ID',
            'users' => Yii::t('common','Users'),
            'slug' => 'Slug',
            'title' => Yii::t('common','Title'),
            'type' => 'Type',
            'category_id' => Yii::t('common','Category'),
            'price' => 'Price for chanel',
            'exchange_price' => Yii::t('common','Price'),
            'channel_description' => Yii::t('common','Channel Description'),
            'avatar_path' => 'Avatar Path',
            'avatar_base_url' => 'Avatar Base Url',
            'status' => 'Status',
            'reason' => Yii::t('common', 'Reason')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ChannelCategory::className(), ['id' => 'category_id']);
    }


    public function getOrdersCheckedChannel()
    {
        return $this->hasMany(CheckedChannel::class, ['channel_id' => 'id']);
    }

    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['order_id' => 'order_id'])
            ->via('ordersCheckedChannel');
    }

    public function getCountOrders(){
        $count = CheckedChannel::find()
            ->where(['channel_id' => $this->id])
            ->count();

        return $count;
    }

    public function getCountNewOrders(){
        $count = CheckedChannel::find()
            ->where(['channel_id' => $this->id])
            ->andWhere(['status' => 1])
            ->count();

        return $count;
    }


    public function getImageUrl($default = null)
    {
        return $this->avatar_path
            ? Yii::getAlias('/storage/web/source/'. $this->avatar_path)
            : $default;
    }

    public function getimageurl2()
    {
        // return your image url here
        return Url::to(\Yii::$app->request->BaseUrl.'/storage/web/source'.urldecode($this->avatar_path),true) ;

    }
    /**
     * @return ArticleQuery
     */
    public static function find()
    {
        return new ChannelQuery(get_called_class());
    }

    public function getChannelOwner()
    {
        $owner = $this->findModelUser($this->author_id);
        return Html::a(
            $owner->username,
            ['/user/view', 'id' => $this->author_id],
            ['class' => 'btn btn-link']
        );
    }

    protected function findModelUser($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
