<?php declare(strict_types=1);


namespace common\models\query;

use common\models\Channel;
use yii\db\ActiveQuery;

class ChannelQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function published()
    {
        $this->andWhere(['status' => Channel::STATUS_PUBLISH]);
        return $this;
    }
}
