<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "channel_category".
 *
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string $body
 * @property int $parent_id
 * @property int $status
 *
 * @property Channel[] $channels
 */
class ChannelCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'channel_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['body'], 'string'],
            [['parent_id', 'status'], 'integer'],
            [['slug'], 'string', 'max' => 1024],
            [['title'], 'string', 'max' => 512],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'title' => 'Title',
            'body' => 'Body',
            'parent_id' => 'Parent ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannels()
    {
        return $this->hasMany(Channel::className(), ['category_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\ChannelCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ChannelCategoryQuery(get_called_class());
    }
}
