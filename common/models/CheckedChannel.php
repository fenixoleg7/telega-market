<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\models\Orders;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "checked_channel".
 *
 * @property int $id
 * @property int $channel_id
 * @property int $order_id
 * @property int $status
 */
class CheckedChannel extends \yii\db\ActiveRecord
{


    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'checked_channel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['channel_id','published_at', 'order_id','status'], 'integer'],
            ['url', 'string', 'max' => 255],
            ['url', 'url', 'defaultScheme' => 'https']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'channel_id' => 'Channel ID',
            'order_id' => 'Order ID',
            'status' => 'Status',
            'url' => 'Url',
            'published_at' => 'Дата публикации',
            'created_at' => 'Создано',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(Channel::class, ['id' => 'channel_id']);
    }

    public function getOrder()
    {
        return $this->hasOne(Orders::class, ['order_id' => 'order_id']);
    }

    public function getOrders()
    {
        return $this->hasMany(Orders::class, ['order_id' => 'order_id']);
    }

    public static function getStatusList(): array
    {
        return [
            Yii::$app->params['STATUS_DRAFT'] => 'Новая заявка',
            Yii::$app->params['TAKEN_TO_WORK'] => 'Взята в работу',
            Yii::$app->params['STATUS_VERIFICATION'] => 'На проверке',
            Yii::$app->params['STATUS_VERIFICATION_OWNER'] => 'На проверке заказчиком',
            Yii::$app->params['STATUS_PUBLISHED'] => 'Опубликована',
            Yii::$app->params['STATUS_COMPLAIN'] => 'Жалоба',
            Yii::$app->params['STATUS_CANCELLED_ADMIN'] => 'Отклонена админом',
            Yii::$app->params['STATUS_CANCELLED_SISTEM'] => 'Отклонена системой',
        ];
    }

    public function getStatusLabel(): string
    {
        switch ($this->status) {
            case Yii::$app->params['STATUS_DRAFT']:
                $class = 'label label-default';
                break;
            case Yii::$app->params['TAKEN_TO_WORK']:
                $class = 'label label-primary';
                break;
            case Yii::$app->params['STATUS_VERIFICATION']:
                $class = 'label label-info';
                break;
             case Yii::$app->params['STATUS_VERIFICATION_OWNER']:
                $class = 'label label-info';
                break;
            case Yii::$app->params['STATUS_PUBLISHED']:
                $class = 'label label-success';
                break;
            case Yii::$app->params['STATUS_COMPLAIN']:
                $class = 'label label-danger';
                break;
            case Yii::$app->params['STATUS_CANCELLED_ADMIN']:
                $class = 'label label-danger';
                break;
            case Yii::$app->params['STATUS_CANCELLED_SISTEM']:
                $class = 'label label-danger';
                break;
        }

        return Html::tag('span', ArrayHelper::getValue($this->statuslist, $this->status), [
            'class' => $class,
        ]);
    }
}
