<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property int $id
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string $created_ip
 * @property string $updated_ip
 * @property string $created_by
 * @property string $updated_by
 * @property string $code
 * @property string $name
 * @property string $symbol
 * @property int $rate
 * @property int $decimal_places
 * @property int $is_default
 * @property int $sort
 * @property int $status
 */
class Currency extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
            ],
            'blameable' => [
                'class' => \yii\behaviors\BlameableBehavior::class,
            ],
            // 'ip' => [
            //     'class' => \yii\behaviors\AttributeBehavior::class,
            //     'attributes' => [
            //         ActiveRecord::EVENT_BEFORE_INSERT => 'created_ip',
            //         ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_ip',
            //     ],
            //     'value' => function ($event) {
            //         return Yii::$app->request->getUserIP();
            //     },
            // ],
        ];
    }

    public static function tableName()
    {
        return 'currency';
    }

    public function rules()
    {
        return [
            [['code','rate' ], 'required'],
            [['is_default', 'sort', 'status'], 'integer'],
            [['decimal_places'], 'integer', 'max' => 9],
            [['rate'], 'number'],
            [['code', 'base_ccy'], 'string', 'length' => 3],
            [['code'], 'unique'],
            [['code'], 'match', 'pattern' => '/^[A-Z]+$/', 'message' => Yii::t('app', 'Это поле может содержать только строчные буквы, цифры и дефис')],
            [['name', 'symbol'], 'string', 'max' => 32],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата обновления'),
            'created_by' => Yii::t('app', 'Создано пользователем'),
            'updated_by' => Yii::t('app', 'Обновлено пользователем'),
            'created_ip' => Yii::t('app', 'Создано c IP'),
            'updated_ip' => Yii::t('app', 'Обновлено c IP'),
            'code' => Yii::t('app', 'Код'),
            'base_ccy' => Yii::t('app', 'Код национальной валюты'),
            'name' => Yii::t('app', 'Название'),
            'symbol' => Yii::t('app', 'Символ'),
            'rate' => Yii::t('app', 'Курс'),
            'decimal_places' => Yii::t('app', 'Количество знаков после запятой'),
            'is_default' => Yii::t('app', 'По умолчанию'),
            'sort' => Yii::t('app', 'Сортировка'),
            'status' => Yii::t('app', 'Опубликован'),
        ];
    }
   
    // public function afterSave($insert, $changedAttributes)
    // {
    //     return Yii::$app->session->remove('currency');
    // }
    
    public static function listItems()
    {
        $items = self::find()
            ->select(['code'])
            ->where(['status' => self::STATUS_ACTIVE])
            ->orderBy(['sort' => SORT_ASC])
            ->asArray()
            ->all();
        return ArrayHelper::map($items, 'code', 'code');
    }
    
    public static function statuses()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Опубликован'),
            self::STATUS_INACTIVE => Yii::t('app', 'Не опубликован'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\CurrencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\CurrencyQuery(get_called_class());
    }
}
