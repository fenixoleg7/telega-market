<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use \common\models\Currency;

/**
 * This is the model class for table "money".
 *
 * @property int $id
 * @property int $user_id
 * @property int $order_id
 * @property string $text
 * @property string $currency
 * @property int $type
 * @property int $value
 * @property int $status
 * @property int $currency_id
 * @property integer             $created_by
 * @property integer             $updated_by
 * @property integer             $created_at
 * @property integer             $updated_at
 * @property string              $email_confirm_token
 * @property string              $requisites
 */
class Money extends \yii\db\ActiveRecord
{
    const TYPE_ADD_MONEY            = 1;
    const TYPE_ADD_ADMIN_MONEY      = 2;
    const TYPE_SALE_MONEY           = 3;
    const TYPE_REFUNDED_MONEY       = 4;
    const TYPE_DECLINED_MONEY       = 5;
    const TYPE_WITHDRAW_MONEY       = 6;
    const TYPE_CHARGED_ADMIN_MONEY  = 7;

    const STATUS_OK_MONEY            = 1;
    const STATUS_REJECTED_MONEY      = 2; //СТАТУС ОТКЛОНЕННЫЕ ДЕНЬГИ
    const STATUS_CONSIDERATION_MONEY = 3; // На рассмотрение

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'money';
    }

     /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'text', 'currency_id', 'value'], 'required'],
            [['user_id', 'order_id','currency_id', 'type', 'value', 'status'], 'integer'],
            [['text'], 'string', 'max' => 1024],
            [['currency'], 'string', 'max' => 512],
            [['email_confirm_token'], 'string', 'max' => 128],
            [['requisites'], 'string', 'max' => 25],
            [['email_confirm_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            //'id' => Yii::t('app', 'ID'),
            'id' => 'id',
            // 'user_id' => Yii::t('app', 'User ID'),
            'user_id' => 'Пользователь',
            //'order_id' => Yii::t('app', 'Order ID'),
            'order_id' => 'Номер заказа',
            // 'text' => Yii::t('app', 'Text'),
            'text' => 'Описание',
            'currency' => Yii::t('app', 'Currency'),
            'requisites' =>'Реквизиты',
            // 'currency_id' => Yii::t('app', 'Currency type'),
            'currency_id' => 'Валюта',
            // 'type' => Yii::t('app', 'Type'),
            'type' => 'Тип операции',
            // 'value' => Yii::t('app', 'Value'),
            'value' => 'Значение',
            // 'status' => Yii::t('app', 'Status'),
            'status' => 'Статус',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\MoneyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MoneyQuery(get_called_class());
    }

    public function getCurrencyValue()
    {
        return $this->hasOne(Currency::class, ['id' => 'currency_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

}
