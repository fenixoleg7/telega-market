<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CurrencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Currencies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Currency'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <p>
        <?= Html::a(Yii::t('backend', 'Loading Currency'), ['loading-currency'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <?php // $data = file_get_contents('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5');?>

    <?php // print_r($data) ?>> -->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'created_at',
            //'updated_at',
            //'created_ip:ntext',
            //'updated_ip:ntext',
            //'created_by:ntext',
            //'updated_by:ntext',
            
            'name:ntext',
            'code:ntext',
            'base_ccy',
            'symbol:ntext',
            'rate',
            //'decimal_places',
            'is_default',
            'sort',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
