<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Orders;
use common\models\Channel;
use common\models\CheckedChannel;
use common\models\User;
use trntv\yii\datetime\DateTimeWidget;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CheckedChannelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('frontend', 'Checked Channels');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="checked-channel-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => true,
        'pjax' => true,
        'striped' => true,
        'hover' => true,
        'panel' => ['type' => 'primary', 'heading' => 'Grid Grouping Example'],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            [
                'attribute' => 'order_id',
                'width' => '310px',
                'value' => function ($model, $key, $index, $widget) {
                    return $model->order_id;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Orders::find()->orderBy('order_id')->asArray()->all(), 'order_id','order_id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Any supplier'],
                //'group' => true,  // enable grouping,
                //'groupedRow' => true,                    // move grouped column to a single grouped row
                //'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
                //'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
            ],
            [
                'attribute' => 'Client',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()->orderBy('username')->asArray()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Any author'],
                //'format' => 'html',
                'value' => function($model){
                    return $model->order->author;
                },
                'group' => true,  // enable grouping,
                'format' => 'raw',
            ],
            [
                'attribute' => 'Image',
                'format' => 'html',
                'value' => function ($model) {
                    $url = Html::encode($model->order->imageUrl);
                    $url = str_replace('\\', '/', $url);
                    return Html::img($url, ['width' => '50px'],false);
                },
            ],
            [
                'attribute' => 'Text',
                'value' => function ($model, $key, $index, $widget) {
                    return $model->channel->title;
                    return $model->order->advertisement_description;
                },
                'group' => true,  // enable grouping,
            ],

            [
                'attribute' => 'Channel',
                'width' => '250px',
                'format' => 'html',
                'value' => function ($model, $key, $index, $widget) {
                    return Html::a(
                        $model->channel->title,
                        $model->channel->slug,
                        ['class' => 'btn btn-link']);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Channel::find()->orderBy('title')->asArray()->all(), 'id', 'title'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Any category'],
                'group' => true,  // enable grouping
                'subGroupOf' => 1 // supplier column index is the parent group
            ],
            [
                'attribute' => 'Channel Owner',
                'format' => 'html',
                'value' => function($model){

                    return $model->channel->channelOwner;
                }
            ],
            [
                'attribute' => 'Link',
                'value' => function ($model, $key, $index, $widget) {
                    return $model->url;
                },
                'group' => true,  // enable grouping

            ],

            [
                'attribute' => 'Price',
                'value' => function ($model, $key, $index, $widget) {
                    return $model->channel->price;
                },
                'width' => '150px',
                'hAlign' => 'right',
                'format' => ['decimal', 2],
                'pageSummary' => true,
                //'pageSummaryFunc' => GridView::F_AVG
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->statuslabel;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => CheckedChannel::getStatusList(),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Any status'],
                'format' => 'raw',
            ],
            [
            'attribute' => 'published_at',
            'options' => ['style' => 'width: 10%'],
            'format' => 'datetime',
            'filter' => DateTimeWidget::widget([
                'model' => $searchModel,
                'attribute' => 'published_at',
                'phpDatetimeFormat' => 'dd.MM.yyyy',
                'momentDatetimeFormat' => 'DD.MM.YYYY',
                'clientEvents' => [
                    'dp.change' => new JsExpression('(e) => $(e.target).find("input").trigger("change.yiiGridView")'),
                ],
            ]),
        ],
        [
            'attribute' => 'created_at',
            'options' => ['style' => 'width: 10%'],
            'format' => 'datetime',
            'filter' => DateTimeWidget::widget([
                'model' => $searchModel,
                'attribute' => 'created_at',
                'phpDatetimeFormat' => 'dd.MM.yyyy',
                'momentDatetimeFormat' => 'DD.MM.YYYY',
                'clientEvents' => [
                    'dp.change' => new JsExpression('(e) => $(e.target).find("input").trigger("change.yiiGridView")'),
                ],
            ]),
        ],

            //'created_at',
            //'published_at',
            //'updated_at',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
