<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('frontend', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1>Заказы</h1>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => true,
        'pjax' => true,
        'striped' => true,
        'hover' => true,
        'panel' => ['type' => 'primary', 'heading' => 'Grid Grouping Example'],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'order_id',
            [
                'format' => 'html',
                'value' => function ($model) {
                    $url = Html::encode($model->getImageUrl());
                    $url = str_replace('\\', '/', $url);
                    return Html::img($url, ['width' => '50px'],false);
                },
            ],
            //'advertisement_description:ntext',
            [
                'attribute' => 'canals',
                'width' => '250px',
                'value' => function ($model, $key, $index, $widget) {
                     //return $model->channel->title;
                    return $model->channelsListTitle->title;
                },
//                'filterType' => GridView::FILTER_SELECT2,
//                'filter' => $model->getChannelsList(),
//                'filterWidgetOptions' => [
//                    'pluginOptions' => ['allowClear' => true],
//                ],
//                'filterInputOptions' => ['placeholder' => 'Any category']
            ],
            'published_at',
            //'wishes:ntext',
            'author_id',
            //'your_link',
            'status',
            'total_price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
