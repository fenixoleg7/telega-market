<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Channels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="channel-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Channel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'format' => 'html',
                'value' => function ($model) {
                    $url = Html::encode($model->getImageUrl());
                    $url = str_replace('\\', '/', $url);
                    return Html::img($url, ['width' => '50px'],false);
                }
            ],
            //'id',
            'author_id',
            'users',
//            'slug',
            'title',
            //'type',
            //'category_id',
            'price',
            'exchange_price',
            //'channel_description:ntext',
            //'avatar_path',
            'reason',
            [
                'attribute' => 'status',
                'filter' => \common\helpers\ChannelHelper::statusList(),
                'value' => function (\common\models\Channel $model) {
                    return \common\helpers\ChannelHelper::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['style' => 'width: 5%'],
                'template' => '{update} {delete}',
            ]
        ]
    ]); ?>
</div>
