<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use trntv\filekit\widget\Upload;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Channel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="channel-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?php if ($model->avatar_path )
            $url = Html::encode($model->getImageUrl());
            $url = str_replace('\\', '/', $url);
            echo Html::img($url, ['width' => '222px'],false);
        ?>

        <?= $form->field($model, 'exchange_price')->textInput() ?>

        <?php echo $form->field($model, 'status')->dropDownlist([
            \common\models\Channel::STATUS_PUBLISH => 'Опубликован',
            \common\models\Channel::STATUS_NEW => 'Новый',
            \common\models\Channel::STATUS_DELETED_ADMIN => 'Деактивирован администрацией',
            \common\models\Channel::STATUS_DELETED_OWNER => 'Деактивирован владельцем',
            \common\models\Channel::STATUS_DELETED => 'Удалить из БД'
        ], ['prompt' => '','onchange'=>'
            var sel = ($(this).val());
            if(sel == 2){
                jQuery(".other-block").show();
            }else {
                jQuery(".other-block").hide();
            }'], ['id' => 'select']);?>

    
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'slug')->textInput(['readonly'=> true]) ?>

            <?= $form->field($model, 'title')->textInput(['readonly'=> true]) ?>

            <?php echo $form->field($model, 'type')->dropDownlist([
                \common\models\Channel::TYPE_CHANNEL => 'Канал',
                \common\models\Channel::TYPE_CHAT => 'Чат'
            ], ['prompt' => '', 'disabled' => 'disabled']) ?>

            <?= $form->field($model, 'price')->textInput(['readonly'=> true]) ?>


            <?php echo $form->field($model, 'category_id')->dropDownList(\yii\helpers\ArrayHelper::map(
                $categories,
                'id',
                'title'
            ), ['prompt' => '', 'disabled' => 'disabled']) ?>
        </div>
    </div>

    <div class="other-block">
        <?= $form->field($model, 'reason')->textArea()?>
    </div>

    <?= $form->field($model, 'channel_description')->textarea(['rows' => 6,'readonly'=> true]) ?>

    


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
