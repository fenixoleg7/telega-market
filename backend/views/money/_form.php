<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use \common\helpers\MoneyHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Money */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="money-form">

    <?php $form = ActiveForm::begin(); ?>


    <?php echo $form->field($model, 'user_id')->dropDownList(\yii\helpers\ArrayHelper::map(
        $users,
        'id',
        'username'
    ), ['prompt' => '']) ?>

    <?// = $form->field($model, 'order_id')->textInput(['value' => 'Your Value']) ?>

    <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'currency_id')->dropDownList(ArrayHelper::map(
        $currency,
        'id',
        'code'
    ), ['prompt' => '']) ?>

    <?= $form->field($model, 'type')->dropDownList(MoneyHelper::typeList(), ['prompt' => '']) ?> 

    <?= $form->field($model, 'value')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(MoneyHelper::statusList(), ['prompt' => '']) ?> 

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
