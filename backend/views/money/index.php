<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\helpers\MoneyHelper;
use common\models\Money;
use yii\bootstrap\Modal;




/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MonneySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Moneys');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="money-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить платеж', ['create'], ['class' => 'btn btn-success']) ?>

    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->user->username;
                },
            ],
            'order_id',
            'text',
            [
                'attribute' => 'currency',
                'value' => function ($model) {
                    return $model->currencyValue->code;
                },
            ],
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    return MoneyHelper::typeName($model->type);
                },
            ],
            'value',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                   return MoneyHelper::statusName($model->status);
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
