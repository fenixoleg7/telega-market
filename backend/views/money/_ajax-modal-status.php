<?php
/**
 * Created by PhpStorm.
 * User: Fenixol
 * Date: 22.11.2019
 * Time: 11:35
 */

use yii\helpers\Html;
use yii\bootstrap\Modal;

 Modal::begin([
        'header' => '<h2>Подтверждение</h2>',
        'toggleButton' => ['label' => 'Send for verification', 'class' => 'btn btn-primary active send-verification'],
    ]);

    ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Одобрить', ['class' => 'btn btn-success']) ?>
            <?= Html::Button('Отклонить', ['class' => 'btn btn-danger']) ?>
        </div>
    </div>

    <?

Modal::end();?>


