<?php

namespace backend\controllers;

use Yii;
use common\models\Money;
use common\models\Currency;
use backend\models\search\MoneySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;

/**
 * MoneyController implements the CRUD actions for Money model.
 */
class MoneyController extends Controller
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Money models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MoneySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => ['id' => SORT_DESC],
        ];
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single Money model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Money model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Money();
        $users = User::find()->All();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            switch ($model->status) {
                case Money::TYPE_ADD_MONEY:
                    break;
                case Money::TYPE_ADD_ADMIN_MONEY:
                    $model->user->balance = $model->user->balance + $model->value;
                    $model->save();
                    break;
                default:
                    break;
            }

            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
            'users'=> $users,
            'currency' => Currency::find()->active()->all(),
        ]);
    }

    /**
     * Updates an existing Money model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $users = User::find()->All();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            switch ($model->type) {
                case Money::TYPE_ADD_MONEY:
                    $model->user->balance = $model->user->balance + $model->value;
                    $model->user->save();
                    break;
                case Money::TYPE_ADD_ADMIN_MONEY:
                    $model->user->balance = $model->user->balance + $model->value;
                    $model->user->save();
                    break;
                default:
                    break;
            }

            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
            'users'=> $users,
            'currency' => Currency::find()->active()->all(),
        ]);
    }

    /**
     * Deletes an existing Money model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Money model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Money the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Money::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
    }
}
